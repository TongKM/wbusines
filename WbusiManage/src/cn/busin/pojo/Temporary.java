package cn.busin.pojo;

import java.util.Date;
import java.util.List;

public class Temporary {
	public Temporary() {
	}
public Temporary(int oa, String ot) {
		super();
		this.oa = oa;
		this.ot = ot;
	}
private double oa;//对应dao的映射文件
private String ot;
public double getOa() {
	return oa;
}
public void setOa(double oa) {
	this.oa = oa;
}
public String getOt() {
	return ot;
}
public void setOt(String ot) {
	this.ot = ot;
}
private List<Temporary> buyer;//报表
private List<Temporary> seller;
public List<Temporary> getBuyer() {
	return buyer;
}
public void setBuyer(List<Temporary> buyer) {
	this.buyer = buyer;
}
public List<Temporary> getSeller() {
	return seller;
}
public void setSeller(List<Temporary> seller) {
	this.seller = seller;
}


}