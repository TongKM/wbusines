package cn.busin.pojo;

public class Product {
	private Integer id;
	private String proName;
	private Double proPrice;
	private Integer proStatus;//产品状态(1.上架;2.下架)
	private Integer proCount;//库存数量
	private String proPicPath;//产品图片存放路径
	private String proUnit;//产品单位(个/只/斤等)
	private Integer count;//临时购买数量
	private String proDesc;//产品描述
	
	
	public String getProDesc() {
		return proDesc;
	}
	public void setProDesc(String proDesc) {
		this.proDesc = proDesc;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public String getProUnit() {
		return proUnit;
	}
	public void setProUnit(String proUnit) {
		this.proUnit = proUnit;
	}
	public String getProPicPath() {
		return proPicPath;
	}
	public void setProPicPath(String proPicPath) {
		this.proPicPath = proPicPath;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getProName() {
		return proName;
	}
	public void setProName(String proName) {
		this.proName = proName;
	}
	public Double getProPrice() {
		return proPrice;
	}
	public void setProPrice(Double proPrice) {
		this.proPrice = proPrice;
	}
	public Integer getProStatus() {
		return proStatus;
	}
	public void setProStatus(Integer proStatus) {
		this.proStatus = proStatus;
	}
	public Integer getProCount() {
		return proCount;
	}
	public void setProCount(Integer proCount) {
		this.proCount = proCount;
	}
	
}
