package cn.busin.pojo;

import java.util.Date;
import java.util.List;

public class Order {
	private Integer id;
	private String orderNo;
	private Integer oCount;//用不到
	private String proId;//用不到
	private Date oTime;
	private Double oAmount;
	private Integer buyer;
	private Integer seller;
	private String reAddress;
	private Integer oStatus;
	private String message;//买家留言
	private List<Shop> shopList;//每个订单对应的商品集合
	private User user;//临时存放用户对象（买家/卖家）
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<Shop> getShopList() {
		return shopList;
	}
	public void setShopList(List<Shop> shopList) {
		this.shopList = shopList;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getoCount() {
		return oCount;
	}
	public void setoCount(Integer oCount) {
		this.oCount = oCount;
	}
	public String getProId() {
		return proId;
	}
	public void setProId(String proId) {
		this.proId = proId;
	}
	public Date getoTime() {
		return oTime;
	}
	public void setoTime(Date oTime) {
		this.oTime = oTime;
	}
	public Double getoAmount() {
		return oAmount;
	}
	public void setoAmount(Double oAmount) {
		this.oAmount = oAmount;
	}
	public Integer getBuyer() {
		return buyer;
	}
	public void setBuyer(Integer buyer) {
		this.buyer = buyer;
	}
	public Integer getSeller() {
		return seller;
	}
	public void setSeller(Integer seller) {
		this.seller = seller;
	}
	public String getReAddress() {
		return reAddress;
	}
	public void setReAddress(String reAddress) {
		this.reAddress = reAddress;
	}
	public Integer getoStatus() {
		return oStatus;
	}
	public void setoStatus(Integer oStatus) {
		this.oStatus = oStatus;
	}

}
