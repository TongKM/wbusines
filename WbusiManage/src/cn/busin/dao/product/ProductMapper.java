package cn.busin.dao.product;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.busin.pojo.Product;

public interface ProductMapper {
	//查看上级产品(根据产品名称模糊查询，上级id精确查询)
	public List<Product> find(@Param("proName")String proName,@Param("uId")Integer uId);
	//根据条件查询总记录数
	public Integer count(@Param("proName")String proName,@Param("uId")Integer uId);
	//shopByproduct临时数据查询商品
	public Product shopByproduct(@Param("proId")int proId)throws Exception;
	//模糊查询产品信息
	public List<Product> allProduct(@Param("nameofThe")String nameofThe,@Param("proStatus")String proStatus)throws Exception;
	//新增产品
	public int addProduct(Product product)throws Exception;
	//id查询产品
	public Product getProduct(@Param("id")int id)throws Exception;
	//修改
	public int updateProduct(Product product)throws Exception;
	//删除
	public int delProduct(@Param("id")Integer id)throws Exception;
	//生成订单后，对商品库存进行减
	public int reduction(@Param("id")Integer id,@Param("count")Integer count);
	//取消支付后，对商品库存进行加
	public int add(@Param("id")Integer id,@Param("count")Integer count);
	
	/**
	 * 查询待上架产品数量
	 * @return
	 */
	public Integer daiShangjia();
	
	/**
	 * 查询待补货产品数量
	 * @return
	 */
	public Integer daiBuhuo();
	//查询已下架产品集合
	public List<Product> findByDown();
	//查询需补货产品集合
	public List<Product> findByLess();
}
