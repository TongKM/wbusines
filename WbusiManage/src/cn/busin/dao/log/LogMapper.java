package cn.busin.dao.log;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.busin.pojo.Log;

public interface LogMapper {
	//日志录入
	public int entryLog(Log log) throws Exception;
	
	/**
	 * 日志查询
	 * @param 用户名
	 * @param 页码
	 * @param 页面容量
	 * @return
	 */
	public List<Log> queryLog(@Param("userName")String userName, @Param("pageNo")Integer pageNo, @Param("pageSize")Integer pageSize);
	
	/**
	 * 日志总记录数
	 * @param userName
	 * @return
	 */
	public Integer queryLogCount(@Param("userName")String userName);
	
	/**
	 * 查询昨日上线人数
	 * @return
	 */
	public Integer queryYesterdayLevel();
	
	/**
	 * 昨日上线次数
	 * @return
	 */
	public Integer queryYestedayTime();
	
	/**
	 * 近期未上线人数
	 * @return
	 */
	public Integer queryNearFuture();
}
