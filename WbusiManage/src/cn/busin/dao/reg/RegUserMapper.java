package cn.busin.dao.reg;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import cn.busin.pojo.User;


@Repository
public interface RegUserMapper {
	public int regUserSave(User user)throws Exception;
	public int regexist(@Param("emil")String emil) throws Exception;
}
