package cn.busin.dao.user;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.busin.pojo.User;
import cn.busin.pojo.UserDetailed;

public interface UserMapper {
	//邮箱登录
	public User loginByE(@Param("email")String email,@Param("password")String password);
	//手机号登陆
	public User loginByP(@Param("phone")String phone,@Param("password")String password);
	//getUserlowerlevel
	public List<User> getUserlowerlevel(@Param("id")Integer id)throws Exception;
	//修改
	public int upUser(User user) throws Exception;
	//根据userId查询user
	public User queryUser(@Param("id")Integer id);
	
	/**
	 * 本月申请代理数
	 * @return
	 */
	public Integer getMonthAgencyCount();
	
	/**
	 * 本月授权数
	 * @return
	 */
	public Integer getMonthGrantCount(); 
	
	/**
	 * 代理总人数
	 * @return
	 */
	public Integer getAllGrantCount();
	
	/**
	 * 待审核代理
	 * @return
	 */
	public Integer getNoExamineCount();
	
	//Waiting等待审核名单
	public List<User> waitingUser()throws Exception;
	//refusedUser拒绝名单
	public List<User> refusedUser()throws Exception;
	//审核public boolean audit
	public int audit(@Param("id")int id, @Param("state")int state,
			@Param("grantDate")Date grantDate)throws Exception;
	//符合条件的所以数量
	public int countAgentUser(@Param("name")String name)throws Exception;
	//分页查询 起始位置
	public List<User> agentUserList(@Param("starting")int starting,@Param("name")String name)throws Exception;
	//个人信息
	public UserDetailed getUserDetailed(@Param("id")int id)throws Exception;
}
