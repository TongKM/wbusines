package cn.busin.dao.perinfo;

import org.springframework.stereotype.Repository;

@Repository
public interface PerInfoMapper {
	//修改個人信息
	public int modifyPerInfo() throws Exception;
}
