package cn.busin.dao.order;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import cn.busin.pojo.Order;
import cn.busin.pojo.Temporary;
import cn.busin.pojo.User;
import cn.busin.tool.Pages;

@Repository
public interface OrderMapper {
	public List<Order> Buyorder(@Param("buyer") int buyer) throws Exception;

	public List<Order> Sellorder(@Param("seller") int seller) throws Exception;

	// id删除delOidOrder
	public int delOidOrder(@Param("id") int id) throws Exception;

	// 完成订单
	public int completeTrading(@Param("oId") Integer oId) throws Exception;

	// superUser id查信息
	public User superUser(@Param("id") int id);

	// 报表查询public List<Order> report(HttpSession session)买入，卖出(修改)
	public List<Temporary> reportBuyer(@Param("buyer") Integer buyer)
			throws Exception;

	public List<Temporary> reportSeller(@Param("seller") Integer seller)
			throws Exception;

	// ----

	/**
	 * 本月成交金额
	 * 
	 * @return
	 */
	public Double getMonthTotal();

	/**
	 * 本月成交量
	 * 
	 * @return
	 */
	public Integer getMonthCount();

	// lastmonth 上月排名
	public List<Order> lastmonth(@Param("id") int  id)
			throws Exception;

	// thismonth 本月排名
	public List<Order> thismonth(@Param("id") int  id)
			throws Exception;

	// totallist 总排行
	public List<Order> totallist(@Param("id") int  id)
			throws Exception;
	//管理查询信息，有分页management  条件（conditions）
	public List<Order> getManageOrderList(@Param("conditions")String conditions,@Param("pageNo")Integer pageNo,@Param("pageSize")Integer pageSize);
	//我的订单总记录数
	public Integer getManageOrderCount(@Param("conditions")String conditions);
	
	//统计图
	public List<Order> statisticalOrder(@Param("statistical")int statistical)throws Exception;
	//查询全部数据
	public List<Order> getListOrder()throws Exception;
}
