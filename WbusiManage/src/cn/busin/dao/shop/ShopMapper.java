package cn.busin.dao.shop;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.busin.pojo.Order;
import cn.busin.pojo.Product;
import cn.busin.pojo.Shop;

public interface ShopMapper {
	//查询购物车是否有物品
	public Shop anyShop(Shop shop)throws Exception;
	//添加到购物车，如果购物车吗没有数据，就添加，否则修改
	public int IncreaseShop(Shop shop)throws Exception;
	//否则修改
	public int updateShop(Shop shop)throws Exception;
	//查看购物车
	public List<Product> shoppingCart(@Param("buyer")Integer buyerid)throws Exception;
	//修改数量
	public int modifyShopping(@Param("buyerid")Integer buyerid, @Param("count")Integer count, @Param("proId")Integer proId)throws Exception;
	//删除
	public int delShopping(@Param("buyerid")Integer buyerid, @Param("proId")Integer proId)throws Exception;
	//查询本人购物车信息
	public List<Shop> getShopListInfo(@Param("buyer")Integer buyer)throws Exception;
	//生成订单
	public int generate(Order order)throws Exception;
	//修改个人购物车状态modifystatus
	public int modifyStatus(Shop shop)throws Exception;
	//orderidByshop由订单id查询订单集合
	public List<Shop> orderidByshop(@Param("oId")int oId)throws Exception;
	//根据订单id删除delShop
	public int delOidShop(@Param("oId")int oId)throws Exception;
	//oid查询
	public List<Shop> getOidShop(@Param("oId")Integer oId)throws Exception;
}