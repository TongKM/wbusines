package cn.busin.tool;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.web.multipart.MultipartFile;


public class PicPath {
	/**
	 * 保存图片，返回图片相对路径
	 * 
	 */
	public static String getPath(MultipartFile attachs,HttpServletRequest request){
		String errorInfo=null;
		String path="/WbusiManage/uploadFiles/";
				/*getRealPath("statics"+File.separator+"uploadfiles");*/
			MultipartFile attach=attachs;
			String fileName="";
			//判断文件是否为空
			if(!attach.isEmpty()){
				String oldFileName=attach.getOriginalFilename();//原文件名
				String prefix=FilenameUtils.getExtension(oldFileName);//原文件后缀名
				int filesize=50000000;//定义文件不能超过的大小
				
				if(attach.getSize()>filesize){//上传大小不得超过500K
					request.setAttribute(errorInfo, "上传大小不得超过500K");
				}else if(prefix.equalsIgnoreCase("bmp")||prefix.equalsIgnoreCase("jpg")||prefix.equalsIgnoreCase("png")
						||prefix.equalsIgnoreCase("jpeg")||prefix.equalsIgnoreCase("pneg")){//上传图片格式不正确
					//随机创建一个新文件夹名
					fileName=System.currentTimeMillis()+RandomUtils.nextInt(1000000)+"_Personal.jpg";
					//使用该路径该文件夹名来创建一个File对象
					File targetFile=new File("/uploadFiles",fileName);
					if(!targetFile.exists()){//判断该文件是否存在
						targetFile.mkdirs();
					}
					//保存
					try {
						 //1图片存储的路径  
						String filePath = request.getSession().getServletContext().getRealPath("/") + "uploadFiles/"  
		                        + fileName;  
		                // 转存文件  
						attach.transferTo(new File(filePath));  
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						request.setAttribute(errorInfo, "上传失败！");
					} 
				}else{
					request.setAttribute(errorInfo, "上传图片格式不正确");
				}
			}
		return path+fileName;
	}
	/**
     * 删除单个文件
     *
     * @param fileName
     *            要删除的文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public static boolean deleteFile(String fileName,HttpServletRequest request) {
    	String fileName2=StringUtils.substringBefore(request.getRealPath(request.getRequestURI()), "\\WbusiManage"); 
		fileName2+=fileName;
    	if(fileName2==null)
    		return false;
        File file = new File(fileName2);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                System.out.println("删除单个文件" + fileName2 + "成功！");
                return true;
            } else {
                System.out.println("删除单个文件" + fileName2 + "失败！");
                return false;
            }
        } else {
            System.out.println("删除单个文件失败：" + fileName2 + "不存在！");
            return false;
        }
    }
}
