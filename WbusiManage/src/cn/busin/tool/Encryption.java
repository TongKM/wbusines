package cn.busin.tool;

import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * 加密类
 * @author asus
 *
 */
public class Encryption {
	public static final String KEY_SHA = "SHA"; //固定字符串SHA
	/**
	 * 加密过程，返回加密后的字符串
	 * @param inputStr
	 * @return String
	 */
	 public static String getResult(String inputStr)
	  {
	    BigInteger sha =null;
	    //System.out.println("=======加密前的数据:"+inputStr);
	    byte[] inputData = inputStr.getBytes(); 
	    try {
	    	//获取MessageDigest实例
	       MessageDigest messageDigest = MessageDigest.getInstance(KEY_SHA);
	       messageDigest.update(inputData);//使用update()方法处理被加密数据
	       sha = new BigInteger(messageDigest.digest()); //生成加密后的数据
	       //System.out.println("SHA加密后:" + sha.toString()); 
	    } catch (Exception e) {e.printStackTrace();}
	    return sha.toString();
	  }
}
