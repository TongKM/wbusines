package cn.busin.tool;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cn.busin.pojo.User;
/**
 * 拦截器：用于拦截用户跳过注册和登陆的所有操作
 * @author asus
 *
 */
public class Interceptor extends HandlerInterceptorAdapter {
	
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response,
			Object handler){
		HttpSession session=request.getSession();
		User user=(User)session.getAttribute("user");
		System.out.println(user);
		if(null==user){
			try {
				response.sendRedirect("/WbusiManage/Account/Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
		return true;
	}
}
