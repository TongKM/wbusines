package cn.busin.tool;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cn.busin.pojo.Temporary;

public class Auxiliary {
	public static String generateOrderSerial(int id) {
		// hh时mm分ss秒SSS毫秒"
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmmssSSS");
		String serial = format.format(new Date());
		serial += id;
		return "Gj" + serial;
	}

	// 报表辅助转换
	public static Temporary conversionTemporary(Temporary temporary) {
		Temporary tem = new Temporary();
		List<Temporary> listb = new ArrayList<Temporary>();
		for (int i = -4; i < 1; i++) {
			List<Temporary> list = new ArrayList<Temporary>();
			if (temporary != null && temporary.getBuyer() != null) {
				list = temporary.getBuyer();
				boolean sb = true;
				for (Temporary temporary2 : list) {
					if (temporary2.getOt() != null&& temporary2.getOt().equals(sb(i))) {
						listb.add(temporary2);
						tem.setBuyer(listb);
						sb = false;
					}
				}
				if (sb) {
					listb.add(new Temporary(0, sb(i)));
					tem.setBuyer(listb);
				}
			} else {
				listb.add(new Temporary(0, sb(i)));
				tem.setBuyer(listb);
			}
		}
		List<Temporary> listbs = new ArrayList<Temporary>();
		for (int i = -4; i < 1; i++) {
			List<Temporary> list = new ArrayList<Temporary>();
			if (temporary != null && temporary.getSeller() != null) {
				list = temporary.getSeller();
				boolean sb = true;
				for (Temporary temporary2 : list) {
					if (temporary2.getOt() != null&& temporary2.getOt().equals(sb(i))) {
						listbs.add(temporary2);
						tem.setSeller(listbs);
						sb = false;
					}
				}
				if (sb) {
					listbs.add(new Temporary(0, sb(i)));
					tem.setSeller(listbs);
				}
			} else {
				listbs.add(new Temporary(0, sb(i)));
				tem.setSeller(listbs);
			}
		}
		return tem;
	}

	public static String sb(int ib) {
		SimpleDateFormat matter = new SimpleDateFormat("MM");
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, ib);// 负数,0当前月
		Date date02 = calendar.getTime();
		String time02 = matter.format(date02);
		// System.out.println(time02);//2017-08格式
		return time02;
	}
}
