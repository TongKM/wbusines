package cn.busin.tool;

/**
 * 分页查询工具类,传入页面容量，总记录数，当前页码即可计算出总页数和起始位置
 * @author asus
 *
 */
public  class Pages {
	private Integer pageSize;	//页面容量
	private Integer totalCount;	//记录总数
	private Integer totalPageCount;	//总页数（自动计算）
	private Integer pageIndex;		//当前页码
	private Integer from;			//mysql查询的开始位置（自动计算）
	
	public Integer getFrom() {
		this.from=(this.pageIndex-1)*this.pageSize;
		return from;
	}
	public void setFrom(Integer from) {
		this.from = from;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public Integer getTotalPageCount() {
		if(this.totalCount % this.pageSize == 0){
			return this.totalCount / this.pageSize;
		}else{
			return (this.totalCount / this.pageSize + 1);
		}
	}
	public void setTotalPageCount(Integer totalPageCount) {
		this.totalPageCount=totalPageCount;
	}
	public Integer getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(Integer pageIndex) {
		if(pageIndex>0){
			this.pageIndex = pageIndex;
		}else{
			this.pageIndex=1;
		}
		
	}
	
}
