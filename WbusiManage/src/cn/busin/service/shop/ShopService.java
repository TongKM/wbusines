package cn.busin.service.shop;

import java.util.List;

import javax.servlet.http.HttpSession;

import cn.busin.pojo.Product;
import cn.busin.pojo.Shop;

public interface ShopService {
		//将商品添加到购物车，不增加订单，在生成订单是添加订单
		public boolean IncreaseShop(Shop shop);
		//查看个人购物车
		public List<Product> shoppingCart(Integer buyerid);
		//修改购物车物品
		public boolean modifyShopping(Integer buyerid,Integer count,Integer proId);
		//删除购物车物品
		public boolean delShopping(Integer buyerid,Integer proId);
		//根据买家生成订单
		public boolean Togenerateorders(HttpSession session,Double oAmount,String tempAddress,String message);
}
