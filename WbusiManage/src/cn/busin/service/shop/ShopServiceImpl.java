package cn.busin.service.shop;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import cn.busin.dao.product.ProductMapper;
import cn.busin.dao.shop.ShopMapper;
import cn.busin.pojo.Order;
import cn.busin.pojo.Product;
import cn.busin.pojo.Shop;
import cn.busin.pojo.User;
import cn.busin.tool.Auxiliary;
//商品
@Service
public class ShopServiceImpl implements ShopService{
	@Resource
	private ShopMapper shopMapper;
	@Resource
	private ProductMapper productMapper;
	public boolean IncreaseShop(Shop shop){
		if(null==shop)
			return false;
		boolean increase =false;
		try {
			Shop any=shopMapper.anyShop(shop);
			if(null!=any&&any.getId()!=null){
				shop.setId(any.getId());
				shop.setCount(any.getCount()+shop.getCount());
				if(shopMapper.updateShop(shop)>0)
					increase=true;
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			if(!increase&&shopMapper.IncreaseShop(shop)>0)
				increase=true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return increase;
	}
	@Override
	public List<Product> shoppingCart(Integer buyerid) {
		// TODO Auto-generated method stub
		List<Product> list=new ArrayList<Product>();
		try {
			list=shopMapper.shoppingCart(buyerid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	@Override
	public boolean modifyShopping(Integer buyerid, Integer count, Integer proId) {
		// TODO Auto-generated method stub
		try {
			if(shopMapper.modifyShopping(buyerid, count, proId)>0)
				return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	@Override
	public boolean delShopping(Integer buyerid, Integer proId) {
		// TODO Auto-generated method stub
		try {
			if(shopMapper.delShopping(buyerid,  proId)>0)
				return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	@Override
	public boolean Togenerateorders(HttpSession session,Double oAmount,String tempAddress,String message) {
		// TODO 查询出符合条件的商品，然后再生成订单，改变商品状态
		//List<Shop> list=new ArrayList<Shop>();
		try {
			User user=(User) session.getAttribute("user");
			//生成订单
			Order order=new Order();
			if(user!=null){
				order.setoTime(new Date());
				order.setoAmount(oAmount);
					order.setSeller(1);//设置卖家
				if(tempAddress!=null&&tempAddress!="")
					order.setReAddress(tempAddress);
				else if(user.getAddress()!=null)
					order.setReAddress(user.getAddress());
				if(user.getId()!=null)
					order.setBuyer(user.getId());
				order.setoStatus(1);
				if(message!=null)
					order.setMessage(message);
				if(user.getId()!=null)
				order.setOrderNo(Auxiliary.generateOrderSerial(user.getId()));
				shopMapper.generate(order);
				//修改购物和商品属性
				Shop shop =new Shop();
				shop.setoId(order.getId());
				if(user.getId()!=null)
					shop.setBuyer(user.getId());
				if(shopMapper.modifyStatus(shop)>0){
					//对商品进行减
					//生成订单后，对商品库存进行减
					//查询出当前订单的详细信息
					List<Shop> proList=new ArrayList<Shop>();
					proList=shopMapper.getOidShop(order.getId());
					for( Shop pro:proList){
						productMapper.reduction(pro.getProId(), pro.getCount());
					}
				}
					return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
}
