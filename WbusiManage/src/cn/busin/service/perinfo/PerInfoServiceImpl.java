package cn.busin.service.perinfo;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.busin.dao.perinfo.PerInfoMapper;

@Service
public class PerInfoServiceImpl implements PerInfoService{
		@Resource
		private PerInfoMapper perInfoMapper;

		@Override
		public boolean modifyPerInfo() {
			// TODO Auto-generated method stub
			boolean mod=false;
			try {
				if(perInfoMapper.modifyPerInfo()>0)
					mod=true;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return mod;
		}
}
