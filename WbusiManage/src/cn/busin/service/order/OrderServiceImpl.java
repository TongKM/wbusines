package cn.busin.service.order;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import cn.busin.dao.order.OrderMapper;
import cn.busin.dao.product.ProductMapper;
import cn.busin.dao.shop.ShopMapper;
import cn.busin.dao.user.UserMapper;
import cn.busin.pojo.Order;
import cn.busin.pojo.Product;
import cn.busin.pojo.Shop;
import cn.busin.pojo.Temporary;
import cn.busin.pojo.User;
import cn.busin.tool.Auxiliary;
import cn.busin.tool.Pages;

@Service
public class OrderServiceImpl implements OrderService {

	@Resource
	private OrderMapper orderMappper;
	@Resource
	private ShopMapper shopMapper;
	@Resource
	private ProductMapper productMapper;
	@Resource
	private UserMapper userMapper;
	@Override
	public List<Order> Buyorder(HttpSession session) {
		// TODO Auto-generated method stub
		List<Order> listOrder = new ArrayList<Order>();
		User user = (User) session.getAttribute("user");
		if (null != user) {
			try {
				// 订单--临时商品--商品
				listOrder = orderMappper.Buyorder(user.getId());
				for (Order order : listOrder) {
					// -临时商品
					List<Shop> shopList = shopMapper.orderidByshop(order
							.getId());
					for (Shop shop : shopList) {
						// 商品proId
						Product product = productMapper.shopByproduct(shop
								.getProId());
						shop.setPro(product);
					}
					order.setShopList(shopList);
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return listOrder;
	}

	@Override
	public List<Order> Sellorder(HttpSession session) {
		// TODO Auto-generated method stub
		List<Order> list = new ArrayList<Order>();
		User user = (User) session.getAttribute("user");
		if (null != user) {
			try {
				list = orderMappper.Sellorder(user.getId());
				for (Order order : list) {
					// -临时商品
					List<Shop> shopList = shopMapper.orderidByshop(order
							.getId());
					for (Shop shop : shopList) {
						// 商品proId
						Product product = productMapper.shopByproduct(shop
								.getProId());
						shop.setPro(product);
					}
					if (order.getSeller() != null)
						order.setUser(orderMappper.superUser(order.getBuyer()));
					order.setShopList(shopList);
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return list;
	}

	@Override
	public boolean closeTrading(Integer oId) {
		// TODO 关闭交易
		boolean close = false;
		// 删除shop,order
		try {
			//对商品进行加
			//生成订单后，对商品库存进行减
			//查询出当前订单的详细信息
			List<Shop> proList=new ArrayList<Shop>();
			proList=shopMapper.getOidShop(oId);
			for( Shop pro:proList){
				productMapper.add(pro.getProId(), pro.getCount());
			}
			if (!(shopMapper.delOidShop(oId) > 0))
				return false;
			if (orderMappper.delOidOrder(oId) > 0)
				close = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return close;
	}

	@Override
	public boolean completeTrading(Integer oId) {
		// TODO Auto-generated method stub
		try {
			orderMappper.completeTrading(oId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public User superUser(HttpSession session) {
		// TODO Auto-generated method stub
		User user = (User) session.getAttribute("user");
		if (user.getSuperior() != null)
			return orderMappper.superUser(user.getSuperior());
		return null;
	}

	@Override
	public Temporary report(HttpSession session) {
		// TODO Auto-generated method stub
		Temporary template = new Temporary();
		List<Temporary> list = new ArrayList<Temporary>();
		User user = (User) session.getAttribute("user");
		try {
			if (user.getSuperior() != null) {
				list = orderMappper.reportBuyer(user.getId());
				template.setBuyer(list);
				list.add(template);
			}
			if (user.getSuperior() != null) {
				list = orderMappper.reportSeller(user.getId());
				template.setSeller(list);
				list.add(template);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Auxiliary.conversionTemporary(template);
		//return template;

	}
	
	@Override
	public Double queryMonthTotal() {
		// TODO Auto-generated method stub
		return orderMappper.getMonthTotal();
	}
	
	@Override
	public Integer queryMonthCount() {
		// TODO Auto-generated method stub
		return orderMappper.getMonthCount();
	}

	@Override
	public List<Order> rankingThree(Integer userId, String attrRanking) {
		// TODO rankingThree排名,attrRanking需要设置属性 3个属性(Lastmonth,Thismonth,totallist)
		List<Order> list=new ArrayList<Order>();
		try {
			if(attrRanking!=null&&attrRanking.equals("Lastmonth"))
				list=orderMappper.lastmonth(userId);
			if(attrRanking!=null&&attrRanking.equals("Thismonth"))
				list=orderMappper.thismonth(userId);
			if(attrRanking!=null&&attrRanking.equals("totallist"))
				list=orderMappper.totallist(userId);
		} catch (Exception e) {
			// TODO: handle exception·
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Order> getManageOrderList(String conditions, Pages page) {
		// TODO Auto-generated method stub
		if(conditions==null)
			conditions="";
		List<Order> list=new ArrayList<Order>();
			try {
				
					List<Order>	listb=orderMappper.getManageOrderList(conditions, page.getFrom(),page.getPageSize());
					for (Order order : listb) {
						List<Shop> shopb=new ArrayList<Shop>();
						order.setUser(userMapper.queryUser(order.getBuyer()));
						List<Shop> shop=shopMapper.orderidByshop(order.getId());
						for (Shop shop2 : shop) {
							Product pro=new Product();
							pro=productMapper.shopByproduct(shop2.getProId());
							shop2.setPro(pro);
							shopb.add(shop2);
						}
						order.setShopList(shopb);
						list.add(order);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return list;
	}
	
	
	@Override
	public Integer getManageOrderCount(String conditions) {
		// TODO Auto-generated method stub
		Integer count = orderMappper.getManageOrderCount(conditions);
		return count;
	}

	@Override
	public List<Order> statisticalOrder(int statistical) {
		// TODO Auto-generated method stub
		List<Order> order=new ArrayList<Order>();
		try {
			List<Order> orderb=orderMappper.statisticalOrder(statistical);
			if(orderb!=null){
				for (Order order2 : orderb) {
					Order or=new Order();
					Product pro=productMapper.getProduct(Integer.parseInt(order2.getProId()));
					if(pro!=null&&pro.getProName()!=null){
						or.setoCount(order2.getoCount());
						or.setProId(pro.getProName());
						order.add(or);
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return order;
	}

	@Override
	public void date() {
		// TODO Auto-generated method stub
		try {
			List<Order> list=orderMappper.getListOrder();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	        Date date=new Date();  
	        Calendar calendar = Calendar.getInstance();  
	        calendar.setTime(date);  
	        calendar.add(Calendar.DATE, -1);  
	        date = calendar.getTime();  
	        System.out.println("一天后时间："+sdf.format(date));  
			for(Order order:list){
				Date date2=order.getoTime();
				if(date2.getMonth()+date2.getDay()+date2.getHours()+date2.getMinutes()+date2.getSeconds()>=sdf.MONTH_FIELD+sdf.DAY_OF_YEAR_FIELD+sdf.HOUR_OF_DAY1_FIELD+sdf.MINUTE_FIELD+sdf.SECOND_FIELD)
					this.closeTrading(order.getId());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}