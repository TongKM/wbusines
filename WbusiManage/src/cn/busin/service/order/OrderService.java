package cn.busin.service.order;

import java.util.List;

import javax.servlet.http.HttpSession;

import cn.busin.pojo.Order;
import cn.busin.pojo.Temporary;
import cn.busin.pojo.User;
import cn.busin.tool.Pages;

public interface OrderService {
	//订单分类查看：买入订单Buy orders
	public List<Order> Buyorder(HttpSession session);
	//订单分类查看：卖出订单Sell orders
	public List<Order> Sellorder(HttpSession session);
	//关闭交易
	public boolean closeTrading(Integer oId);
	//完成交易
	public boolean completeTrading(Integer oId);
	//返回上级信息
	public User superUser(HttpSession session);
	//报表统计report  Temporary创建的临时存储报表信息(买入，卖出)
	public Temporary report(HttpSession session);
	//本月成交总金额
	public Double queryMonthTotal();
	//本月成交量
	public Integer queryMonthCount();
	//rankingThree排名,attrRanking需要设置属性 3个属性(Lastmonth,Thismonth,totallist)
	public List<Order> rankingThree(Integer userId, String attrRanking);
	
	/**
	 * 管理端我的订单查询
	 * @param conditions
	 * @param userId
	 * @param page
	 * @return
	 */
	public List<Order> getManageOrderList(String conditions,Pages page);
	

	/**
	 * 管理端我的订单总记录数
	 * @param conditions
	 * @param userId
	 * @return
	 */
	public Integer getManageOrderCount(String conditions);
	
	//管理：销量统计statistical
	public List<Order> statisticalOrder(int statistical);
	public void date();
	
}
