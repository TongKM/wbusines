package cn.busin.service.product;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import cn.busin.dao.product.ProductMapper;
import cn.busin.pojo.Product;
import cn.busin.tool.PicPath;
@Service
public class ProductServiceImpl implements ProductService{
	@Resource
	private ProductMapper productMapper;
	
	@Override
	public List<Product> find(String proName, Integer uId) {
		// TODO Auto-generated method stub
		return productMapper.find(proName, uId);
	}

	@Override
	public Integer count(String proName, Integer uId) {
		// TODO Auto-generated method stub
		return productMapper.count(proName, uId);
	}

	@Override
	public List<Product> allProduct(String nameofThe, String status) {
		// TODO Auto-generated method stub
		List<Product> list=new ArrayList<Product>();
		if(nameofThe==null)
			nameofThe="";
		try {
			list=productMapper.allProduct(nameofThe, status);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public Product getProduct(int id) {
		// TODO Auto-generated method stub根据id查询
		Product product=new Product();
		try {
			product=productMapper.getProduct(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return product;
	}

	@Override
	public boolean saveProduct(Product product) {
		// TODO 未调用dao层代码，dao层代码已编写,新增时注意不要少数据...
		try {
			product.setProStatus(1);
			if(productMapper.addProduct(product)>0)
				return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean updateProduct(Product product,MultipartFile attachs,HttpServletRequest request) {
		// TODO 修改信息（产品）
		try {
			if(attachs!=null){
				String fileName=productMapper.getProduct(product.getId()).getProPicPath();
				if(fileName!=null&&fileName!=""){
					PicPath.deleteFile(fileName,request);
				}
					
				product.setProPicPath(PicPath.getPath(attachs, request));
			}
			if(productMapper.updateProduct(product)>0)
				return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delProduct(Integer id,HttpServletRequest request) {
		// TODO Auto-generated method stub删除
		try {
				String fileName=productMapper.getProduct(id).getProPicPath();
				if(fileName!=null&&fileName!="")
					PicPath.deleteFile(fileName,request);
			if(productMapper.delProduct(id)>0)
				return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public Integer daiShangjia() {
		// TODO Auto-generated method stub
		return productMapper.daiShangjia();
	}

	@Override
	public Integer daiBuhuo() {
		// TODO Auto-generated method stub
		return productMapper.daiBuhuo();
	}

	@Override
	public List<Product> findByDown() {
		// TODO Auto-generated method stub
		return productMapper.findByDown();
	}

	@Override
	public List<Product> findByLess() {
		// TODO Auto-generated method stub
		return productMapper.findByLess();
	}
}
