package cn.busin.service.product;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;


import cn.busin.pojo.Product;

public interface ProductService {
	//查看上级产品(根据产品名称模糊查询，上级id精确查询)
	public List<Product> find(String proName,Integer uId);
	//根据条件查询总记录数
	public Integer count(String proName,Integer uId);
	//模糊查询产品信息 nameofThe 查询条件
	public List<Product> allProduct(String nameofThe, String status);
	//查询产品的详细信息getProduct
	public Product getProduct(int id);
	//新增产品saveProduct
	public boolean saveProduct(Product product);

	//修改产品信息updateProduct 
	public boolean updateProduct(Product product,MultipartFile attachs,HttpServletRequest request);
	//删除 产品id 删除
	public boolean delProduct(Integer id,HttpServletRequest request);
	
	/**
	 * 待上架产品数量
	 * @return
	 */
	public Integer daiShangjia();
	
	/**
	 * 待补货产品数量
	 * @return
	 */
	public Integer daiBuhuo();
	//查询已下架产品集合
	public List<Product> findByDown();
	//查询需补货产品集合
	public List<Product> findByLess();
}
