package cn.busin.service.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.busin.dao.user.UserMapper;
import cn.busin.pojo.User;
import cn.busin.pojo.UserDetailed;
@Service
public class UserServiceImpl implements UserService {
	@Resource
	private UserMapper userMapper;
	@Override
	public User loginByE(String email, String password) {
		// TODO Auto-generated method stub
		return userMapper.loginByE(email, password);
	}

	@Override
	public User loginByP(String phone, String password) {
		// TODO Auto-generated method stub
		return userMapper.loginByP(phone, password);
	}

	@Override
	public List<User> getUserlowerlevel(Integer id) {
		// TODO 查看全部下级代理
		List<User> list=new ArrayList<User>();
		try {
			list=userMapper.getUserlowerlevel(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public boolean upUser(User user) {
		// TODO Auto-generated method stub
		boolean upu=false;
		try {
			if(userMapper.upUser(user)>0)
				upu =true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return upu;
	}

	
	@Override
	public User queryUser(Integer id) {
		// TODO Auto-generated method stub
		return userMapper.queryUser(id);
	}

	@Override
	public Integer queryMonthAgencyCount() {
		// TODO Auto-generated method stub
		return userMapper.getMonthAgencyCount();
	}

	@Override
	public Integer queryMonthGrantCount() {
		// TODO Auto-generated method stub
		return userMapper.getMonthGrantCount();
	}

	@Override
	public Integer queryAllGrantCount() {
		// TODO Auto-generated method stub
		return userMapper.getAllGrantCount();
	}

	@Override
	public Integer queryNoExamineCount() {
		// TODO Auto-generated method stub
		return userMapper.getNoExamineCount();
	}

	@Override
	public List<User> waitingUser() {
		// TODO Auto-generated method stub
		try {
			return userMapper.waitingUser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<User> refusedUser() {
		// TODO Auto-generated method stub
		try {
			return userMapper.refusedUser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean audit(int id, int state,Date grantDate) {
		// TODO Auto-generated method stub
			try {
				if(userMapper.audit(id,state,grantDate)>0)
					return true;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return false;
	}

	@Override
	public int countAgentUser(String name) {
		// TODO Auto-generated method stub
		int count=0;
		try {
			count=userMapper.countAgentUser(name);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public List<User> agentUserList(int starting, String name) {
		// TODO Auto-generated method stub
		List<User> list=new ArrayList<User>();
		try {
			list=userMapper.agentUserList(starting, name);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public UserDetailed getUserDetailed(int id) {
		// TODO Auto-generated method stub
		UserDetailed userDetailed=new UserDetailed();
		User  user=this.queryUser(id);
		userDetailed.setId(user.getId());
		userDetailed.setName(user.getName());
		userDetailed.setPhone(user.getPhone());
		userDetailed.setEmail(user.getEmail());
		userDetailed.setAddress(user.getAddress());
		try {
			UserDetailed	userDetaileds=	userMapper.getUserDetailed(id);
			if(userDetaileds!=null){
				userDetailed.setTotalAmount(userDetaileds.getTotalAmount());
				userDetailed.setTotalCount(userDetaileds.getTotalCount());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userDetailed;
	}
}
