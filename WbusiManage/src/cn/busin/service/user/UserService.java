package cn.busin.service.user;

import java.util.Date;
import java.util.List;

import cn.busin.pojo.User;
import cn.busin.pojo.UserDetailed;

public interface UserService {
	// 邮箱登录
	User loginByE(String email, String password);

	// 手机号登陆
	User loginByP(String phone, String password);

	// 查看下级审核名单
	public List<User> getUserlowerlevel(Integer id);

	// 修改
	public boolean upUser(User user);

	// 根据id查询user
	public User queryUser(Integer id);

	/**
	 * 查询本月申请代理数
	 * 
	 * @return
	 */
	public Integer queryMonthAgencyCount();

	/**
	 * 查询本月授权数
	 * 
	 * @return
	 */
	public Integer queryMonthGrantCount();

	/**
	 * 查询代理总人数
	 * 
	 * @return
	 */
	public Integer queryAllGrantCount();

	/**
	 * 待审核代理
	 * 
	 * @return
	 */
	public Integer queryNoExamineCount();

	// Waiting等待审核名单
	public List<User> waitingUser() ;

	// refusedUser拒绝名单
	public List<User> refusedUser() ;
	//审核audit state状态（）。。。1-3
	public boolean audit(int id,int state,Date grantDate);
	//代理管理
	//符合条件的所以数量
	public int countAgentUser(String name);
	//分页查询 起始位置
	public List<User> agentUserList(int starting,String name);
	//个人信息
	public UserDetailed getUserDetailed(int id);
	
}
