package cn.busin.service.reg;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.busin.dao.reg.RegUserMapper;
import cn.busin.pojo.User;

@Service
public class RegUserServiceImpl implements RegUserService{
@Resource
private RegUserMapper tRegUserMapper;

@Override
public User regUserSave(User user) {
	// TODO Auto-generated method stub
	if(null!=user){
		user.setRegDate(new Date());
		user.setStatus(2);
		user.setType(2);
		user.setName(user.getEmail());
		try {
			tRegUserMapper.regUserSave(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	return user;
}

@Override
public boolean regexist(String emil) {
	// TODO Auto-generated method stub
	boolean exist=false;
	try {
		int i=tRegUserMapper.regexist(emil);
		if(i==0){
			exist=true;
		}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return exist;
}

}
