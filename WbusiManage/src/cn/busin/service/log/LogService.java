package cn.busin.service.log;

import java.util.List;

import javax.servlet.http.HttpSession;

import cn.busin.pojo.Log;

public interface LogService {
	//日志录入
	public boolean entryLog(HttpSession session,String logDetail);
	public boolean entryLog(Integer id,String logDetail);
	
	/**
	 * 日志查询
	 * @param 用户名
	 * @param 页码
	 * @param 页面容量
	 * @return
	 */
	public List<Log> queryLog(String userName, Integer pageNo, Integer pageSize);
	
	/**
	 * 日志总记录数
	 * @param userName
	 * @return
	 */
	public Integer queryLogCount(String userName);
	
	/**
	 * 查询昨日上线人数
	 * @return
	 */
	public Integer queryYesterdayLevel();
	
	/**
	 * 查询昨日上线次数
	 * @return
	 */
	public Integer queryYestedayTime();
	
	/**
	 * 近期未上线
	 * @return
	 */
	public Integer queryNearFuture();
}
