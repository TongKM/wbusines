package cn.busin.service.log;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import cn.busin.dao.log.LogMapper;
import cn.busin.pojo.Log;
import cn.busin.pojo.User;

@Service
public class LogServiceImpl implements LogService{
	@Resource
	private LogMapper logMapper;
	@Override
	public boolean entryLog(HttpSession session, String logDetail) {
		// TODO Auto-generated method stub
		Log log=new Log();
		log.setuId(((User)session.getAttribute("user")).getId());
		log.setOperTime(new Date());
		log.setLogDetail(logDetail);
		try {
			if(logMapper.entryLog(log)>0)
				return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	@Override
	public boolean entryLog(Integer id, String logDetail) {
		// TODO Auto-generated method stub
		Log log=new Log();
		log.setuId(id);
		log.setOperTime(new Date());
		log.setLogDetail(logDetail);
		try {
			if(logMapper.entryLog(log)>0)
				return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public List<Log> queryLog(String userName, Integer pageNo, Integer pageSize) {
		// TODO Auto-generated method stub
		if(0==pageNo){
			pageNo=1;
		}
		return logMapper.queryLog(userName, (pageNo-1)*pageSize, pageSize);
	}
	@Override
	public Integer queryLogCount(String userName) {
		// TODO Auto-generated method stub
		return logMapper.queryLogCount(userName);
	}
	@Override
	public Integer queryYesterdayLevel() {
		// TODO Auto-generated method stub
		return logMapper.queryYesterdayLevel();
	}
	
	@Override
	public Integer queryYestedayTime() {
		// TODO Auto-generated method stub
		return logMapper.queryYestedayTime();
	}
	
	@Override
	public Integer queryNearFuture() {
		// TODO Auto-generated method stub
		return logMapper.queryNearFuture();
	}
	
}
