package cn.busin.controller;

import javax.annotation.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.busin.pojo.User;
import cn.busin.service.log.LogService;
import cn.busin.service.reg.RegUserService;
import cn.busin.tool.Mail;
import cn.busin.tool.Encryption;

@RequestMapping("/reg")
@Controller
public class RegUserController {
	//查看下级代理/申请取消下级代理资格 Personal information个人信息
	//邮件注入
	@Resource
	private JavaMailSender mailSender; 
	@Resource
	private RegUserService tRegUserService;
	
	@Resource
	private LogService logService;
	
	@RequestMapping("/RegisterWithTel")
	public ModelAndView regUser(String Tel,HttpServletRequest request){
		ModelAndView model=new ModelAndView();
		if(Tel!=null&&Tel!="")
		{
			Mail mail=new Mail();
			if(tRegUserService.regexist(Tel)&&mail.sendMail(mailSender,Tel)){
				model.setViewName("/Account/Login");
			}else{
				model.addObject("error", 1);
				model.addObject("emil", Tel);
				model.setViewName("/Account/Register");
			}
		}
		return model;
	}
	
	@RequestMapping("/cdMail")
	public ModelAndView cdMail(String mailName){
		ModelAndView model=new ModelAndView("/Account/RegisterUsers");
		if(null!=mailName)
			model.addObject("mailNmae", mailName);
		return model;
	}
	
	@RequestMapping("/RegisterWithTelTo")
	public ModelAndView regUserSave(User user,String Tel,HttpServletRequest request,
			HttpSession session){
		ModelAndView model=new ModelAndView();
		user.setPassword(Encryption.getResult(user.getPassword()));
		if(null!=Tel)
			user.setEmail(Tel);
		if(tRegUserService.regUserSave(user)!=null)
			model.setViewName("/Account/Login");
		else
			model.setViewName("/error");
		session.setAttribute("user", user);
		logService.entryLog(session, "申请了代理！");
		session.removeAttribute("user");
		//实现用户申请操作
		return model; 
	}
}
