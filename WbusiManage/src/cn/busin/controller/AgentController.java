package cn.busin.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;

import cn.busin.pojo.Temporary;
import cn.busin.pojo.User;
import cn.busin.pojo.UserDetailed;
import cn.busin.service.log.LogService;
import cn.busin.service.order.OrderService;
import cn.busin.service.user.UserService;
import cn.busin.tool.Pages;

@Controller
@RequestMapping("/sys/agent")
public class AgentController {
	
	@Resource
	private UserService userService;
	@Resource
	private OrderService orderService;
	@Resource
	private LogService logService;
	private Pages page=new Pages();
	@RequestMapping("/find")
	public String find(Model model,HttpSession session){//代理端查看下级代理
		Integer id=((User)session.getAttribute("user")).getId();
		List<User> userList=userService.getUserlowerlevel(id);
		model.addAttribute("userList", userList);
		return "/agent/agency";
	}
	
	@RequestMapping(value="/chart")
	@ResponseBody
	public String chart(HttpSession session){
		Temporary tem=orderService.report(session);
		HashMap<String, Temporary> map=new HashMap<String, Temporary>();
		map.put("tem", tem);
		
		return JSONArray.toJSONString(map);
	}
	@RequestMapping("/VerifyList")
	public String VerifyList(Model model){//跳转到待审核页
		//查询出所有未审核代理
		List<User> list=userService.waitingUser();
		model.addAttribute("list", list);
		return "Manage/VerifyList";
	}
	@RequestMapping("/RefuseAndRebackAgency")
	public String RefuseAndRebackAgency(Model model){//跳转到已拒绝页
		//查询出所有已拒绝代理
		List<User> list=userService.refusedUser();
		model.addAttribute("list", list);
		return "Manage/RefuseAndRebackAgency";
	}
	@RequestMapping("/verify")
	public String verify(Integer id,Model model){//进入审核页面
		User user=userService.queryUser(id);
		model.addAttribute("user", user);
		return "Manage/Verify";
	}
	@RequestMapping("/agree")
	public String agree(Integer id,Integer state){//点击同意时进入
		userService.audit(id, state,new Date());
		logService.entryLog(id, "被审核通过！");
		return "redirect:/sys/agent/VerifyList";
	}
	@RequestMapping("/refuse")
	public String refuse(Integer id,Integer state){//点击拒绝时进入
		userService.audit(id, state,null);
		logService.entryLog(id, "审核不通过！");
		return "redirect:/sys/agent/VerifyList";
	}
	@RequestMapping("/All")
	public String all(Model model){//刚进入代理管理页面时
		//分页获取第一页的代理列表(参数起始位置0,每页条数5)
		
		//向model里存放第一页的代理信息集合
		
		return "Manage/AgenciesAll";
	}
	
	@RequestMapping("/mFind")
	@ResponseBody
	public Object mFind(String searchWord,Integer pageIndex){//管理端根据名字模糊查询代理信息
		HashMap<String, Object> map=new HashMap<String, Object>();
		if(null==pageIndex || pageIndex<1){
			pageIndex=1;
		}
		//先获取总记录数
		page.setTotalCount(userService.countAgentUser(searchWord));
		page.setPageSize(4);
		page.setPageIndex(pageIndex);
		Integer totalPageCount=page.getTotalPageCount();
		if(pageIndex>totalPageCount){
			pageIndex=totalPageCount;
		}
		//根据searchWord和开始位置查询当前页的代理信息集合
		List<User> agentList=userService.agentUserList(page.getFrom(), searchWord);
		//向map里存放当前页的代理信息集合，和分页相关数据(总记录数，总页数，当前页数)
		map.put("agentList", agentList);
		map.put("totalCount", page.getTotalCount());
		map.put("totalPageCount", totalPageCount);
		map.put("pageIndex", pageIndex);
		return map;
	}
	@RequestMapping("/AgencyInfo")
	public String AgencyInfo(Integer id,Model model){//跳转到代理详情页
		//根据id查询代理信息
		UserDetailed user=userService.getUserDetailed(id);
		//获取该代理所有订单总金额
		
		//获取该代理所有订单的商品总件数
		model.addAttribute("user", user);
		return "Manage/AgencyInfo";
	}
	@RequestMapping("/cancel")
	public String cancel(Integer id,Model model){//撤销代理资格
		//根据id撤销代理资格
		userService.audit(id, 4,null);
		logService.entryLog(id, "被撤销代理资格！");
		
		return "Manage/AgenciesAll";
	}
	
}
