package cn.busin.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.busin.pojo.Product;
import cn.busin.pojo.User;
import cn.busin.service.product.ProductService;
import cn.busin.service.shop.ShopService;

@Controller
@RequestMapping("sys")
public class CartController {//导航条跳转
	
	@Resource
	private ShopService shopService;
	@Resource
	private ProductService productService;
	
	@RequestMapping("/cart/list")
	public String cartList(Model model,HttpSession session) {//进入购物车页
		//根据买家id(并且oId为空)查询出购物车里的所有商品
		Integer buyer=((User)session.getAttribute("user")).getId();

		List<Product> list=shopService.shoppingCart(buyer);
		//List<Product> list=productService
		//model.addAttribute("list", list);
		session.setAttribute("cartList", list);
		//flag用于标识红点显示状态
		session.setAttribute("flag", "0");
		model.addAttribute("pagePath", "/sys/agent/cart");//agent/cart位于ForwardController
		return "agent/app";

	}
	@RequestMapping("/cart/EditCart")
	public String EditCart(Integer proId,Integer count,Integer buyerid){
		shopService.modifyShopping(buyerid, count, proId);
		return "agent/cart";
	}
	@RequestMapping("/cart/delPro")
	public String delPro(Integer proId,Integer buyerid){
		shopService.delShopping(buyerid, proId);
		return "agent/cart";
	}
	@RequestMapping("/shopping/list")
	public String mainList(Model model,HttpSession session) {
		//进入主页(agent/shop位于ForwardController)
		model.addAttribute("pagePath", "/sys/agent/shop");
		User user=(User)session.getAttribute("user");
		List<Product> list=productService.find(null, user.getSuperior());
		session.setAttribute("list", list);
		return "agent/app";
	}
	
	@RequestMapping("/agency/person")
	public String personList(Model model) {
		//进入个人信息页
		model.addAttribute("pagePath", "/sys/agent/person");
		return "agent/app";
	}
}
