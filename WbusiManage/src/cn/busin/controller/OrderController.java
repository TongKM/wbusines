package cn.busin.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.busin.pojo.Order;
import cn.busin.pojo.User;
import cn.busin.service.order.OrderService;
import cn.busin.service.shop.ShopService;
import cn.busin.service.user.UserService;
import cn.busin.tool.Pages;

import com.alibaba.fastjson.JSONArray;

@Controller
@RequestMapping("/sys/order")
public class OrderController {//订单
	@Resource
	private OrderService orderService;
	@Resource
	private ShopService shopService;
	@Resource
	private UserService userService;
	private Pages page=new Pages();
	@RequestMapping("/BuyOrder")
	public String Buyorder(@RequestParam(value="oStatus",required=false)Integer oStatus,
			HttpSession session,Model model){
		//默认查询全部买入订单
		List<Order> orderList=orderService.Buyorder(session);
		session.setAttribute("orderList", orderList);
		if(null==oStatus || 0==oStatus){
			model.addAttribute("active", "all");
		}else{
			if(1==oStatus){
				//
				model.addAttribute("status", "1");
				model.addAttribute("active", "pay");
			}else if(2==oStatus){
				//
				model.addAttribute("status", "2");
				model.addAttribute("active", "send");
			}
		}
		return "agent/buyOrder";
	}
	
	@RequestMapping("/closeOrder")
	@ResponseBody
	public Object closeOrder(Integer orderId,Model model){//关闭订单
		String data=null;
		if(orderService.closeTrading(orderId)){
			data="1";
		}else{
			data="0";
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("data", data);
		return JSONArray.toJSONString(map);
	}
	@RequestMapping("/pay")
	public String pay(Integer orderId,Model model,HttpSession session,String oAmount){//付款
		//查询卖家姓名
		User seller=orderService.superUser(session);
		model.addAttribute("oAmount", oAmount);
		model.addAttribute("seller", seller);
		model.addAttribute("orderId", orderId);
		return "agent/orderPay";
	}
	
	@RequestMapping("/payed")
	public String payed(String orderid){
		System.out.println(orderid);
		String[] id = orderid.split("-");
		System.out.println(id[1]);
		orderService.completeTrading(Integer.valueOf(id[1]));
		return "redirect:/sys/order/BuyOrder";
	}
	
	@RequestMapping("/addOrder")
	public String addOrder(String reAddress,Double oAmount,HttpSession session,String message,
			Model model){
		shopService.Togenerateorders(session, oAmount, reAddress,message);
		
		//减少商品库存
		
		
		User user=(User)session.getAttribute("user");
		if(null==user.getAddress() || "".equals(user.getAddress())){
			//当第一次设置收货地址时，要在用户表里添加新地址
			user.setAddress(reAddress);
			userService.upUser(user);
		}
		model.addAttribute("fin", "0");
		return "/agent/cart";
	}
	
	@RequestMapping("/SaleOrder")
	public String Sellorder(/*@RequestParam(value="oStatus",required=false)Integer oStatus,*/
			HttpSession session,Model model){
		//查询卖出订单
		List<Order> orderList=orderService.Sellorder(session);
		//查询买家信息
		User user=orderService.superUser(session);
		model.addAttribute("orderList", orderList);
		/*if(null==oStatus || 0==oStatus){
			model.addAttribute("active", "all");
		}else{
			if(1==oStatus){
				//按状态查询买入订单
				model.addAttribute("status", "1");
				model.addAttribute("active", "pay");
			}else if(2==oStatus){
				//按状态查询买入订单
				model.addAttribute("status", "2");
				model.addAttribute("active", "send");
			}
		}*/
		return "agent/saleOrder";
	}
	
	@RequestMapping("/sendOrder")
	public String SaleOrder(Model model, String searchName, Integer pageNo){//进入订单
		/*if(pageNo==null)
			pageNo=1;
		//Pages page = new Pages();
		page.setPageIndex(pageNo);
		page.setPageSize(3);
		Integer count = orderService.getManageOrderCount(searchName);
		page.setTotalCount(count);
		List<Order> orderList = orderService.getManageOrderList(searchName, page);
		model.addAttribute("orderlist", orderList);
		model.addAttribute("searchName", searchName);
		model.addAttribute("page",page);*/
		return "/Manage/SaleOrder";
	}
	
	@RequestMapping("/mFind")
	@ResponseBody
	public Object mFind(String searchWord,Integer pageIndex){//管理端根据名字模糊查询代理信息
		HashMap<String, Object> map=new HashMap<String, Object>();
		if(null==pageIndex || pageIndex<1){
			pageIndex=1;
		}
		//先获取总记录数
		page.setTotalCount(orderService.getManageOrderCount(searchWord));
		page.setPageSize(3);
		page.setPageIndex(pageIndex);
		Integer totalPageCount=page.getTotalPageCount();
		if(pageIndex>totalPageCount){
			pageIndex=totalPageCount;
		}
		//根据searchWord和开始位置查询当前页的代理信息集合
		List<Order> orderList = orderService.getManageOrderList(searchWord, page);
		//向map里存放当前页的代理信息集合，和分页相关数据(总记录数，总页数，当前页数)
		map.put("orderList", orderList);
		map.put("totalCount", page.getTotalCount());
		map.put("totalPageCount", totalPageCount);
		map.put("pageIndex", pageIndex);
		return map;
	}
	
	/*@RequestMapping("/findOrder")
	@ResponseBody
	public Object findPro(Integer tradeStatus, String searchName, Integer pageNo, HttpSession session){//管理端按名字模糊查询商品
		List<Order> orderList = orderService.getManageOrderList(searchName, session, pageNo);
		Map<String,Object> result = new HashMap<String, Object>();  
	    result.put("list", orderList);
		return result;
	}*/
	
	@RequestMapping("/toStatisticsPurchasers")
	public String StatisticsPurchasers(Model model,
			HttpSession session,Integer interval){//进入订单销量统计页面
		//session中的interval表示与当前月份的间隔，0表示当前月，1表示推前一个月，如此类推
		if(null==interval){
			interval=0;
		}
		Date dBefore = new Date();
		Calendar calendar = Calendar.getInstance(); //得到日历
		calendar.add(calendar.MONTH, (0-interval));  //设置为前几个月
		dBefore = calendar.getTime();   //得到前几个月的时间
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月"); //将日期格式化
		String date=sdf.format(dBefore);
		model.addAttribute("date", date);
		model.addAttribute("interval", interval);
		return "/Manage/StatisticsPurchasers";
	}
	
	@RequestMapping("/StatisticsPurchasers")
	@ResponseBody
	public Object toStatisticsPurchasers(Integer interval){
		//异步获取订单统计数据
		
		HashMap<String, Object> map=new HashMap<String, Object>();
		
		return JSONArray.toJSONString(map);
	}
	
	@RequestMapping("/StatisticsSold")
	@ResponseBody
	public Object StatisticsSold(HttpSession session,Integer interval){
		//异步获取商品统计数据
		List<Order> orderList=orderService.statisticalOrder(interval);
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("orderList", orderList);
		map.put("interval", interval);
		return JSONArray.toJSONString(map);
	}
	@RequestMapping("/toStatisticsSold")
	public String StatisticsSoldGet(Integer interval,Model model){//进入商品销量统计页面
		//session中的interval表示与当前月份的间隔，0表示当前月，1表示推前一个月，如此类推
		if(null==interval){
			interval=0;
		}
		Date dBefore = new Date();
		Calendar calendar = Calendar.getInstance(); //得到日历
		calendar.add(calendar.MONTH, (0-interval));  //设置为前几个月
		dBefore = calendar.getTime();   //得到前几个月的时间
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月"); //将日期格式化
		String date=sdf.format(dBefore);
		model.addAttribute("date", date);
		model.addAttribute("interval", interval);
		return "/Manage/StatisticsSold";
	}
}