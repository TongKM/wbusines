package cn.busin.controller;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.busin.pojo.User;
import cn.busin.pojo.Log;
import cn.busin.service.log.LogService;
import cn.busin.service.user.UserService;
import cn.busin.tool.Pages;

@Controller
@RequestMapping("/sys/manage")
public class ManageController {
	@Resource
	private UserService userService;
	private Pages page=new Pages();
	@Resource
	private LogService logService;
	@RequestMapping("/index")
	public String manageIndex(Model model){//跳转到代理主页
		//获取待发货订单数
		
		//获取待审核代理数
		
		//本月申请代理数
		
		//本月授权代理数
		
		//代理总人数
		
		//本月成交金额
		
		//本月成交量
		
		return "redirect:/sys/wang/admin";
	}
	
	@RequestMapping("/StatisticsPurchasers")
	public String StatisticsPurchasers(){
		
		return "/Manage/StatisticsPurchasers";
	}
	
	@RequestMapping("/Manage")
	public String manage(Model model,HttpSession session){//跳转到系统信息维护页面
		//根据id查询用户信息
		return "/Manage/ManageEdit";
	}
	@RequestMapping("/update")
	public String update(User user,HttpSession session){//进行信息修改
		userService.upUser(user);
		session.setAttribute("user", user);
		return "redirect:/sys/wang/admin";
	}
	@RequestMapping("/Log")
	public String Log(){
		
		return "/Manage/SystemLog";
	}
	@RequestMapping("/mFind")
	@ResponseBody
	public Object mFind(String searchWord,Integer pageIndex){
		HashMap<String, Object> map=new HashMap<String, Object>();
		if(null==pageIndex || pageIndex<1){
			pageIndex=1;
		}
		//先获取总记录数
		page.setTotalCount(logService.queryLogCount(searchWord));
		page.setPageSize(5);
		page.setPageIndex(pageIndex);
		Integer totalPageCount=page.getTotalPageCount();
		if(pageIndex>totalPageCount){
			pageIndex=totalPageCount;
		}
		//根据searchWord和当前页码查询当前页的日志信息集合
		List<Log> logList = logService.queryLog(searchWord, pageIndex, 5);
		//向map里存放当前页的代理信息集合，和分页相关数据(总记录数，总页数，当前页数)
		map.put("logList", logList);
		map.put("totalCount", page.getTotalCount());
		map.put("totalPageCount", totalPageCount);
		map.put("pageIndex", pageIndex);
		return map;
		
	}
}
