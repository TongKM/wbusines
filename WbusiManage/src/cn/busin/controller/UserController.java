package cn.busin.controller;

import java.text.DecimalFormat;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.busin.pojo.Product;
import cn.busin.pojo.User;
import cn.busin.service.log.LogService;
import cn.busin.service.order.OrderService;
import cn.busin.service.product.ProductService;
import cn.busin.service.user.UserService;
import cn.busin.tool.Encryption;

@Controller
@RequestMapping("/user")
public class UserController {
	@Resource
	private UserService userService;
	@Resource
	private ProductService productService;
	//@Resource
	//private Pages page;
	@Resource
	private OrderService orderService;
	@Resource
	private LogService logService;
	//登录方法
	@RequestMapping("/Login")
	public String login(@RequestParam(value="info",required=false)String info,
			@RequestParam(value="password",required=false)String password,HttpSession session,
			Model model){
		//将密码加密
		String pass=Encryption.getResult(password);
		User user=null;
		//如果是邮箱登录
		if(null!=info && info.contains("@")){
			//将加密后的密码放到数据库里查询
			user=userService.loginByE(info, pass);
		}
		//如果是手机号登录
		if(null!=info && !info.contains("@")){
			//将加密后的密码放到数据库里查询
			user=userService.loginByP(info, pass);
		}
		if(null==user){//登录信息不正确时
			model.addAttribute("error", "用户名或密码错误！");
			return "/Account/Login";
		}else{
			//登录信息正确后根据用户类型(管理还是代理)跳转到不同页面
			if(null==user.getType()){
				model.addAttribute("error", "该用户未审核成为代理，无法登录！");
				return "/Account/Login";
			}
			if(user.getType()==1){
				//是管理员的时候
				session.setAttribute("user", user);
				session.setAttribute("yesterdayCount", logService.queryYesterdayLevel());
				session.setAttribute("yesterdayTime", logService.queryYestedayTime());
				session.setAttribute("nearFuture", logService.queryNearFuture());
				session.setAttribute("daiShangjia", productService.daiShangjia());
				session.setAttribute("daiBuhuo", productService.daiBuhuo());
				
				//本月客单价
				double kedanji = orderService.queryMonthTotal()/orderService.queryMonthCount();
				DecimalFormat df = new DecimalFormat(".00");
				String kedanjia=df.format(kedanji);
				session.setAttribute("kedanjia", kedanjia);
				logService.entryLog(session, "进入了系统！");
				return "redirect:/sys/wang/admin";
			}else if(user.getType()==2 || user.getType()==3){
				//是代理的时候,判断是否被审核
				if(2==user.getStatus() || 3==user.getStatus() || null==user.getStatus()){
					//未审核或拒绝返回到登录页
					model.addAttribute("error", "该用户无代理权限，无法登录！");
					return "/Account/Login";
				}else{//代理登录成功
					session.setAttribute("user", user);
					return "redirect:/sys/wang/agent";
				}
			}else{
				//不是管理又不是代理时，即没有审核
				model.addAttribute("error", "该用户未审核成为代理，无法登录！");
				return "/Account/Login";
			}
		}
	}
	//退出
	@RequestMapping("/Logout")
	public String Logout(HttpSession session){
		session.invalidate();
		return "redirect:/Account/Login.jsp";
	}
	
}
