package cn.busin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("sys/agent")
public class ForwardController {

	@RequestMapping("/shop")
	public String shop(Model model){
		//登陆进来自动进入首页
		model.addAttribute("active", "shouye");
		return "agent/shopping";
	}
	
	@RequestMapping("/cart")
	public String showCart(Model model) {
		model.addAttribute("active", "gouwu");
		return "/agent/cart";
	}
	
	@RequestMapping("/person")
	public String showPerson(Model model) {
		model.addAttribute("active", "geren");
		return "/agent/person";
	}
	@RequestMapping("/address")
	public String address(){
		return "/agent/address";
	}
	
	@RequestMapping("/Purchasers")
	public String Purchasers(){//进入销量统计页面
		
		return "/agent/statisticsPurchasers";
	}
	@RequestMapping("About")
	public ModelAndView About(){
		
		return new ModelAndView("/agent/about");
	}
}
