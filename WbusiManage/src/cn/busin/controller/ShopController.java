package cn.busin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.busin.dao.shop.ShopMapper;
import cn.busin.pojo.Shop;
import cn.busin.service.shop.ShopService;

@Controller
@RequestMapping("/sys/shop")
public class ShopController {//加入购物车
	@Resource
	private ShopService shopService;
	
	@RequestMapping("/addToCart")
	public String IncreaseShop(Shop shop,HttpSession session){//添加商品到购物车
		//先判断当前商品是否已经被添加过购物车：
		//1.先根据商品id和买家id查询是否存在该商品（还要判断oId是否为空）
		//2.若存在，则根据商品id和买家id将其数量累加
		//flag用于设置小红点是否显示
		session.setAttribute("flag", "1");
		shop.setSeller(1);
		shopService.IncreaseShop(shop);
		return "agent/app";
	}
}
