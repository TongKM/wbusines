package cn.busin.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.busin.service.perinfo.PerInfoService;

@Controller
@RequestMapping("/sys/perinfo")
public class PerInfoController {//个人信息
	@Resource
	private PerInfoService perInfoService;
	@RequestMapping("/")
	public String perinfo(){
		perInfoService.modifyPerInfo();
		return "";
	}
}
