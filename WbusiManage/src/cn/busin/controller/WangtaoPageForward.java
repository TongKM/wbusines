package cn.busin.controller;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import cn.busin.pojo.Order;
import cn.busin.pojo.Product;
import cn.busin.pojo.User;
import cn.busin.service.log.LogService;
import cn.busin.service.order.OrderService;
import cn.busin.service.product.ProductService;
import cn.busin.service.user.UserService;
import cn.busin.tool.Iptool;
import cn.busin.tool.PicPath;

@Controller
@RequestMapping("/sys/wang")
public class WangtaoPageForward {

	@Resource
	private UserService userService;
	
	@Resource
	private OrderService orderService;
	
	@Resource
	private ProductService productService;
	
	@Resource
	private LogService logService;
	@RequestMapping("/person")
	public String detail(Model model, @RequestParam("nextpath")String nextPath) {
		model.addAttribute("pagePath", "/sys/wang/"+nextPath+"");
		return "agent/app";
	}
	
	@RequestMapping("/detail")
	public String personDetail() {
		return "/agent/personDetail";
	}
	
	@RequestMapping("/edit")
	public String editDetail(@ModelAttribute("user")User user) {
		return "/agent/edit";
	}
	
	@RequestMapping("/save")
	public void saveDetail(User user, HttpSession session, HttpServletResponse response) throws Exception {
		PrintWriter out = response.getWriter();
		userService.upUser(user);
		logService.entryLog(session, "修改了个人信息！");
		User us = (User)(session.getAttribute("user"));
		User u = userService.queryUser(us.getId());
		session.setAttribute("user", u);
		out.print("<script>parent.location.href='/WbusiManage/sys/wang/person?nextpath=detail'</script>");
		out.flush();
		out.close();
	}
	
	
	@RequestMapping("/admin")
	public String admin(Model model,HttpServletRequest request,HttpSession session) {
		Double monthTotal = orderService.queryMonthTotal();
		Integer monthCount = orderService.queryMonthCount();
		Integer monthAgency = userService.queryMonthAgencyCount();
		Integer monthGrant = userService.queryMonthGrantCount();
		Integer allGrant = userService.queryAllGrantCount();
		Integer noExamine = userService.queryNoExamineCount();
		String ip = Iptool.getIpAddr(request);
		Integer userId = ((User)session.getAttribute("user")).getId();
		List<Order> lastmonthList = orderService.rankingThree(userId, "Lastmonth");
		List<Order> thismonthList = orderService.rankingThree(userId, "Thismonth");
		List<Order> totalList = orderService.rankingThree(userId, "totallist");
		
		model.addAttribute("lastmonthList", lastmonthList);
		model.addAttribute("thismonthList", thismonthList);
		model.addAttribute("totalList", totalList);
		model.addAttribute("ip", ip);
		model.addAttribute("monthTotal", monthTotal);
		model.addAttribute("monthCount", monthCount);
		model.addAttribute("monthAgency", monthAgency);
		model.addAttribute("monthGrant", monthGrant);
		model.addAttribute("allGrant", allGrant);
		model.addAttribute("noExamine", noExamine);
		model.addAttribute("status", "Admin");
		session.setAttribute("daiShangjia", productService.daiShangjia());
		session.setAttribute("daiBuhuo", productService.daiBuhuo());
		return "/Manage/Admin";
	}
	
	@RequestMapping("/addgood")
	public String addGood(@ModelAttribute("product")Product product) {
		return "/Manage/GoodAdd";
	}
	
	@RequestMapping("/savegood")
	public String saveGood(Product product,MultipartFile attachs, HttpServletRequest request) {
		String path = PicPath.getPath(attachs, request);
		product.setProPicPath(path);
		productService.saveProduct(product);
		return "redirect:/sys/pro/Good";
	}
	
	@RequestMapping("/BuyOrder")
	public String Buyorder(@RequestParam(value="oStatus",required=false)Integer oStatus,
			HttpSession session,Model model){
		//默认查询全部买入订单
		List<Order> orderList=orderService.Buyorder(session);
		session.setAttribute("orderList", orderList);
		if(null==oStatus || 0==oStatus){
			model.addAttribute("active", "all");
		}else{
			if(1==oStatus){
				//
				model.addAttribute("status", "1");
				model.addAttribute("active", "pay");
			}else if(2==oStatus){
				//
				model.addAttribute("status", "2");
				model.addAttribute("active", "send");
			}
		}
		return "agent/buyOrder";
	} 
	
	@RequestMapping("/Purchasers")
	public String Purchasers(){//进入销量统计页面
		return "/agent/statisticsPurchasers";
	}
	
	@RequestMapping("About")
	public ModelAndView About(){
		return new ModelAndView("/agent/about");
	}
	
	@RequestMapping("/agent")
	public String Agent(Model model, HttpSession session) {
		User user = (User)session.getAttribute("user");
		logService.entryLog(session, "进入了系统！");
		//默认读取上级的所有产品
		List<Product> list=productService.find(null, user.getSuperior());
		session.setAttribute("list", list);
		//model.addAttribute("list", list);
		model.addAttribute("pageName", "shopping");
		model.addAttribute("list", list);
		model.addAttribute("pagePath", "/sys/agent/shop");
		return "agent/app";
	}
	
}
