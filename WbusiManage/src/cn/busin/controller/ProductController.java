package cn.busin.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.busin.pojo.Product;
import cn.busin.pojo.User;
import cn.busin.service.product.ProductService;
import cn.busin.tool.Pages;
@Controller
@RequestMapping("/sys/pro")
public class ProductController {//产品
	@Resource
	private ProductService productService;
	@Resource
	private Pages page;
	@RequestMapping("/find")
	@ResponseBody
	public Object find(@RequestParam(value="proName",required=false)String proName,
			@RequestParam(value="uId",required=false)Integer uId,Model model,HttpSession session){
		//用于异步查询商品
		/*Integer count=productService.count(proName, uId);//查询记录总数
		page.setTotalCount(count);
		page.setPageSize(3);				//设置页面容量
		page.setPageIndex(pageIndex);		//设置当前页码
*/		//默认读取上级的所有产品
		List<Product> list=productService.find(proName, uId);
		User user=(User) session.getAttribute("user");
		Map<String,Object> result = new HashMap<String, Object>();  
		result.put("list", list);
		result.put("user", user);
		/*model.addAttribute("pageIndex", pageIndex);
		model.addAttribute("totalCount", count);
		model.addAttribute("totalPageCount", page.getTotalPageCount());*/
		return result;
	}
	
	@RequestMapping("/Good")
	public String Good(Model model, @RequestParam(value="str",required=false)String str){//跳转到商品管理
		//默认查询全部
		List<Product> prolist=productService.allProduct(null,str);
		model.addAttribute("prolist", prolist);
		return "/Manage/Good";
	}
	
	@RequestMapping("/findPro")
	@ResponseBody
	public Object findPro(String proName, @RequestParam(value="str",required=false)String str){//管理端按名字模糊查询商品
		List<Product> list=productService.allProduct(proName,str);
		Map<String,Object> result = new HashMap<String, Object>();  
	    result.put("list", list);
		return result;
	}
	@RequestMapping("/findById")
	public String findById(Integer id,Model model,HttpSession session){
		//根据id获取商品信息，进入商品详情页
		Product pro=productService.getProduct(id);
		session.setAttribute("pro", pro);
		return "/Manage/GoodDetail";
	}
	@RequestMapping("/updatePro")
	public String updatePro(Integer id){
		//根据id获取商品信息，进入商品详情页

		return "/Manage/GoodEdit";
	}
	@RequestMapping("/updateCount")
	public String updateCount(Integer id,HttpSession session){
		//根据id获取商品信息，进入商品详情页
		Product pro=productService.getProduct(id);
		session.setAttribute("pro", pro);
		return "/Manage/GoodEdit";
	}
	@RequestMapping("/update")
	public String update(Product pro,MultipartFile attaches,Model model,HttpServletRequest request){
		//如果没有上传文件，则给attaches赋值null
		if(attaches.getSize()==0){
			attaches=null;
		}
		//执行更新操作
		productService.updateProduct(pro,attaches, request);
		return "redirect:/sys/pro/Good";
	}
	@RequestMapping("/down")
	public String down(Product pro,Model model,
			HttpServletRequest request,HttpSession session){
		//根据proStatus和id进行下架操作
		productService.updateProduct(pro, null, request);
		Product prod=productService.getProduct(pro.getId());
		session.setAttribute("pro", prod);
		return "/Manage/GoodDetail";
	}
	@RequestMapping("/up")
	public String up(Product pro,Model model,
			HttpServletRequest request,HttpSession session){
		//根据proStatus和id进行上架操作
		productService.updateProduct(pro, null, request);
		Product prod=productService.getProduct(pro.getId());
		session.setAttribute("pro", prod);
		return "/Manage/GoodDetail";
	}
	
	@RequestMapping("/del")
	public String del(Integer id,HttpServletRequest request){
		//根据id删除产品
		productService.delProduct(id,request);

		return "redirect:/sys/pro/Good";
	}
	@RequestMapping("/findByDown")
	public String findByDown(Model model){//查询所有已下架的产品
		List<Product> proList=productService.findByDown();
		model.addAttribute("proList", proList);
		return "/Manage/GoodOfDown";
	}
	@RequestMapping("/findByLess")
	public String findByLess(Model model){//查询所有需补货的产品
		List<Product> proList=productService.findByLess();
		model.addAttribute("proList", proList);
		return "/Manage/GoodOfLess";
	}
}
