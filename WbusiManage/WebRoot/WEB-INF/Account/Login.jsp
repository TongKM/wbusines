<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <title>登录 - 代理</title>

    <script type="text/javascript">
        (function appInit() {
            if (top)
                if (top.location.href != self.location.href) {
                    top.location.href = self.location.href;
                }
        })();
    </script>

    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />

    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>

    <link href="/WbusiManage/src/content/pc.css" rel="stylesheet"/>


    <style type="text/css">
        .iframe { background: transparent; box-shadow: none; -webkit-box-shadow: none; -moz-box-shadow: none; top: calc(50% - 400px); }
    </style>

</head>
<body>
<div class="container bootcards-container">
    <div class="iframe">
        <div class="row">

            <div class="login-content">
                <input id="isError" name="isError" type="hidden" value="False" />
                <div class="login-header">
                    <h3 class="login-title hidden-xs"></h3>
                    <div class="login-logo hidden-sm hidden-md hidden-lg show-xs">
                    <h2 style="color: gray;font-style: italic;">
                    <img style="width: 70px;height: 86px" src="/WbusiManage/src/content/images/login-logo.jpg"></h2>
                        <!-- <img src="/WbusiManage/src/content/images/login-logo.png"> -->
                    </div>
                    <div class="pull-right swtich-login">
                        <!-- <div class="icon hidden-xs">
                            <span class="icon-code-new"></span>
                        </div> -->
                        <!-- <span class="login-title-small hidden-sm hidden-md hidden-lg show-xs">二维码登录</span> -->
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <!--  -->
                <div class="login-form">
                    <form action="/WbusiManage/user/Login" class="form-horizontal" method="post" role="form">            <div class="form-group" style="margin-top: 10px;">
                        <label class="sr-only" for="Email">邮箱</label>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="icon icon-mobile-phone2 account-icon"></span></div>
                            <input class="form-control" data-val="true" data-val-required="请输入 邮箱" id="Email" name="info" placeholder="输入邮箱" type="text" value="" />
                        </div>
                    </div>
                        <div class="form-group password-row">
                            <label class="sr-only" for="Password">密码</label>
                            <div class="input-group">
                                <div class="input-group-addon"><span class="icon icon-lock account-icon"></span></div>
                                <input class="form-control" data-val="true" data-val-required="请输入 密码" id="Password" name="password" placeholder="输入密码" type="password" />
                            </div>
                        </div>
                        <div class="form-group hidden-xs remember-row">
                            <input data-val="true" data-val-required="记住我? 字段是必需的。" id="RememberMe" name="RememberMe" type="checkbox" value="true" /><input name="RememberMe" type="hidden" value="false" /> &nbsp;<label for="RememberMe" class="span-remember-me"> 记住我</label>
                        </div>
                        <span style="color: #CC0000">${error}</span>
                        <div id="confirmCodeRow" class="row hidden">
                            <div class="col-md-4 col-xs-6">
                                <input Value="#12#" autocomplete="off" class="form-control" data-val="true" data-val-length="字段 验证码 必须是一个字符串，其最小长度为 4，最大长度为 4。" data-val-length-max="4" data-val-length-min="4" data-val-remote="&#39;验证码&#39; is invalid." data-val-remote-additionalfields="*.ConfirmCode,*.Email" data-val-remote-url="/Validate/VerifyLoginConfirmCode" data-val-required="请输入验证码" id="ConfirmCode" name="ConfirmCode" placeholder="验证码" style="margin-top:2px;margin-left:2px;" type="text" value="" />
                            </div>
                            <div>
                                <label for="ConfirmCode" style="margin-top:3px;">
                                    <img id="confrimCodeImg" src="/WbusiManage/image/ConfirmCode/m.jpg" alt="checkCode" onclick="this.src = '/Image/ConfirmCode/m?' + Math.random()" style="cursor:pointer;" class="img-rounded" title="点击更换验证码" />
                                </label>
                            </div>
                        </div>
                        <div class="validation-summary-valid text-danger" data-valmsg-summary="true"><ul><li style="display:none"></li>
                        </ul></div>            <div class="form-group submit-row">
                            <button type="submit" class="btn btn-desktop-default btn-block"> 登录</button>
                        </div>
                    </form>        <div class="clearfix">&nbsp;</div>
                    <div style="padding:5px 0">
                        还没有申请成为代理？
                        <a href="/WbusiManage/Account/Register.jsp">申请代理！</a>
                    </div>
                    <hr class="split hidden-xs" />
                    <!-- <div class="other-login hidden-xs">
                        <span>其他登录：</span>
                        <a href="/Account/LoginWithTencent">
                            <i class="icon icon-QQ"></i>
                        </a>
                        <a href="/Account/LoginWithSina">
                            <i class="icon icon-weibo"></i>
                        </a>
                    </div> -->
                    <!-- <div class="mobile-other-login hidden-sm hidden-md hidden-lg show-xs">
                        <span class="login-split-title">其他登录</span>
                        <div class="login-media-group">
                            <div class="col-xs-offset-3 col-xs-3 text-center">
                                <a class="login-media media-weibo" href="/Account/LoginWithSina">
                                    <i class="icon icon-weibo"></i>
                                </a>
                                <div class="desc">微博</div>
                            </div>
                            <div class="col-xs-3 text-center">
                                <a class="login-media" href="/Account/LoginWithTencent">
                                    <i class="icon icon-QQ"></i>
                                </a>
                                <div class="desc">QQ</div>
                            </div>
                        </div>
                    </div> -->
                </div>
                <!-- <div class="qr-form" style="display:none;">
                    <div class="qr-title text-center hidden-sm hidden-md hidden-lg show-xs">
                        <h3>二维码登录</h3>
                        <div style="color:#999">请打开手机微信“扫一扫”进行扫码登录</div>
                    </div>
                    <div id="img"></div>
                    <div class="desc hidden-xs">
                        <span class="fa fa-weixin" style="color:rgb(0,193,9)"></span> 打开<span style="color:rgb(120,174,45)">微信</span>“扫一扫”登录
                    </div>
                </div> -->
                
            </div>

        </div>
    </div>
</div>
<script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>

<script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
<script src="/WbusiManage/src/bootstrap-3.3-2.5-dist/js/bootstrap.min.js"></script>
<script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
<script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
<script src="//sso.xuanpin.org/Scripts/zepto.js"></script>
<script src="//sso.xuanpin.org/Scripts/takeit.js"></script>
<script>
    var ss = new ssoImage('047EDCBA-8827-4595-87D1-C141AA2DC897');
    $(function(){
    	$('.login-form').show();
    });
    $('.swtich-login').click(function () {
        $('div').popover('destroy');
        var b = $(this).find('span').hasClass('icon-code-new');
        if (!b) {
            $(this).find('.icon').html('<span class="icon-code-new"></span>');
            $('.login-form').show();
            $('.qr-form').hide();
            $('.login-title').html('密码登录');
            $('.login-title-small').html('二维码登录');
            $('.login-logo img').removeClass('hidden');
            ss.clearInterval();
        }
        else {
            $(this).find('.icon').html('<span class="icon-computer-new"></span>');

            $('.login-form').hide();
            $('.qr-form').show();
            $('.login-title').html('二维码登录');
            $('.login-title-small').html('密码登录');
            $('#img').html('');
            $('.login-logo img').addClass('hidden');

            ss.createLogin('#img');
        }
    });

    var isError = $('input[name="isError"]').val();
    if (isError == "False")
        $('.swtich-login').trigger('click');

</script>
<script type="text/javascript">
    var loginName = $('#Email').val();
    if (loginName.length != 0)
        isNeed();

    var timeoutId;
    $('#Email').on('focusout', function () {
        if (!timeoutId)
            clearTimeout(timeoutId);

        timeoutId = setTimeout(function () {
            isNeed();
            timeoutId = undefined;
        }, 500);
    });


    function isNeed() {
        var loginName = $('#Email').val();
        $.post('/Account/IsNeedLoginConfirmImage?loginName=' + loginName, null, function (result) {
            console.log(result.isNeed);
            if (result.isNeed) {
                $('#confirmCodeRow').removeClass('hidden');
                $('#ConfirmCode').val('');
            }
            else {
                $('#confirmCodeRow').addClass('hidden');
                if ($('#ConfirmCode').val() == '')
                    $('#ConfirmCode').val('#12#');
            }
            rebindValid('#form-signin');
        });
    }
    function handlerSuccess1(err) {
        handlerSuccess(err);
        if (!err.success)
            isNeed();
    }

    function handlerFailure1(err) {
        handlerFailure(err);
        isNeed();
    }


    function rebindValid(formId) {
        var form = $(formId);
        form.removeData("validator")
        form.removeData("unobtrusiveValidation");

        $.validator.unobtrusive.parse(form);
    }
</script>

<script src="/WbusiManage/src/scripts/takeit.js"></script>

  </body>
</html>
