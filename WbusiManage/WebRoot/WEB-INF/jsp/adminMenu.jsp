<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<nav id="adminMenu" class="navmenu offcanvas offcanvas-left">
        <ul class="nav">
            <li class="admin-home-nav">
                <a href="/WbusiManage/sys/manage/index" data-title="首页">
                    <i class="icon icon-home"></i>
                    首页
                </a>
            </li>
                <li>
                    <a href="/WbusiManage/sys/pro/Good" data-title="产品管理">
                        <i class="icon icon-product"></i>
                        产品管理
                    </a>
                </li>
                            <li>
                    <a href="/WbusiManage/sys/order/sendOrder" data-title="我的订单">
                        <i class="icon icon-my-order2"></i>
                        我的订单
                    </a>
                </li>
                            <li>
                    <a href="/WbusiManage/sys/agent/VerifyList" data-title="代理审核">
                        <i class="icon icon-pending-agents"></i>
                        代理审核
                    </a>
                </li>
                            <!-- <li>
                    <a href="/Admin/Refund" data-title="退款管理">
                        <i class="icon icon-refund"></i>
                        退款管理
                    </a>
                </li>
                            <li>
                    <a href="/Admin/Deposit?status=1" data-title="预存款管理">
                        <i class="icon icon-deposit"></i>
                        预存款管理
                    </a>
                </li> -->
                            <li>
                    <a href="/WbusiManage/sys/agent/All" data-title="代理管理">
                        <i class="icon icon-all-agents"></i>
                        代理管理
                    </a>
                </li>
                            <li>
                    <a href="/WbusiManage/sys/order/toStatisticsSold" data-title="销量统计">
                        <i class="icon icon-sales-statistics"></i>
                        销量统计
                    </a>
                </li>
                            <!-- <li>
                    <a href="/Admin/Agencies/StatisticsRegisteredPerson" data-title="代理统计">
                        <i class="icon icon-aents-statistics"></i>
                        代理统计
                    </a>
                </li> -->
               <!--  <li class="hidden-xs">
                    <a href="/Admin/Account/LoginStatistics" data-title="登录统计">
                        <i class="icon icon-login-statistics"></i>
                        登录统计
                    </a>
                </li> -->
                
                <li>
                    <a href="/WbusiManage/sys/manage/Log" data-title="操作日志">
                        <i class="icon icon-journal"></i>
                        操作日志
                    </a>
                </li>
            <li>
                <a href="/WbusiManage/sys/manage/Manage" data-title="公司信息维护">
                    <i class="icon icon-system"></i>
                        公司信息维护
                </a>
            </li>
            	<li>
                    <a href="/WbusiManage/user/Logout" data-title="退出">
                        <i class="icon icon-signin"></i>
                        退出登录
                    </a>
                </li>
        </ul>
    </nav>
    <script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
    <script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/WbusiManage/src/scripts/takeit.js"></script>
    <script src="/WbusiManage/js/app.js"></script>