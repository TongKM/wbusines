<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<input type="hidden" id="biao" value="${active }"/>
<div class="navbar navbar-default navbar-fixed-bottom">
    <div class="container">
        <div class="btn-group">

            <a onclick="parent.location.href='/WbusiManage/sys/shopping/list'" id="shouye" class="btn btn-default">
                <i class="icon-home-o-new"></i>
                首页
            </a>
            
            <a onclick="parent.location.href='/WbusiManage/sys/cart/list'" id="gouwu" class="btn btn-default shoping-cart ">
                <i class="icon-shopping-cart-o-new" style="position:relative;">
                   <span class="shoping-cart-num label" style="margin-left:-2px !important;margin-top:-3px !important;position:absolute;"></span>
                </i>
                购物车
            </a>
            <a onclick="parent.location.href='/WbusiManage/sys/agency/person'" id="geren" class="btn btn-default ">
                <i class="icon-people-o-new" style="position:relative;">
                </i>
                个人中心
            </a>
        </div>
    </div>
</div>
<input type="hidden" id="cart" value="${fn:length(sessionScope.cartList)}"/>

<script type="text/javascript" src="/WbusiManage/src/jquery/jquery-2.1.4.js"></script>

<script type="text/javascript">
	//高亮显示图标
	$(document).ready(function() {
		var val = $("#biao").val();
		$("#"+val+"").addClass("active");
		/* if($("#cart").val() == 0) {
			$(".shoping-cart-num").hide();
		}else{
			$(".shoping-cart-num").show();
		} */
	});
	
</script>
