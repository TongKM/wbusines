<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title> 部落管家</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>

    
</head>
<body class="admin-browse">
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {
                        window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                    }
                } else {
                    top.document.getElementById('page_title_content').innerHTML = '代理信息';
                    if (document.body.clientWidth <= 1100) {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" value="代理信息" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">代理信息</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/Admin/Employee/Details" id="fa_user">
                <i class="fa fa-user" style="font-size:1.6em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
            
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<input data-val="true" data-val-required="JUserId 字段是必需的。" id="JUserId" name="JUserId" type="hidden" value="dcef3f86-f630-4c7f-ac04-aa9f52ff62e5" />
<div class="bootcards-cards">
    <div class="panel panel-default margin">
        <div class="list-group">
            <div class="list-group-item">
                <img src="https://wx.qlogo.cn/mmopen/PiajxSqBRaELvV84DPXFfYAGEBVQAuQBzxgdm5OftCaJ68Xcb3uEFibsa7K43q3w4Rp40iawK8gHd5Q0XlyUoO8RQ/0" class="img-rounded pull-left">
                <label>姓名</label>
                <h4 class="list-group-item-heading">agency_1622584</h4>
            </div>
            <a class="list-group-item" href="/Admin/Agencies/IdentityCard/dcef3f86-f630-4c7f-ac04-aa9f52ff62e5">
                <label>证件照</label>
                <h4 class="list-group-item-heading"></h4>
            </a>
        </div>
    </div>
    <div class="panel panel-default margin">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">代理信息</h3>
        </div>
        <div class="list-group">
            <div class="list-group-item">
                <label>代理级别</label>
                <h4 class="list-group-item-heading">
                    <span>大区代理</span>
                </h4>
            </div>
            <div class="list-group-item">
                <label>授权状态</label>
                    <h4 class="list-group-item-heading">有效</h4>
            </div>
            <div class="list-group-item">
                <label>所属上级</label>
                <h4 class="list-group-item-heading">agency_6756453</h4>
            </div>
            <div class="list-group-item">
                <label>所在区域</label>
                <h4 class="list-group-item-heading">总区</h4>
            </div>
            <div class="list-group-item">
                <label>授权号</label>
                <h4 class="list-group-item-heading">GJ636322762481222398</h4>
            </div>
            <div class="list-group-item">
                <label>授权日期</label>
                <h4 class="list-group-item-heading">2017-06-05</h4>
            </div>
            <div class="list-group-item">
                <label>注册时间</label>
                <h4 class="list-group-item-heading">2017-06-05 16:17:28</h4>
            </div>
        </div>
    </div>
    <div class="panel panel-default margin">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">销售总金额</h3>
        </div>
        <div class="list-group">
            <div class="list-group-item">
                <label>售出总金额</label>
                <h4 class="list-group-item-heading">&#165;1,100,960.00</h4>
            </div>
            <div class="list-group-item">
                <label>买入总金额</label>
                <h4 class="list-group-item-heading">&#165;609,999,984.00</h4>
            </div>
        </div>
    </div>
    <div class="panel panel-default margin">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">推广二维码</h3>
        </div>
        <div class="panel-body">
            <div>
                推广链接：https://w.url.cn/s/AX4AMi9
            </div>
            <div class="text-center">
                <img src="/Admin/QRCode/Image?content=https://w.url.cn/s/AX4AMi9" title="推广二维码" style="width:95%;max-width:320px" />
            </div> 
        </div>
    </div>
</div>

        </div>
    </div>
    <!-- slide in menu (mobile only) -->
    <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>
    
    

    
    <script type="text/javascript">

        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/js/weixinsdk?v=FuHO4EYdVtWj94At6HA29wrWQNt6iYo2nqlZNRPrmqE1"></script>

    <script type="text/javascript">
        if (isWeixin) {
            $.get('/home/getShare', function (data) {
                weixinService.init(data.title, data.desc, data.imgUrl, data.link);
            })
        }
    </script>
</body>
</html>
