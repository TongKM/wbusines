<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>部落管家</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>

    
</head>
<body class=admin-browse>
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {

                        if (document.body.className == 'agency-browse') {
                            window.location.href = '/App#' + window.location.pathname + window.location.search;
                            return;
                        }

                        if (document.body.className == 'admin-browse') {
                            window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                            return;
                        }
                    }
                } else {
                    if (document.body.clientWidth <= 1100 && document.body.className == 'admin-browse') {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" value="agency_1622584的卖出订单" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">agency_1622584的卖出订单 </a>
            </div>
                <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/Admin/Employee/Details" id="fa_user">
                    <i class="fa fa-user" style="font-size:1.6em;"></i>
                </a>
                <a style="position: absolute; top: 12px; right: 45px;" href="javascript:void(0)" onclick="remoteFullModal('/Admin/Agencies/RegisterCode')">
                    代理入口
                </a>
                <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                    <i class="fa fa-lg fa-bars"></i>
                </button>
                <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                    <i class="icon icon-home" style="font-size:1.3em;"></i>
                </a>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            

<div class="bootcards-cards">
        <div class="panel panel-default">
            <div class="list-group">
                    <a href="/Order/Detail/GJ636322805468053619" class="list-group-item" data-id="GJ636322805468053619">
                        <img src="https://wx.qlogo.cn/mmopen/E8kJ4o21rLLyBEMrmD9eiawjt97TtkffjW6flb5Pao3DHyX7s2VhAy57wHy0Oom6Z12cGSXbJhuLsibS87baV2f6zdPIQGSFma/0" style="width:40px;" class="img-rounded pull-left" alt="agency_1039197" />
                        <h4 class="list-group-item-heading">2017-06-05 17:29:06</h4>
                        <p class="list-group-item-text"><span class="text-danger">&#165;357.00</span></p>
                    </a>
                    <a href="/Order/Detail/GJ636322802257067136" class="list-group-item" data-id="GJ636322802257067136">
                        <img src="https://wx.qlogo.cn/mmopen/E8kJ4o21rLLyBEMrmD9eiawjt97TtkffjW6flb5Pao3DHyX7s2VhAy57wHy0Oom6Z12cGSXbJhuLsibS87baV2f6zdPIQGSFma/0" style="width:40px;" class="img-rounded pull-left" alt="agency_1039197" />
                        <h4 class="list-group-item-heading">2017-06-05 17:23:45</h4>
                        <p class="list-group-item-text"><span class="text-danger">&#165;184.00</span></p>
                    </a>
                    <a href="/Order/Detail/GJ636322799706999035" class="list-group-item" data-id="GJ636322799706999035">
                        <img src="https://wx.qlogo.cn/mmopen/E8kJ4o21rLLyBEMrmD9eiawjt97TtkffjW6flb5Pao3DHyX7s2VhAy57wHy0Oom6Z12cGSXbJhuLsibS87baV2f6zdPIQGSFma/0" style="width:40px;" class="img-rounded pull-left" alt="agency_1039197" />
                        <h4 class="list-group-item-heading">2017-06-05 17:19:30</h4>
                        <p class="list-group-item-text"><span class="text-danger">&#165;205.00</span></p>
                    </a>
                    <a href="/Order/Detail/GJ636322791373150541" class="list-group-item" data-id="GJ636322791373150541">
                        <img src="https://wx.qlogo.cn/mmopen/E8kJ4o21rLLyBEMrmD9eiawjt97TtkffjW6flb5Pao3DHyX7s2VhAy57wHy0Oom6Z12cGSXbJhuLsibS87baV2f6zdPIQGSFma/0" style="width:40px;" class="img-rounded pull-left" alt="agency_1039197" />
                        <h4 class="list-group-item-heading">2017-06-05 17:05:37</h4>
                        <p class="list-group-item-text"><span class="text-danger">&#165;1,100,000.00</span></p>
                    </a>
            </div>
        </div>
<div class="pager" data-action="/Admin/Order/OthersSale?sellerId=dcef3f86-f630-4c7f-ac04-aa9f52ff62e5&amp;pageIndex=%23" data-current="1" data-updateTargetId=""><a disabled="disabled"><<</a><a disabled="disabled"><</a><span data-pageIndex='1'>1</span><a disabled="disabled">></a><a disabled="disabled">>></a></div></div>
        </div>
    </div>
        <!-- slide in menu (mobile only) -->
        <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>

    
    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/js/weixinsdk?v=FuHO4EYdVtWj94At6HA29wrWQNt6iYo2nqlZNRPrmqE1"></script>

    <script type="text/javascript">
    if (isWeixin) {
        $.get('/home/getShare', function (data) {
            weixinService.init(data.title, data.desc, data.imgUrl, data.link);
        })
    }
    </script>
    <script src="/src/scripts/takeit.js"></script>
</body>
</html>
