<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸 - 管理版</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
  <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>

    
</head>
<body class="admin-browse">
        <script type="text/javascript">
            /* (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {
                        window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                    }
                } else {
                    top.document.getElementById('page_title_content').innerHTML = '管理版';
                    if (document.body.clientWidth <= 1100) {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })(); */
        </script>
    <input type="hidden" name="navmenu_title" value="产品管理" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">产品编辑</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/WbusiManage/user/Logout" id="fa_user">
                <i class="fa fa-user bt-logout icon icon-signin" style="font-size:1.3em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
            
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<div class="bootcards-cards">
    <div class="panel panel-default">
<form action="/WbusiManage/sys/pro/update" id="editgoodform" 
	method="post" enctype="multipart/form-data">
	<input id="GoodId" name="id" type="hidden" value="${sessionScope.pro.id}" />
			<div class="panel-heading clearfix">
                <h3 class="panel-title pull-left"></h3>
                <a href="javascript:history.back();">
            		<button name="btn_cancel" type="button" class="btn btn-success pull-left">
                        返回
                    </button>
                </a>
                <button name="btn_save" type="submit" class="btn btn-success pull-right">
                    保存
                </button>
            </div>
            <div class="list-group">
            	<div class="list-group-item">
            		<label class="list-group-item-text" for="Title">温馨提示：带*号为必填项</label>
            	</div>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Title">* 产品名称</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-required="产品名称必填" 
                        id="Title" name="proName" type="text" value="${sessionScope.pro.proName }" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Title" 
                        data-valmsg-replace="true"></span>
                    </div>
                </div>
               
<!--                 <div class="list-group-item">
                    <label class="list-group-item-text" for="ArtNo">编号</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-required="编号 必填" id="ArtNo" name="ArtNo" type="text" value="1001" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="ArtNo" data-valmsg-replace="true"></span>
                    </div>
                </div> -->
                
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Spec">* 产品单位</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-required="规格 必填" 
                        id="Spec" name="proUnit" type="text" value="${sessionScope.pro.proUnit }" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Spec" 
                        data-valmsg-replace="true"></span>
                    </div>
                </div>
                
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Price">* 库存数量</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-number="The field 库存 must be a number." 
                        data-val-required="库存必填" id="Count" name="proCount" type="text" value="${sessionScope.pro.proCount}" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Price" 
                        data-valmsg-replace="true"></span>
                    </div>
                </div>
                
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Price">* 产品单价</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-number="The field 单价 must be a number." 
                        data-val-required="单价必填" id="Price" name="proPrice" type="text" value="${sessionScope.pro.proPrice}" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Price" 
                        data-valmsg-replace="true"></span>
                    </div>
                </div>
               
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Price">产品描述</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true"
                         id="Desc" name="proDesc" type="text" value="${sessionScope.pro.proDesc}" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Price" 
                        data-valmsg-replace="true"></span>
                    </div>
                </div>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="ImgUrl">产品图片</label>
                    <div class="list-group-item-heading">
                        <label class="bt-upload" for="inputUp">
		                    <c:choose>
		                   	<c:when test="${fn:contains(sessionScope.pro.proPicPath, '.')}">
		                   	<img src="${sessionScope.pro.proPicPath}" 
		                        style="width:100px;height:100px;">
		                   	</c:when>
		                   	<c:otherwise>
		                   	(暂无图片)
		                   	<img src="/WbusiManage/src/images/site/no_photo.png" 
		                        style="width:60px;height:60px;">
		                   	</c:otherwise>
		                	</c:choose>
                                <%-- <img src="${sessionScope.pro.proPicPath}" style="width: 150px;height: 150px" id="logobase" alt="图片" /> --%>
                        </label>
                        
                        <!-- 修改产品图片 -->
                        <div id="filebox" >
                            <input type="file" name="attaches" accept="image/*" onchange="viewImage(this)"/>
                            <!-- 用于显示上传的图片 -->
                			<img id="preview" width="0" height="0" style="display:none" />
                        </div>
                        	
                        <span class="fa fa-spinner fa-spin inline-loading-main" style="display: none"></span>
                        <input data-val="true" data-val-required="产品主图片必填" id="ImgUrl" name="ImgUrl" type="hidden" 
                        value="/upload/images/good/covers/201712/636489628219657747.jpg" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="ImgUrl" data-valmsg-replace="true"></span>
                        <span style="color: gray;font-size:14px">（可添加或替换产品图片，建议使用正方形的图片，仅支持上传一张）</span>
                    </div>
                </div>
            </div>
		</form>    
	</div>
</div>

        </div>
    </div>
    <!-- slide in menu (mobile only) -->
    <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>
    
    <script type="text/javascript">
    	//上传图片后预览图片
		function viewImage(file){
            var preview = document.getElementById('preview');
            if(file.files && file.files[0]){
                //火狐下
                preview.style.display = "block";
                preview.style.width = "150px";
                preview.style.height = "180px";
                preview.src = window.URL.createObjectURL(file.files[0]);
            }else{
                //ie下，使用滤镜
                file.select();
                var imgSrc = document.selection.createRange().text;
                var localImagId = document.getElementById("localImag"); 
                //必须设置初始大小 
                localImagId.style.width = "250px"; 
                localImagId.style.height = "200px"; 
                try{ 
                localImagId.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale)";
                locem("DXImageTransform.Microsoft.AlphaImageLoader").src = imgSrc; 
                }catch(e){ 
                alert("您上传的图片格式不正确，请重新选择!"); 
                return false; 
                } 
                preview.style.display = 'none'; 
                document.selection.empty(); 
                } 
                return true; 
        	}

            $("#descImgs").change(function () {
            if (this.value) {
                var data = new FormData();
                var that = this;
                var files = that.files;
                data.append("files", files[0]);
                data.append("dataType", "goodDesc");
                    $("#editgoodform .inline-loading-desc").show();
                $.ajax({
                    url: '/upload/UploadFile',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "POST",
                    success: function (data) {
                        if (data.success) {
                            var src = data.path;
                            var html = "<li style='display:inline-block;'><input type='hidden' value='" + src + "' name='ImgDescUrl'/><img name='ImgDesc' style='border:1px solid orange;width:100px;height:100px;margin-bottom:5px;margin-right:5px' src='" + src + "'/></li>";
                            $(".imgs-box").append(html);
                            $("#editgoodform .inline-loading-desc").hide();
                        }
                        else {
                            popMessage(data.message);
                        }
                        that.value = "";
                    },
                });
            }
        });

        $('[name="btn_save"]').click(function () {
                var f = $("#editgoodform").valid();
                var url = $("input[name=ImgUrl]").val();

                if (url == "") {
                    $("span[data-valmsg-for=ImgUrl]").removeClass("field-validation-valid").addClass("field-validation-error");
                    $("span[data-valmsg-for=ImgUrl]").html("产品图片必填");
                    f = false;
                }

                $('input[data-name="LevelPrice"]').each(function (index, e) {
                    $(e).attr('name', "LevelPrice")
                });

               return f;
            });

            $(document).on("click", "[name=ImgDesc]", function () {
                if (confirm("删除图片")) {
                    $.post("/Admin/Good/DeleteImgByUrl", { url: $(this).attr("src") });
                    $(this).parent().remove();
                }
        });
    </script>

    <script type="text/javascript">

        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);
            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/WbusiManage/js/weixinsdk.js"></script>

    <script type="text/javascript">
        if (isWeixin) {
            $.get('/home/getShare', function (data) {
                weixinService.init(data.title, data.desc, data.imgUrl, data.link);
            });
        }
    </script>
</body>
</html>
