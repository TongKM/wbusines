<%@ page language="java" import="java.util.*,cn.busin.tool.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸 - 管理版</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
    <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>
    <link href="/WbusiManage/css/home.css" rel="stylesheet"/>
    <link href="/WbusiManage/src/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
	<style type="text/css">
		#myorderlist {
		   padding-top: 0px;
		}
		
		 @media (max-width: 768px) {
			#order_search_box{
				display: block;
			}
		} 
	</style>
	
</head>
<body class="admin-browse">
    <input type="hidden" name="navmenu_title" value="我的订单" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">卖出订单</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/WbusiManage/user/Logout" id="fa_user">
                <i class="fa fa-user bt-logout icon icon-signin" style="font-size:1.3em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars" ></i>
            <!-- <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-user" style="font-size:1.3em;"></i>
            </button> -->

            </button>
    <button type="button" class="btn btn-default btn-menu offCanvasToggle" style="right:15px;left:auto" data-toggle="off-search-canvas" id="btn_menu">
        <!-- <i class="icon-search" style="color:#999"></i> -->
    </button>

        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<div class="simple-search-container order-search-box order-search-mobile" style="z-index: 10;">
<form action="/Admin/Order/SaleOrder" class="search-form" method="post">        
	   <div class="row">
            <div class="col-xs-12 col-sm-12">
               <div class="form-group input-group-search">
                   <input type="text" name="keywords" class="form-control search-txt" placeholder="订单号/买家姓名/买家邮箱">
                   <input type="hidden" name="tradeStatus" value="0">
                   <button type="submit" class="btn">
                       <i class="icon icon-search-o"></i>
                   </button>
               </div>
           </div> 
       </div>
</form>
</div>
<div class="bootcards-cards order-sales-cards" id="bootcards-cards">
    <div id="myorderlist" style="display:block">
        
    <div id="order_search_box" style="padding:15px 0 25px 0">
<form action="/WbusiManage/sys/order/sendOrder" class="nav-search-form" method="post">     
	<input type="hidden" id="pageNo" name="pageNo" value="${page.pageIndex }"/>
	<input type="hidden" id="totalPage" name="totalPage" value="${page.totalPageCount }"/>
       <div class="row" >
       
                <div class="col-xs-12 col-sm-12">
                    <div class="input-group">

                        <input class="form-control search-text" id="s_Keywords" name="searchName" 
                        placeholder="订单号/买家姓名/买家邮箱" type="text" style="width: 350px" />
                        <input id="s_TradeStatus" name="s.TradeStatus" type="hidden" value="0" />
                        <!-- <div class="input-group-btn">
                            <a type="submit" class="btn btn-default"></a>
                        </div> -->
                    </div>
                </div>
                <!-- <a href="javascript:void(0)" class="btn" id="filter_btn">
                    更多筛选条件
                    <i class="fa fa-angle-down"></i>
                </a> -->
            </div>
            <div class="order-filter-box hidden">
                <div class="row">
                    <div class="col-sm-12">
                        <label class="control-label text-right">创建时间</label>
                        <input class="form-control" data-val="true" data-val-date="The field StartTime must be a date." id="startTime" name="s.StartTime" style="width:155px;" type="text" value="" />-<input class="form-control" data-val="true" data-val-date="The field EndTime must be a date." id="endTime" name="s.EndTime" style="width:155px;" type="text" value="" />
                        <label class="control-label text-right" for="s_ReceiverTel" style="margin-left:20px">收货人手机</label>
                        <input class="form-control" id="s_ReceiverTel" name="s.ReceiverTel" style="width:120px;" type="text" value="" />
                    </div>
                </div>
                <div class="row" style="margin-top:5px">
                    <div class="col-sm-12">
                        <input type="hidden" name="closeFilter" value="True" />
                        <label class="control-label text-right" for="s_ReceiverName">收货人姓名</label>
                        <input class="form-control" id="s_ReceiverName" name="s.ReceiverName" style="width:130px;" type="text" value="" />
<label class="control-label text-right" for="s_SearchTradeStatus" style="margin-left:208px">交易状态</label><select class="form-control text-center" id="s_SearchTradeStatus" name="s.SearchTradeStatus" style="width:120px;padding:0px;-webkit-appearance: menulist"><option value="0">全部</option>
<option value="1">交易关闭</option>
<option value="2">未付款</option>
<option value="3">等待审核</option>
<option value="4">等待发货</option>
<option value="6">交易成功</option>
<option value="7">全额退款</option>
</select>                    </div>
                </div>
            </div>
</form>    </div>
<div id="orderlist" style="none;">
        <c:forEach var="order" items="${orderlist}">
        	<div class="order-item">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left" style="font-size:13px;margin-right:15px">
                        <i class="fa fa-user" style="font-size:1.3em;"></i>
                        ${order.user.name }
                    </h3>
                    <h3 class="panel-title pull-left" style="font-size:13px;margin-right:15px">
                        	邮箱：${order.user.email}
                    </h3>
                    <h3 class="panel-title pull-left" style="font-size:13px;margin-right:15px">
                        	收货地址：${order.user.address}
                    </h3>
                    <h3 class="panel-title pull-left" style="font-size:13px;margin-right:15px">
                        	金额：￥${order.oAmount}
                    </h3>
                    <h3 class="panel-title pull-left hidden-xs" title="订单号" style="font-size:13px;">订单号：${order.orderNo }</h3>
                    <span class="panel-title pull-right visible-xs"><fmt:formatDate pattern="MM-dd HH:mm" value="${order.oTime }"></fmt:formatDate></span>
                    <span class="panel-title pull-right hidden-xs"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${order.oTime }"></fmt:formatDate></span>
                </div>
                <c:forEach var="shop" items="${order.shopList }">
                	<div class="list-group">
                        <a class="list-group-item order-good-item" href="" data-id="GJ636529915246505696">
                            <h4 class="list-group-item-heading">${shop.pro.proName }</h4>
                            <p class="list-group-item-text">&#165;${shop.pro.proPrice } x ${shop.count }</p>
                        </a>
                    <div class="list-group-item text-right">
                        <!-- <p class="list-group-item-text">
                             共  件商品，
                            实付：<b class="text-danger">&#165;2,103.00</b> 
                        </p> -->
                    </div>
                </div>
                </c:forEach>
            </div>
        	</div>
        </c:forEach>
      </div>
        <!-- 分页 -->
        <div class="pager" id="pager" data-action="/Admin/Order/SaleOrder?closeFilter=True&amp;tradeStatus=0&amp;numberCondition=0&amp;pageIndex=%23" data-current="1" data-updateTargetId="">
        	<!-- <a id="first"><<</a>
			<a id="prev"><</a>
			<a class="hidden-xs" data-page="2">1</a>
			<a class="hidden-xs" data-page="2">2</a>
			<a class="hidden-xs" data-page="3">3</a>
			<a class="hidden-xs" data-page="4">4</a>
			<a class="hidden-xs" data-page="5">5</a>
			<a data-page="2" id="next">></a>
			<a data-page="54" id="last">>></a> -->
		</div>
		
        <script type="text/javascript">
            var scrollPosition = localStorage['scrollPosition'];

            document.getElementById('myorderlist').style.display = '';
            if (scrollPosition) {
                var cards = document.getElementById('bootcards-cards');
                cards.scrollTop = scrollPosition;
            }
        </script>

    </div>
</div>
<div class="navmenu off-search-canvas offcanvas-full-left order-search-box">
<form action="/Admin/Order/SaleOrder" class="search-form" method="post">        
		<div class="row">
            <div class="col-xs-12">
                <div class="form-group input-group-search">
                    <input type="text" name="keywords" class="form-control search-txt" placeholder="订单号/买家姓名/买家邮箱">
                    <input type="hidden" name="tradeStatus" value="0">
                    <button type="submit" class="btn">
                        <i class="icon icon-search-o"></i>
                    </button>
                </div>
            </div>
        </div>
</form></div>
<div class="modal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="btn-group pull-left">
                    <button class="btn btn-danger" data-dismiss="modal">
                        取消
                    </button>
                </div>
                <h4 class="modal-title">
                    填写理由
                </h4>
            </div>
<form action="/Order/OrderPayCheck" class="form-horizontal" data-ajax="true" data-ajax-begin="handlerBegin" data-ajax-failure="handlerFailure" data-ajax-method="POST" data-ajax-success="handlerSuccess" id="refuse_form" method="post">                
<div class="modal-body">
                    <input type="hidden" name="orderId" />
                    <input type="hidden" name="isPass" value="false">
                    <div class="form-group">
                        <label class="col-xs-4 control-label">拒绝理由</label>
                        <div class="col-xs-8">
                            <textarea type="text" name="reason" class="form-control" placeholder="在此处填写订单审核未通过理由"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">
                        确定
                    </button>
                </div>
</form>        </div>
    </div>
</div>

        </div>
    </div>
    <!-- slide in menu (mobile only) -->
    <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>

    
    <script src="/WbusiManage/src/moment/moment.js"></script>
    <script src="/WbusiManage/src/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
			
			//点击页数时进入的函数
     	function turnPage(pageIndex){
     		var searchWord=document.getElementById("s_Keywords");
     		jump(searchWord.value,pageIndex);
     	}
     	function nextPages(totalPageCount){//点击“...”显示后面页码
     		//获取当前显示的最后一个页数
     		var last=$(".hidden-xs:last").text();
     		if(last=='...'){
     			last=$(".hidden-xs:last").prev().text();
     		}
     		//去掉原先页码的元素
     		$(".hidden-xs").remove();
     		$("#pageIndex").remove();
     		var html="";
   			html+='<a class="hidden-xs" href="JavaScript:prevPages('+totalPageCount+');">...</a>';
   			if(totalPageCount-parseInt(last)>=5){//判断之后显示的页码有几个（5个）
   				for(var i=parseInt(last)+1;i<=parseInt(last)+5;i++){
   				html+='<a class="hidden-xs" class="pages" href="JavaScript:turnPage('+i+');">'+i+'</a>';
   				}
   			}else{
   				for(var i=parseInt(last)+1;i<=totalPageCount;i++){//判断之后显示的页码有几个（小于5个）
   				html+='<a class="hidden-xs" class="pages" href="JavaScript:turnPage('+i+');">'+i+'</a>';
   				}
   			}
   			if((totalPageCount-last)>=5){//判断是否需要向后显示页码的“...”
   			//增加向后显示页码的“...”
   			html+='<a class="hidden-xs" href="JavaScript:nextPages('+totalPageCount+');">...</a>';
   			}
     		//在id="pageIndex"后追加页数
     		$("#addPage").after(html);
     	}
     	function prevPages(totalPageCount){//点击“...”显示前面页码
     		//获取当前显示的第一个页数
     		var first=$(".hidden-xs:first").text();
     		if(first=='...'){
     			first=$(".hidden-xs:first").next().text();
     		}
     		//去掉原先页码的元素
     		$(".hidden-xs").remove();
     		$("#pageIndex").remove();
     		var html="";
     		if((parseInt(first)-5)>5){//判断是否需要向前显示页码的“...”
     			//增加向前显示页码的“...”
     			html+='<a class="hidden-xs" href="JavaScript:prevPages('+totalPageCount+');">...</a>';
     		}
     			for(var i=parseInt(first)-5;i<first;i++){
     				html+='<a class="hidden-xs" class="pages" href="JavaScript:turnPage('+i+');">'+i+'</a>';
     			}
     			html+='<a class="hidden-xs" href="JavaScript:nextPages('+totalPageCount+');">...</a>';
     		//在id="pageIndex"后追加页数
     		$("#addPage").after(html);
     	}
     	window.onload=function(){
     		var proName=document.getElementById('s_Keywords');
     		jump('', 1);
  			//输入框改变内容触发
     		proName.oninput=function(){
                jump(proName.value,1);//默认显示第一页
            };
        };
            function jump(searchWord,pageIndex){
     		//alert('搜索：'+searchWord+'页数：'+pageIndex);
     		$.ajax({
            url: "/WbusiManage/sys/order/mFind", 
            type: "Post", 
            dataType:"json",
            data: {searchWord:searchWord,pageIndex:pageIndex}, //传入搜索字符和分页信息
            success: function (result) {
            	document.getElementById('orderlist').innerHTML="";
            	var html='';
            	var list=result.orderList;//获取订单集合
            	//拼接98-111行的页面标签（a标签）
               	for(var i=0;i<list.length;i++){
               		html+='<div class="order-item"><div class="panel panel-default">';
                	html+='<div class="panel-heading clearfix">';
                	html+='<h3 class="panel-title pull-left" style="font-size:13px;margin-right:15px">';
                	html+='<i class="fa fa-user" style="font-size:1.3em;"></i>&nbsp;'+list[i].user.name+'</h3>';
                	html+='<h3 class="panel-title pull-left" style="font-size:13px;margin-right:15px">';
                	html+='邮箱：'+list[i].user.email+'</h3>';
                	html+='<h3 class="panel-title pull-left" style="font-size:13px;margin-right:15px">';
                	html+='收货地址：'+list[i].user.address+'</h3>';
                	html+='<h3 class="panel-title pull-left" style="font-size:13px;margin-right:15px">';
                	html+='金额：￥'+list[i].oAmount+'</h3>';
                	//pull-left后面省略hidden-xs
                	html+='<h3 class="panel-title pull-left" title="订单号" ';
                	html+='style="font-size:13px;">订单：'+list[i].orderNo+'</h3>';
                	//html+='<span class="panel-title pull-right visible-xs">'+list[i].oTime+'</span>';
                	//pull-right后面省略hidden-xs
                	html+='<span class="panel-title pull-right">'+list[i].oTime+'</span></div>';
                	for(var j=0;j<list[i].shopList.length;j++){
                		html+='<div class="list-group">';
                		html+='<a class="list-group-item order-good-item"';
                		html+=' href="javascript:;" data-id="GJ636529915246505696">';
                		if(list[i].shopList[j].pro.proPicPath.indexOf('.')>-1){//如果有图片
                    		html+="<img class='img-responsive img-rounded pull-left' src='"+list[i].shopList[j].pro.proPicPath+"' style='width:70px;height:70px;'>";
                    	}else{//如果没有图片
                    		html+="<img class='img-responsive img-rounded pull-left' src='/WbusiManage/src/images/site/no_photo.png' style='width:60px;height:60px;'>";
                    	}
                		html+='<h4 class="list-group-item-heading">';
                		html+=list[i].shopList[j].pro.proName+'</h4><p class="list-group-item-text">&#165;';
                		html+=list[i].shopList[j].pro.proPrice+'x'+list[i].shopList[j].count+'</p><br>';
                		html+='</a><div class="list-group-item text-right"></div></div>';
                	}
                	html+='</div></div>';
               	}
               	document.getElementById('orderlist').innerHTML=html;
               		var page='';
               		//计算出当前页码所在显示页码中的最大页码
               		var mPage='';
               		if(result.pageIndex%5==0){
               			mPage=result.pageIndex;
               		}else{
               			mPage=(parseInt(result.pageIndex/5)*5)+5;
               		}
      
           			if(result.pageIndex>=1 && result.pageIndex<=5){//前面没有“...”，后面可能有，可以点击上一页
           				if(result.pageIndex==1){
           					page+='<a disabled="disabled"><<</a><a id="addPage" disabled="disabled"><</a>';
           				}else{
           					page+='<a href="JavaScript:turnPage(1);"><<</a><a id="addPage" href="JavaScript:turnPage('+(result.pageIndex-1)+');"><</a>';
           				}
           				if(result.totalPageCount>5){//总页数大于5，可以显示后面“...”
	           				for(var i=1;i<=5;i++){//遍历页码也不一样
	       						if(result.pageIndex==i){
	       							page+='<span id="pageIndex">'+result.pageIndex+'</span>';
	       							continue;
	       						}
	       						page+='<a class="hidden-xs" href="JavaScript:turnPage('+i+');">'+i+'</a>';
	       					}
           					page+='<a class="hidden-xs" href="JavaScript:nextPages('+result.totalPageCount+');">...</a>';
           				}else{//总页数大于5，不显示后面“...”
           					for(var i=1;i<=result.totalPageCount;i++){
	       						if(result.pageIndex==i){
	       							page+='<span id="pageIndex">'+result.pageIndex+'</span>';
	       							continue;
	       						}
	       						page+='<a class="hidden-xs" href="JavaScript:turnPage('+i+');">'+i+'</a>';
	       					}
           				}
           				if(result.pageIndex<result.totalPageCount){//不是最后一页时
           					page+='<a href="JavaScript:turnPage('+(result.pageIndex+1)+');">></a>'+
	           				'<a href="JavaScript:turnPage('+result.totalPageCount+');">>></a>';
           				}else{//是最后一页时
           					page+='<a disabled="disabled">></a><a disabled="disabled">>></a>';
           				}
           			}else if(result.pageIndex>5 && result.totalPageCount>mPage){//前面有“...”，后面也有“...”
           				//alert(888);
           				page+='<a href="JavaScript:turnPage(1);"><<</a><a id="addPage" href="JavaScript:turnPage('+(result.pageIndex-1)+');"><</a>';
           				page+='<a class="hidden-xs" href="JavaScript:prevPages('+result.totalPageCount+');">...</a>';
           				for(var i=mPage-4;i<=mPage;i++){
       						if(result.pageIndex==i){
       							page+='<span id="pageIndex">'+result.pageIndex+'</span>';
       							continue;
       						}
       						page+='<a class="hidden-xs" href="JavaScript:turnPage('+i+');">'+i+'</a>';
       					}
       					page+='<a class="hidden-xs" href="JavaScript:nextPages('+result.totalPageCount+');">...</a>';
       					page+='<a href="JavaScript:turnPage('+(result.pageIndex+1)+');">></a>'+
	           			'<a href="JavaScript:turnPage('+result.totalPageCount+');">>></a>';
           			}else if(result.pageIndex>5 && result.totalPageCount<=mPage){//前面有“...”，后面没有“...”
           				//alert(999);
           				page+='<a href="JavaScript:turnPage(1);"><<</a><a id="addPage" href="JavaScript:turnPage('+(result.pageIndex-1)+');"><</a>';
           				page+='<a class="hidden-xs" href="JavaScript:prevPages('+result.totalPageCount+');">...</a>';
           				for(var i=mPage-4;i<=result.totalPageCount;i++){
       						if(result.pageIndex==i){
       							page+='<span id="pageIndex">'+result.pageIndex+'</span>';
       							continue;
       						}
       						page+='<a class="hidden-xs" href="JavaScript:turnPage('+i+');">'+i+'</a>';
       					}
       					if(result.pageIndex<result.totalPageCount){//不是最后一页时
       						page+='<a href="JavaScript:turnPage('+(result.pageIndex+1)+');">></a>'+
	           				'<a href="JavaScript:turnPage('+result.totalPageCount+');">>></a>';
       					}else{//是最后一页时
       						page+='<a disabled="disabled">></a><a disabled="disabled">>></a>';
       					}
       					
           			}
               	//将分页元素设置到页面上
               	var pager=document.getElementById("pager");
               	pager.innerHTML="";
               	pager.innerHTML=page;
            }
        	});
     	}
        
	</script>
    <script type="text/javascript">
    
        $(document).on("click", "a[name=download]", function () {
            var ids = $(this).attr("data-id");
            location.href = "/Order/ExportOrder?ids=" + ids + "";
        });
        function handlerSuccessClose(callback) {
            if (callback.success) {
                var id = callback.result;
                $('#' + id).find('.panel-footer').html('<span class="pull-left" style="color:#f8910b">交易关闭</span>');
                $('#alertModel').modal('hide');
            }
            else {
                var m = setTimeout(function () {
                    popMessage('错误', "关闭订单出错");
                    clearTimeout(m);
                }, 500);
            }
        }

        var goToDetail = false;
        $('.order-good-item').click(function () {
            var scrollPosition = $('.bootcards-cards').scrollTop();
            localStorage['scrollPosition'] = scrollPosition;

            goToDetail = true;
            if ($('body').hasClass('admin-browse') && $(window).width() >= 900) {
                var id = $(this).data('id');
                var url = '/Order/Detail?id=' + id + '&r=' + Math.random();
                remoteFullModal('/App/DialogBox?redirectUrl=' + encodeURIComponent(url));
                return false;
            };

            return true;
        });
        $('a[name="send"]').click(function () {
            if ($('body').hasClass('admin-browse') && $(window).width() >= 900) {
                var id = $(this).data('id');
                var url = '/Order/Send?id=' + id + '&r=' + Math.random();
                remoteFullModal('/App/DialogBox?redirectUrl=' + encodeURIComponent(url));
                return false;
            };
            return true;
        });

        function selectOrderIds(type) {
            var checkboxs = $('input[type="checkbox"][name="order_checkbox"]:checked');
            if (checkboxs.length == 0) {
                alert("请选择要打印的订单");
                return false;
            }
            var ids = '';
            checkboxs.each(function (index, e) {
                ids += $(e).val() + ',';
            });

            if (type == "orderBill")
                window.open('/Admin/Order/PrintBill?orderIds=' + ids);
            else window.open('/Admin/Order/PrintExpress?orderIds=' + ids);
        }

        function exportOrderIds(e) {
            var checkboxs = $('input[type="checkbox"][name="order_checkbox"]:checked');

            if (checkboxs.length == 0) {
                alert("请选择要导出的订单");
                return false;
            }
            var ids = '';

            checkboxs.each(function (index, e) {
                ids += $(e).val() + ',';
            });

            e.href = "/Order/ExportOrder?ids=" + ids;

            return true;
        }

        $('#order_checkbox_all').click(function () {
            if (this.checked) {
                $('input[type="checkbox"][name="order_checkbox"]').prop('checked', true);
            } else {
                $('input[type="checkbox"][name="order_checkbox"]').prop('checked', false);
            }
        });

        $(window).unload(function () {
            if (!goToDetail) {
                localStorage['scrollPosition'] = '';
            }
        });

        $('#startTime').datetimepicker({
            format: 'YYYY/MM/DD HH:mm:ss'
        });
        $('#endTime').datetimepicker({
            format: 'YYYY/MM/DD HH:mm:ss'
        });

        $('#filter_btn').click(function () {
            var has = $('.order-filter-box').hasClass('hidden');
            if (has) {
                $('.order-filter-box').removeClass('hidden');
                $('[name="closeFilter"]').val('false');
            } else {
                $('.order-filter-box').addClass('hidden');
                $('[name="closeFilter"]').val('true');
            }
        });
    </script>
    <script type="text/javascript">
        filterCanvas.init(0, 'off-search-canvas');
        function renderScanData() {
            var ids = '';
            $('a[name="scan"]').each(function () {
                ids += $(this).data('id') + ';';
            });
            if (ids != '') {
                $.get('/TraceSource/GetOrderCodeCount?ids=' + ids, function (result) {
                    if (result.success) {
                        result.result.forEach(function (e) {
                            var v = e.split(';');
                            $('a[name="scan"][data-id="' + v[0] + '"]').find('span').html('(' + v[1] + ')');
                        });
                    }
                });
            }
        }

        renderScanData();
    </script>
    <script type="text/javascript">
        $('a[name="btn-nopass"]').click(function () {
            var id = $(this).data('id');
            $('#refuse_form').find('[name="orderId"]').val(id);
            $('#refuse_form').find('[name="reason"]').val('');
            $('#editModal').modal();
        });
    </script>

    <script type="text/javascript">

        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    
    <script type="text/javascript">
        $(document).ready(function() {
        	$(".hidden-xs").click(function() {
        		var pageNo = $(this).text();
        		$("#pageNo").val(pageNo);
        		$(".nav-search-form").submit();
        	});
        	
        	$("#first").click(function() {
        		$("#pageNo").val(1);
        		$(".nav-search-form").submit();
        	});
        	
        	$("#prev").click(function() {
        		var pageNo = $("#pageNo").val()-1;
        		$("#pageNo").val(pageNo);
        		$(".nav-search-form").submit();
        	});
        	
        	$("#next").click(function() {
        		var pageNo = $("#pageNo").val()+1;
        		$("#pageNo").val(pageNo);
        		$(".nav-search-form").submit();
        	});
        	
        	$("#last").click(function() {
        		var totalPage = $("#totalPage").val();
        		$("#pageNo").val(totalPage);
        		$(".nav-search-form").submit();
        	});
        });
    </script>
</body>
</html>
