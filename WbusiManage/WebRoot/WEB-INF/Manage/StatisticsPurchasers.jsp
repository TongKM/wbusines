<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸 - 管理版</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>
    <script type="text/javascript" src="/WbusiManage/js/jquery-1.4.2.min.js"></script>
	<script src="/WbusiManage/js/highcharts.js"></script>
    
</head>
<body class="admin-browse">
    <input type="hidden" name="navmenu_title" value="销量统计" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">订单销量统计</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/WbusiManage/user/Logout" id="fa_user">
                <i class="fa fa-user bt-logout icon icon-signin" style="font-size:1.3em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
            
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<div class="navmenu search-nav search-nav-2">
    <ul class="nav">
        <li class="active">
            <a data-title="订单销量统计" href="/WbusiManage/sys/order/toStatisticsPurchasers">
                订单销量统计
            </a>
        </li>
            <!-- <li class="tt">
                <a data-title="团队销量统计" href="/Admin/Order/RegionTeamSold">
                    团队销量统计
                </a>
            </li> -->
        <li class="tt">
            <a data-value="2" data-title="商品销量统计" href="/WbusiManage/sys/order/toStatisticsSold">
                商品销量统计
            </a>
        </li>
    </ul>
</div>

<div class="bootcards-cards" style="padding-top:0">
    <div class="panel panel-default margin-top-nav">
        <div class="panel-heading clearfix text-center">
        <!-- 点击查看上一个月 -->
            <a class="btn btn-default pull-left" style="width:65px" 
            href="/WbusiManage/sys/order/StatisticsPurchasers?interval=${interval+1}">
                <i class="icon-left-new"></i>
            </a>
            <label style="padding:0px 15px;margin-top:5px">${date}</label>
            <!-- 如果interval为0，即当前时间的月份，则不能点击下一个月 -->
            <c:if test="${interval>0}">
                <a class="btn btn-default pull-right" style="margin-left:0px;width:65px;"
                href="/WbusiManage/sys/order/StatisticsPurchasers?interval=${interval-1}">
                    <i class="icon-right-new"></i>
                </a>
            </c:if>
        </div>
    </div>
    <c:if test="${fn:length(orderList)==0}">
        <div class="row text-center body-padding-normal">无相关的数据</div>
        </c:if>
</div>

        </div>
    </div>
    <!-- slide in menu (mobile only) -->
	<jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>
    
    <script src="/src/highcharts-4.2.8/highcharts.js"></script>
    <script type="text/javascript">
/*         $(function () {
            if ($("#charData").length == 0) return;

            var data = eval($("#charData").val());
            var charData = [];
            $.each(data, function () {
                if (this.purchaseAmount >= 0) {
                    var b = new Object();
                    b.name = this.realName;
                    b.y = this.purchaseAmount;
                    charData.push(b);
                }
            });

            // Build the chart
            $('#chartAgencies').highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: null,
                },
                tooltip: {
                    pointFormat: '购买量: <b>{point.percentage:.2f}%</b>'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            useHTML: true,
                            format: '<big>{point.name}</big><br/> <small>{point.percentage:.2f}%</small>',
                            distance: -50,
                            style: {
                                fontWeight: 'bold',
                                color: 'white',
                                textShadow: '0px 1px 2px black'
                            }
                        },
                        startAngle: -180,
                        endAngle: 180,
                        center: ['50%', '50%']
                    }
                },
                series: [{
                    name: "Agencies",
                    colorByPoint: true,
                    data: charData,
                }]
            });
        }); */
        (function($) { // encapsulate jQuery
    $(document).ready(function() {
        var jsonyD2 = [];
        $.ajax({
            url: '/WbusiManage/sys/order/StatisticsPurchasers',
            cache: false,
            async: false,
            success: function(data) {
                var json = eval("(" + data + ")");
                if (json.Rows.length > 0) {
                    for (var i = 0; i < json.Rows.length; i++) {
                       	var jsonyD1 = [];
                        var rows = json.Rows[i];
                        var Time = rows.time;
                        var SumCount = rows.sumCount;
                        jsonyD1.push(Time);
                        jsonyD1.push(parseInt(SumCount));
                        jsonyD2.push(jsonyD1);
                    } //for
                    var chart;
                    chart = new Highcharts.Chart({
                        chart: {
                            renderTo: 'container',
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false
                        },
                        title: {
                            text: '数据饼状图表'
                        },
                        tooltip: {
                            formatter: function() {
                                return '<b>' + this.point.name + '</b>: ' + this.percentage.toFixed(2) + ' %';
                            }
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    color: '#000000',
                                    connectorColor: '#000000',
                                    formatter: function() {
                                        return '<b>' + this.point.name + '</b>: ' + this.percentage.toFixed(2) + ' %';
                                    }
                                }
                            }
                        },
                        series: [{
                            type: 'pie',
                            name: 'pie',
                            data:   jsonyD2
                        }]
                    });
                }
            }
        });
    });
})(jQuery);
    </script>

    <script type="text/javascript">

        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
</body>
</html>
