<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title> 部落管家</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
	<jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>
    
</head>
<body class="admin-browse">
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {
                        window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                    }
                } else {
                    top.document.getElementById('page_title_content').innerHTML = '类别管理';
                    if (document.body.clientWidth <= 1100) {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" value="系统管理" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">类别管理</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/Admin/Employee/Details" id="fa_user">
                <i class="fa fa-user" style="font-size:1.6em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
            
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<div class="bootcards-cards">
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <a href="/Admin/Category/Add" class="btn btn-primary pull-right">
                <i class="icon icon-add-to"></i>
                <span> 添加类别</span>
            </a>
        </div>
    </div>
    <div class="panel panel-default">
            <div class="list-group">
                    <a class="list-group-item" href="/Admin/Category/Edit/1">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="list-group-item-heading pull-left">
                                    健康酒
                                </h4>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item" href="/Admin/Category/Edit/5">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="list-group-item-heading pull-left">
                                    日化用品
                                </h4>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item" href="/Admin/Category/Edit/17">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="list-group-item-heading pull-left">
                                    食品
                                </h4>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item" href="/Admin/Category/Edit/21">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="list-group-item-heading pull-left">
                                    玫瑰测试
                                </h4>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item" href="/Admin/Category/Edit/22">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="list-group-item-heading pull-left">
                                    游戏
                                </h4>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item" href="/Admin/Category/Edit/12">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="list-group-item-heading pull-left">
                                    保健品
                                </h4>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item" href="/Admin/Category/Edit/15">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="list-group-item-heading pull-left">
                                    女装
                                </h4>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item" href="/Admin/Category/Edit/25">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="list-group-item-heading pull-left">
                                    测试
                                </h4>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item" href="/Admin/Category/Edit/26">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="list-group-item-heading pull-left">
                                    测试
                                </h4>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item" href="/Admin/Category/Edit/16">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="list-group-item-heading pull-left">
                                    饰品
                                </h4>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item" href="/Admin/Category/Edit/11">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="list-group-item-heading pull-left">
                                    白酒
                                </h4>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item" href="/Admin/Category/Edit/4">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="list-group-item-heading pull-left">
                                    宠物
                                </h4>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item" href="/Admin/Category/Edit/24">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="list-group-item-heading pull-left">
                                    白酒
                                </h4>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item" href="/Admin/Category/Edit/23">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="list-group-item-heading pull-left">
                                    纸尿裤
                                </h4>
                            </div>
                        </div>
                    </a>
            </div>
    </div>
</div>

        </div>
    </div>
    <!-- slide in menu (mobile only) -->
    <jsp:include page="/WEB-INF/jsp/menu.jsp"></jsp:include>
    
    <script type="text/javascript">

        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/js/weixinsdk?v=FuHO4EYdVtWj94At6HA29wrWQNt6iYo2nqlZNRPrmqE1"></script>

    <script type="text/javascript">
        if (isWeixin) {
            $.get('/home/getShare', function (data) {
                weixinService.init(data.title, data.desc, data.imgUrl, data.link);
            })
        }
    </script>
</body>
</html>
