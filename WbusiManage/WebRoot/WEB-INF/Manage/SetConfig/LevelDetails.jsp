<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title> 部落管家</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>

    
</head>
<body class="admin-browse">
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {
                        window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                    }
                } else {
                    top.document.getElementById('page_title_content').innerHTML = '代理等级规则';
                    if (document.body.clientWidth <= 1100) {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" value="系统管理" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">代理等级规则</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/Admin/Employee/Details" id="fa_user">
                <i class="fa fa-user" style="font-size:1.6em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
            
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<div class="bootcards-cards">
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <a class="btn btn-primary pull-left" href="/Admin/Level" style="margin-left:0px">
                <i class="fa fa-arrow-left"></i>
                <span>返回</span>
            </a>
                <a href="/Admin/Level/Edit/1" class="btn btn-primary  pull-right">
                    <i class="fa fa-pencil"></i>
                    编辑
                </a>
        </div>
        <div class="list-group">
            <div class="list-group-item">
                <label>代理等级</label>
                <h4 class="list-group-item-heading">
                    联合创始人
                </h4>
            </div>
            <div class="list-group-item">
                <label for="FirstLimitSum">首次进货金额</label>
                <h4 class="list-group-item-heading">
                    100.00
                </h4>
            </div>
            <div class="list-group-item">
                <label for="SendLimitSum">补货金额</label>
                <h4 class="list-group-item-heading">
                    5.00
                </h4>
            </div>
        </div>
            <div class="panel-footer" style="padding:5px 15px;">

                    <span>
                        一次性进货（?）即可获资质
                    </span>
                    <div class="pull-right">
                        <a href="javascript:void(0)" onclick="remoteFullModal('/Admin/Level/UpdateRequireAmount/1')" class="btn btn-primary txt">
                            【配置】
                        </a>
                    </div>
            </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">考核规则</h3>
                <div class="pull-right text-right">
<form action="/Admin/Level/DeleteRule/44ddb0c9-befa-44a5-a39d-0639aa7a6e9c" class="pull-left" data-ajax="true" data-ajax-begin="handlerBegin" data-ajax-confirm="您确定删除该升级规则么？" data-ajax-failure="handlerFailure" data-ajax-method="POST" data-ajax-success="handlerSuccess" id="form0" method="post">                            <button type="submit" class="btn btn-danger">
                                <i class="fa fa-trash-o"></i>删除
                            </button>
</form>                    <a href="/Admin/Level/SetExamRule/1" class="btn btn-primary">
                        <i class="fa fa-pencil"></i>
                        编辑
                    </a>
                </div>
        </div>
        <div class="list-group">
                <div class="list-group-item">
                    <label for="LastLevelDownRule_ObserveTime">考核周期（月）</label>
                    <h4 class="list-group-item-heading">
                        1
                    </h4>
                </div>
                <div class="list-group-item">
                    <label for="LastLevelDownRule_ObserveType">考核方式</label>
                    <h4 class="list-group-item-heading">
                        月最低销售额
                    </h4>
                </div>
                <div class="list-group-item">
                    <label for="LastLevelDownRule_LevelUpSum">月销售额</label>
                    <div class="list-group-item-heading">
                        100.00
                    </div>
                </div>
                <div class="list-group-item">
                    <label> 且</label>
                </div>
                <div class="list-group-item">
                    <label for="LastLevelDownRule_ObserveSum">周期总销售额</label>
                    <div class="list-group-item-heading">
                        0.00
                    </div>
                </div>
        </div>
    </div>
</div>
        </div>
    </div>
    <!-- slide in menu (mobile only) -->
   <jsp:include page="/WEB-INF/jsp/menu.jsp"></jsp:include>

    
    <script type="text/javascript">

        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/js/weixinsdk?v=FuHO4EYdVtWj94At6HA29wrWQNt6iYo2nqlZNRPrmqE1"></script>

    <script type="text/javascript">
        if (isWeixin) {
            $.get('/home/getShare', function (data) {
                weixinService.init(data.title, data.desc, data.imgUrl, data.link);
            })
        }
    </script>
</body>
</html>
