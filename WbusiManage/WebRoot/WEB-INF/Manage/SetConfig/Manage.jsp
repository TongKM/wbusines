<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title> 部落管家</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>

    
    <style type="text/css">
        .list-group-item i {
            margin-right: 0px;
            opacity: 0.6;
            color: #777;
        }
    </style>

</head>
<body class="admin-browse">
        <script type="text/javascript">
            /* (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {
                        window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                    }
                } else {
                    top.document.getElementById('page_title_content').innerHTML = '系统管理';
                    if (document.body.clientWidth <= 1100) {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })(); */
        </script>
    <input type="hidden" name="navmenu_title" value="系统管理" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">系统管理</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/Admin/Employee/Details" id="fa_user">
                <i class="fa fa-user" style="font-size:1.6em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
            
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<div id="manage" class="bootcards-cards list-font" style="padding-top:15px;">
        <div class="panel panel-default margin">
            <div class="list-group">
                    <a class="list-group-item" href="/Admin/SetConfig/InitCompany">
                        <i class="fa fa-lg fa-cog"></i>
                        公司信息初始化
                    </a>
                                    <a class="list-group-item" href="/Admin/WeixinAccount">
                        <i class="icon icon-wechat-public"></i>
                        微信公众号初始化
                    </a>
                                    <a class="list-group-item" href="/Admin/SetConfig/PayAccount">
                        <i class="icon icon-payment"></i>
                        支付信息初始化
                    </a>
            </div>
        </div>
                    <div class="panel panel-default margin">
            <div class="list-group">
                    <a class="list-group-item" href="/Admin/Level">
                        <i class="icon icon-agent-level"></i>
                        代理级别管理
                    </a>
                                    <a class="list-group-item" href="/Admin/Category">
                        <i class="icon icon-product-category"></i>
                        产品类别管理
                    </a>
            </div>
        </div>
            <div class="panel panel-default margin hidden-xs">
            <a class="list-group-item" href="/Admin/Order/SetExportTemplate">
                <i class="icon icon-order-export"></i>
                订单导出配置
            </a>
            <a class="list-group-item" href="/Admin/ExpressTemplate">
                <i class="icon icon-waybill-template"></i>
                运单模板设置
            </a>
            <a class="list-group-item" href="/Admin/Freight/Template">
                <i class="icon icon-freight-template"></i>
                运费模板设置
            </a>
            <a class="list-group-item" href="/Admin/Freight/Free">
                <i class="icon icon-free-shipping"></i>
                免邮规则设置
            </a>
            <a class="list-group-item" href="/Admin/Depot">
                <i class="icon icon-warehouse"></i>
                仓库地址配置
            </a>
        </div>
            <div class="panel panel-default margin">
            <div class="list-group">
                    <a class="list-group-item" href="/Admin/SetConfig/RebatePointLevelOrder">
                        <i class="icon icon-chinses-coins"></i>
                        返点工具设置
                    </a>
                                    <a class="list-group-item" href="/Admin/SetConfig/UpdateRecheckLevelIds">
                        <i class="icon icon-chinses-coins"></i>
                        公司复审设置
                    </a>
            </div>
        </div>
            <div class="panel panel-default margin">
            <div class="list-group">
                    <a class="list-group-item" href="/Admin/FAQ">
                        <i class="icon icon-faq"></i>
                        FAQ设置
                    </a>
                                    <a class="list-group-item" href="/Admin/SetConfig/RegisterFieldUserDefined">
                        <i class="icon icon-dictionaries"></i>
                        注册字典设置
                    </a>
                    <a class="list-group-item" href="/Admin/SetConfig">
                        <i class="icon icon-system-settings"></i>
                        系统基本配置
                    </a>
            </div>
        </div>

    <div class="panel panel-default margin">
        <div class="list-group">
            <a class="list-group-item" href="/Admin/SetConfig/AuthorizationInfo">
                <i class="icon icon-website-authorization"></i>
                网站授权
            </a>
        </div>
    </div>
</div>

        </div>
    </div>
    <!-- slide in menu (mobile only) -->
   <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>

    
    <script type="text/javascript">

        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/js/weixinsdk?v=FuHO4EYdVtWj94At6HA29wrWQNt6iYo2nqlZNRPrmqE1"></script>

    <script type="text/javascript">
        if (isWeixin) {
            $.get('/home/getShare', function (data) {
                weixinService.init(data.title, data.desc, data.imgUrl, data.link);
            })
        }
    </script>
</body>
</html>
