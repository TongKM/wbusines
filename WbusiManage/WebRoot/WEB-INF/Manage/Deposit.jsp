<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title> 部落管家</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>
    
</head>
<body class="admin-browse">
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {
                        window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                    }
                } else {
                    top.document.getElementById('page_title_content').innerHTML = 'agency_1622584资产';
                    if (document.body.clientWidth <= 1100) {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" value="agency_1622584资产" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">agency_1622584资产</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/Admin/Employee/Details" id="fa_user">
                <i class="fa fa-user" style="font-size:1.6em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
            
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<div class="bootcards-cards">
    <div class="margin">
        <div class="panel panel-default">
            <div class="list-group">
                <div class="list-group-item">
                    <img class="img-rounded pull-left" src="https://wx.qlogo.cn/mmopen/PiajxSqBRaELvV84DPXFfYAGEBVQAuQBzxgdm5OftCaJ68Xcb3uEFibsa7K43q3w4Rp40iawK8gHd5Q0XlyUoO8RQ/0" />
                    <h4 class="list-group-item-heading">
                        agency_1622584<small>(上级：agency_6756453)</small>
                    </h4>
                    <p class="list-group-item-text">
                        可用余额： 0.00<span>元</span>
                    </p>
                </div>
            </div>
        </div>
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <div class="panel-title">
                        全部余额
                    </div>
                </div>
                <div class="list-group">
                        <div class="list-group-item" data-ajax-user="true" data-id="00000000-0000-0000-0000-000000000000">
                            <div class="row">
                                <div class="col-sm-12">
                                    <img src="" class="img-rounded pull-left" data-ajax-userlogo="00000000-0000-0000-0000-000000000000">
                                    <div class="pull-left">
                                        <h4 class="list-group-item-heading">
                                            <span data-ajax-username="00000000-0000-0000-0000-000000000000"></span>
                                                <small style="color:#999">(已冻结)</small>
                                        </h4>
                                        <h4 class="list-group-item-heading">
                                            20000.00<span>元</span>
                                        </h4>
                                    </div>
                                        <div class="pull-right" style="padding-top:5px">
                                            <a name="btn-reset" href="/Admin/Deposit/ResetAccount/dcef3f86-f630-4c7f-ac04-aa9f52ff62e5?targetUserId=00000000-0000-0000-0000-000000000000">余额重置</a>
                                        </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="panel-title">
                    下级资产
                </div>
            </div>
                <div class="list-group">
                        <div class="list-group-item">
                            <div class="row">
                                <div class="col-sm-12">
                                    <img src="https://wx.qlogo.cn/mmopen/E8kJ4o21rLLyBEMrmD9eiawjt97TtkffjW6flb5Pao3DHyX7s2VhAy57wHy0Oom6Z12cGSXbJhuLsibS87baV2f6zdPIQGSFma/0" class="img-rounded pull-left">
                                    <h4 class="list-group-item-heading">agency_1039197</h4>
                                    <h4 class="list-group-item-heading">
                                        40.00<span>元</span>
                                    </h4>
                                </div>
                            </div>
                        </div>
                </div>
        </div>
    </div>
</div>

        </div>
    </div>
    <!-- slide in menu (mobile only) -->
    <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>

    
    <script type="text/javascript">
        getUserInfo();
        $('a[name="btn-reset"]').click(function () {
            if ($('body').hasClass('admin-browse') && $(window).width() >= 900) {
                var url = this.href;
                remoteFullModal('/App/DialogBox?redirectUrl=' + encodeURIComponent(url));
                return false;
            };

            return true;
        });
    </script>

    <script type="text/javascript">

        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/js/weixinsdk?v=FuHO4EYdVtWj94At6HA29wrWQNt6iYo2nqlZNRPrmqE1"></script>

    <script type="text/javascript">
        if (isWeixin) {
            $.get('/home/getShare', function (data) {
                weixinService.init(data.title, data.desc, data.imgUrl, data.link);
            })
        }
    </script>
</body>
</html>
