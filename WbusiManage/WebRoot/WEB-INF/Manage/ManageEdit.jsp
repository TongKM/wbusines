<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸 - 管理版</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
  <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>
    
</head>
<body class="admin-browse">
        <script type="text/javascript">
            /* (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {
                        window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                    }
                } else {
                    top.document.getElementById('page_title_content').innerHTML = '管理版';
                    if (document.body.clientWidth <= 1100) {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })(); */
        </script>
    <input type="hidden" name="navmenu_title" value="公司信息维护" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">公司信息维护</a>
            </div>
            <!-- 系统退出 -->
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/WbusiManage/user/Logout" id="fa_user">
                <i class="fa fa-user bt-logout icon icon-signin" style="font-size:1.3em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
            
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<div class="bootcards-cards">
    <div class="panel panel-default">
<form action="/WbusiManage/sys/manage/update" id="editgoodform" 
	method="post" enctype="multipart/form-data">
	<input id="GoodId" name="id" type="hidden" value="${sessionScope.user.id}" />
	<div class="panel-heading clearfix">
                <h3 class="panel-title pull-left"></h3>
                <a href="javascript:history.back();">
            		<button name="btn_cancel" type="button" class="btn btn-success pull-left">
                    返回
                    </button>
                </a>
                <button name="btn_save" type="submit" class="btn btn-success pull-right">
                    保存
                </button>
     </div>
            <div class="list-group">
            	<div class="list-group-item">
            		<label class="list-group-item-text" for="Title">温馨提示：全部是必填项</label>
            	</div>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Title">公司名称</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-required="公司名称必填"
                        id="Title" name="name" type="text" value="${sessionScope.user.name }" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Title" 
                        data-valmsg-replace="true"></span>
                    </div>
                </div>
                
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Spec">联系电话</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-required="联系电话 必填"
                        id="Spec" name="phone" type="text" value="${sessionScope.user.phone }" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Spec" 
                        data-valmsg-replace="true"></span>
                    </div>
                </div>
                
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Spec">公司地址</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-required="公司地址 必填"
                        id="Spec" name="address" type="text" value="${sessionScope.user.address }" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Spec" 
                        data-valmsg-replace="true"></span>
                    </div>
                </div>
                
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Spec">支付宝账号</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-required="支付宝账号 必填"
                        id="Spec" name="payAccount" type="text" value="${sessionScope.user.payAccount }" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Spec" 
                        data-valmsg-replace="true"></span>
                    </div>
                </div>
            </div>
		</form>    
	</div>
</div>

        </div>
    </div>
    <!-- slide in menu (mobile only) -->
    <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>
    
    <script type="text/javascript">
    		//上传了图片后的异步方法
           /*  $("#inputUp").change(function () {
            if (this.value) {
                var data = new FormData();
                var that = this;
                var files = that.files;
                data.append("files", files[0]);
                data.append("dataType", "good");
                    $("#editgoodform .inline-loading-main").show();
                $.ajax({
                    url: '/upload/UploadFile',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "POST",
                    success: function (data) {
                        if (data.success) {
                            document.getElementById("logobase").src = data.path;
                            $("input[name=ImgUrl]").val(data.path);
                            $("#logobase").show();
                            $("span[data-valmsg-for=ImgUrl]").removeClass("field-validation-error").addClass("field-validation-valid");
                            $("span[data-valmsg-for=ImgUrl]").html("");
                            $("#editgoodform .inline-loading-main").hide();
                        }
                        else {
                            popMessage(data.message);
                        }
                        that.value = "";
                    },
            });
            }
        }); */

            $("#descImgs").change(function () {
            if (this.value) {
                var data = new FormData();
                var that = this;
                var files = that.files;
                data.append("files", files[0]);
                data.append("dataType", "goodDesc");
                    $("#editgoodform .inline-loading-desc").show();
                $.ajax({
                    url: '/upload/UploadFile',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "POST",
                    success: function (data) {
                        if (data.success) {
                            var src = data.path;
                            var html = "<li style='display:inline-block;'><input type='hidden' value='" + src + "' name='ImgDescUrl'/><img name='ImgDesc' style='border:1px solid orange;width:100px;height:100px;margin-bottom:5px;margin-right:5px' src='" + src + "'/></li>";
                            $(".imgs-box").append(html);
                            $("#editgoodform .inline-loading-desc").hide();
                        }
                        else {
                            popMessage(data.message);
                        }
                        that.value = "";
                    },
                });
            }
        });

        $('[name="btn_save"]').click(function () {
                var f = $("#editgoodform").valid();
                var url = $("input[name=ImgUrl]").val();

                if (url == "") {
                    $("span[data-valmsg-for=ImgUrl]").removeClass("field-validation-valid").addClass("field-validation-error");
                    $("span[data-valmsg-for=ImgUrl]").html("产品图片必填");
                    f = false;
                }

                $('input[data-name="LevelPrice"]').each(function (index, e) {
                    $(e).attr('name', "LevelPrice")
                });

               return f;
            });

            $(document).on("click", "[name=ImgDesc]", function () {
                if (confirm("删除图片")) {
                    $.post("/Admin/Good/DeleteImgByUrl", { url: $(this).attr("src") });
                    $(this).parent().remove();
                }
        });
    </script>
	<script type="text/javascript">
        filterCanvas.init(44);
    </script>
    <script type="text/javascript">

        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);
            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/WbusiManage/js/weixinsdk.js"></script>

    <script type="text/javascript">
        if (isWeixin) {
            $.get('/home/getShare', function (data) {
                weixinService.init(data.title, data.desc, data.imgUrl, data.link);
            });
        }
    </script>
</body>
</html>
