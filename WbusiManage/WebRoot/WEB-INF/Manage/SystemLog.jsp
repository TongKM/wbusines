<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸 - 管理版</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>

    
</head>
<body class="admin-browse">
    <input type="hidden" name="navmenu_title" value="操作日志" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">用户操作日志</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/WbusiManage/user/Logout" id="fa_user">
                <i class="fa fa-user bt-logout icon icon-signin" style="font-size:1.3em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
 <div class="navmenu search-nav search-nav-3">
    <!--<ul class="nav">
        <li role="presentation" class="active">
            <a data-value="" data-title="系统操作日志" href="/Admin/Account/SystemLog">
                系统操作
            </a>
        </li>
        <li role="presentation" class="tt">
            <a data-value="4" data-title="管理代理日志" href="/Admin/Agencies/OperateLog">
                管理代理
            </a>
        </li>
        <li role="presentation" class="tt">
            <a data-value="2" data-title="用户登录日志" href="/Admin/Account/LoginLog">
                用户登录
            </a>
        </li>
    </ul>-->
</div> 

<div class="bootcards-list list-navmenu">
    <div class="panel panel-default">
        <div class="panel-body">
<form action="/Admin/Account/SystemLog" class="search-form" method="post" role="form">  
		<div class="row">
             <div class="col-xs-9">
                 <div class="form-group">
                 	<!-- 输入用户名自动查询 -->
                     <input class="form-control search-txt" id="searchWord" name="keyword" placeholder="请输入用户名" type="text" value="" />
                     <i class="fa fa-search"></i>
                 </div>
             </div>
             <div class="col-xs-3">
                 <!-- <button type="button" class="btn btn-default btn-menu pull-left btn-block" data-toggle="off-filter-canvas">
                     <i class="icon-screen-o-new"></i>
                     <span>筛选</span>
                 </button> -->
             </div>
       </div>
</form>        
</div>

            <div class="list-group" id="logList">
                <div class="list-group-item">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="list-group-item-text" style="color:#000">代理等级从【二级代理 首批-0.00 续批-0.00】修改为【大区 首批-0.00 续批-0.00】</p>
                            <p class="list-group-item-text">操作时间： 2017-09-16 20:24:40</p>
                        </div>
                    </div>
                </div>
                <div class="list-group-item">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="list-group-item-text" style="color:#000">代理等级从【二级代理 首批-0.00 续批-0.00】修改为【大区 首批-0.00 续批-0.00】</p>
                            <p class="list-group-item-text">操作者：18981522355 &nbsp; 2017-09-16 20:24:40</p>
                        </div>
                    </div>
                </div>
            </div>
           
<!-- 分页 -->
<div id="pager" class="pager" data-action="/Admin/Account/SystemLog?pageIndex=%23" data-current="1" data-updateTargetId="">

<!--<a disabled="disabled"><<</a><a disabled="disabled"><</a>
<span data-pageIndex='1'>1</span>
<a class="hidden-xs" data-page="2" href="/Admin/Account/SystemLog?pageIndex=2">2</a>
<a class="hidden-xs" data-page="3" href="/Admin/Account/SystemLog?pageIndex=3">3</a>
<a class="hidden-xs" data-page="4" href="/Admin/Account/SystemLog?pageIndex=4">4</a>
<a class="hidden-xs" data-page="5" href="/Admin/Account/SystemLog?pageIndex=5">5</a>
<a class="hidden-xs" data-page="6" href="/Admin/Account/SystemLog?pageIndex=6">...</a>
<a data-page="2" href="/Admin/Account/SystemLog?pageIndex=2">></a><a data-page="9" href="/Admin/Account/SystemLog?pageIndex=9">>></a>
 -->
 
</div>
</div>
</div>
<div class="navmenu off-filter-canvas offcanvas-right">
<form action="/Admin/Account/SystemLog" method="post" name="searchform" style="padding:15px 8px 0px 8px"><input id="keyword" name="keyword" type="hidden" value="" />        <div class="form-group">
            <label>记录类型</label>
    <select class="form-control" id="logType" name="logType"><option value="">全部</option>
<option value="1">代理等级</option>
<option value="2">系统配置</option>
	</select>
        </div>
        
        <div class="form-group">
            <label>操作者</label>
            <input class="form-control" id="operateUserName" name="operateUserName" type="text" value="" />
        </div>
        <input type="submit" value="确定" class="btn btn-desktop-primary btn-block" />
</form>
</div>


        </div>
    </div>
    <!-- slide in menu (mobile only) -->
    <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>
    <script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
    <script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/WbusiManage/src/scripts/takeit.js"></script>
    <script src="/WbusiManage/js/app.js"></script>

    
    <script type="text/javascript">
        filterCanvas.init(44);
    </script>

    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });
		
		window.onload=function(){
  			var searchWord=document.getElementById("searchWord");
  			jump('', 1);
  			//输入框改变内容时触发
     		searchWord.oninput=function(){
       		//alert(1);
       		jump(searchWord.value,1);//默认显示第一页
     	};
};
		//点击页数时进入的函数
     	function turnPage(pageIndex){
     		var searchWord=document.getElementById("searchWord");
     		jump(searchWord.value,pageIndex);
     	}
     	function nextPages(totalPageCount){//点击“...”显示后面页码
     		//获取当前显示的最后一个页数
     		var last=$(".hidden-xs:last").text();
     		if(last=='...'){
     			last=$(".hidden-xs:last").prev().text();
     		}
     		//去掉原先页码的元素
     		$(".hidden-xs").remove();
     		$("#pageIndex").remove();
     		var html="";
   			html+='<a class="hidden-xs" href="JavaScript:prevPages('+totalPageCount+');">...</a>';
   			if(totalPageCount-parseInt(last)>=5){//判断之后显示的页码有几个（5个）
   				for(var i=parseInt(last)+1;i<=parseInt(last)+5;i++){
   				html+='<a class="hidden-xs" class="pages" href="JavaScript:turnPage('+i+');">'+i+'</a>';
   				}
   			}else{
   				for(var i=parseInt(last)+1;i<=totalPageCount;i++){//判断之后显示的页码有几个（小于5个）
   				html+='<a class="hidden-xs" class="pages" href="JavaScript:turnPage('+i+');">'+i+'</a>';
   				}
   			}
   			if((totalPageCount-last)>=5){//判断是否需要向后显示页码的“...”
   			//增加向后显示页码的“...”
   			html+='<a class="hidden-xs" href="JavaScript:nextPages('+totalPageCount+');">...</a>';
   			}
     		//在id="pageIndex"后追加页数
     		$("#addPage").after(html);
     	}
     	function prevPages(totalPageCount){//点击“...”显示前面页码
     		//获取当前显示的第一个页数
     		var first=$(".hidden-xs:first").text();
     		if(first=='...'){
     			first=$(".hidden-xs:first").next().text();
     		}
     		//去掉原先页码的元素
     		$(".hidden-xs").remove();
     		$("#pageIndex").remove();
     		var html="";
     		if((parseInt(first)-5)>5){//判断是否需要向前显示页码的“...”
     			//增加向前显示页码的“...”
     			html+='<a class="hidden-xs" href="JavaScript:prevPages('+totalPageCount+');">...</a>';
     		}
     			for(var i=parseInt(first)-5;i<first;i++){
     				html+='<a class="hidden-xs" class="pages" href="JavaScript:turnPage('+i+');">'+i+'</a>';
     			}
     			html+='<a class="hidden-xs" href="JavaScript:nextPages('+totalPageCount+');">...</a>';
     		//在id="pageIndex"后追加页数
     		$("#addPage").after(html);
     	}
     	//输入框改变内容和点击页码时会调用此函数
     	function jump(searchWord,pageIndex){
     		//alert('搜索：'+searchWord+'页数：'+pageIndex);
     		$.ajax({
            url: "/WbusiManage/sys/manage/mFind", 
            type: "Post", 
            dataType:"json",
            data: {searchWord:searchWord,pageIndex:pageIndex}, //传入搜索字符和分页信息
            success: function (result) {
            	document.getElementById('logList').innerHTML="";
            	var html='';
            	var list=result.logList;//获取代理集合
            	//拼接98-111行的页面标签（a标签）
               	for(var i=0;i<list.length;i++){
               		html+='<div class="list-group-item"><div class="row"><div class="col-sm-12">';
                	html+='<p class="list-group-item-text" style="color:#000">';
                	html+=list[i].userName+list[i].logDetail+'</p>';
                	html+='<p class="list-group-item-text">操作时间： '+list[i].operTime+'</p>';
                	html+='</div></div></div>';
                	
               	}
               	document.getElementById('logList').innerHTML=html;
               		var page='';
               		//计算出当前页码所在显示页码中的最大页码
               		var mPage='';
               		if(result.pageIndex%5==0){
               			mPage=result.pageIndex;
               		}else{
               			mPage=(parseInt(result.pageIndex/5)*5)+5;
               		}
      
           			if(result.pageIndex>=1 && result.pageIndex<=5){//前面没有“...”，后面可能有，可以点击上一页
           				if(result.pageIndex==1){
           					page+='<a disabled="disabled"><<</a><a id="addPage" disabled="disabled"><</a>';
           				}else{
           					page+='<a href="JavaScript:turnPage(1);"><<</a><a id="addPage" href="JavaScript:turnPage('+(result.pageIndex-1)+');"><</a>';
           				}
           				if(result.totalPageCount>5){//总页数大于5，可以显示后面“...”
	           				for(var i=1;i<=5;i++){//遍历页码也不一样
	       						if(result.pageIndex==i){
	       							page+='<span id="pageIndex">'+result.pageIndex+'</span>';
	       							continue;
	       						}
	       						page+='<a class="hidden-xs" href="JavaScript:turnPage('+i+');">'+i+'</a>';
	       					}
           					page+='<a class="hidden-xs" href="JavaScript:nextPages('+result.totalPageCount+');">...</a>';
           				}else{//总页数大于5，不显示后面“...”
           					for(var i=1;i<=result.totalPageCount;i++){
	       						if(result.pageIndex==i){
	       							page+='<span id="pageIndex">'+result.pageIndex+'</span>';
	       							continue;
	       						}
	       						page+='<a class="hidden-xs" href="JavaScript:turnPage('+i+');">'+i+'</a>';
	       					}
           				}
           				if(result.pageIndex<result.totalPageCount){//不是最后一页时
           					page+='<a href="JavaScript:turnPage('+(result.pageIndex+1)+');">></a>'+
	           				'<a href="JavaScript:turnPage('+result.totalPageCount+');">>></a>';
           				}else{//是最后一页时
           					page+='<a disabled="disabled">></a><a disabled="disabled">>></a>';
           				}
           			}else if(result.pageIndex>5 && result.totalPageCount>mPage){//前面有“...”，后面也有“...”
           				//alert(888);
           				page+='<a href="JavaScript:turnPage(1);"><<</a><a id="addPage" href="JavaScript:turnPage('+(result.pageIndex-1)+');"><</a>';
           				page+='<a class="hidden-xs" href="JavaScript:prevPages('+result.totalPageCount+');">...</a>';
           				for(var i=mPage-4;i<=mPage;i++){
       						if(result.pageIndex==i){
       							page+='<span id="pageIndex">'+result.pageIndex+'</span>';
       							continue;
       						}
       						page+='<a class="hidden-xs" href="JavaScript:turnPage('+i+');">'+i+'</a>';
       					}
       					page+='<a class="hidden-xs" href="JavaScript:nextPages('+result.totalPageCount+');">...</a>';
       					page+='<a href="JavaScript:turnPage('+(result.pageIndex+1)+');">></a>'+
	           			'<a href="JavaScript:turnPage('+result.totalPageCount+');">>></a>';
           			}else if(result.pageIndex>5 && result.totalPageCount<=mPage){//前面有“...”，后面没有“...”
           				//alert(999);
           				page+='<a href="JavaScript:turnPage(1);"><<</a><a id="addPage" href="JavaScript:turnPage('+(result.pageIndex-1)+');"><</a>';
           				page+='<a class="hidden-xs" href="JavaScript:prevPages('+result.totalPageCount+');">...</a>';
           				for(var i=mPage-4;i<=result.totalPageCount;i++){
       						if(result.pageIndex==i){
       							page+='<span id="pageIndex">'+result.pageIndex+'</span>';
       							continue;
       						}
       						page+='<a class="hidden-xs" href="JavaScript:turnPage('+i+');">'+i+'</a>';
       					}
       					if(result.pageIndex<result.totalPageCount){//不是最后一页时
       						page+='<a href="JavaScript:turnPage('+(result.pageIndex+1)+');">></a>'+
	           				'<a href="JavaScript:turnPage('+result.totalPageCount+');">>></a>';
       					}else{//是最后一页时
       						page+='<a disabled="disabled">></a><a disabled="disabled">>></a>';
       					}
       					
           			}
               	//将分页元素设置到页面上
               	var pager=document.getElementById("pager");
               	pager.innerHTML=page;
            }
        	});
     	}
     	
        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
</body>
</html>
