<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸 - 管理版</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
    <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>

    
</head>
<body class="admin-browse">
        <script type="text/javascript">
            /* (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {
                        window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                    }
                } else {
                    top.document.getElementById('page_title_content').innerHTML = '代理信息';
                    if (document.body.clientWidth <= 1100) {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })(); */
        </script>
    <input type="hidden" name="navmenu_title" value="代理信息" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">代理审核</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/WbusiManage/user/Logout" id="fa_user">
                <i class="fa fa-user bt-logout icon icon-signin" style="font-size:1.3em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
            
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<input data-val="true" data-val-required="JUserId 字段是必需的。" id="JUserId" name="JUserId" type="hidden" value="9184c506-8894-4a5a-be3c-6946d69b9b03" />
<div class="bootcards-cards">
	<div class="panel panel-default margin">
        <div class="list-group">
            <a class="list-group-item" href="javascript:history.back();">
                <h4 class="list-group-item-heading" style="color: #009900;">
                    返回
                </h4>
            </a>
        </div>
    </div>
    <div class="panel panel-default margin">
        <div class="list-group">
            <div class="list-group-item">
                <i class="fa fa-user" style="font-size:1.3em;"></i>
                <label>姓名</label>
                <h4 class="list-group-item-heading">${user.name}</h4>
            </div>
            <!-- <a class="list-group-item" href="/Admin/Agencies/IdentityCard/9184c506-8894-4a5a-be3c-6946d69b9b03">
                <label>证件照</label>
                <h4 class="list-group-item-heading">220202199102204211</h4>
            </a> -->
        </div>
    </div>
    <div class="panel panel-default margin">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">联系方式</h3>
        </div>
        <div class="list-group">
            <div class="list-group-item">
                <label>联系电话</label>
                <h4 class="list-group-item-heading">${user.phone}</h4>
            </div>
            <div class="list-group-item">
                <label>邮箱</label>
                <h4 class="list-group-item-heading">${user.email}</h4>
            </div>
            <!-- <div class="list-group-item">
                <label>微信号</label>
                <h4 class="list-group-item-heading">364761860</h4>
            </div>
            <div class="list-group-item">
                <label>地址</label>
                <h4 class="list-group-item-heading"></h4>
            </div> -->
        </div>
    </div>
    <div class="panel panel-default margin">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">代理信息</h3>
        </div>
        <div class="list-group">
            <div class="list-group-item">
                    <label>审核状态</label>
                    <h4 class="list-group-item-heading">等待审核</h4>
            </div>
           <!--  <div class="list-group-item">
                    <label>申请级别</label>
                <h4 class="list-group-item-heading">
                    <span>营销员</span>
                </h4>
            </div> -->
                <!-- <div class="list-group-item">
                    <label>所属上级</label>
                    <h4 class="list-group-item-heading">公司直属</h4>
                </div>
                <div class="list-group-item">
                    <label>所在区域</label>
                    <h4 class="list-group-item-heading">总区</h4>
                </div>-->
                <div class="list-group-item">
                    <label>注册时间</label>
                    <h4 class="list-group-item-heading">
                    <fmt:formatDate value="${user.regDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
                    </h4>
                </div> 
        </div>
    </div>
    <div class="panel panel-none" style="margin-bottom:0">
        <div class="panel-body" style="padding-top:0">
                
<a class="btn btn-desktop-primary btn-block" data-ajax-method="POST" 
	href="javascript:;" onclick="agree('${user.id}',1);" style="margin-top:5px">同意</a>
	
<a class="btn btn-desktop-danger btn-block" href="javascript:;" onclick="refuse('${user.id}',3);"
 style="margin-top:5px">拒绝</a>

        </div>
    </div>
</div>
        </div>
    </div>
    <!-- slide in menu (mobile only) -->
    <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>

    
    <script type="text/javascript">
		
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });
		function agree(id,state){
			if(confirm('确定同意吗？')){
				location.href='/WbusiManage/sys/agent/agree?id='+id+'&state='+state;
			}
		}
		function refuse(id,state){
			if(confirm('确定拒绝吗？')){
				location.href='/WbusiManage/sys/agent/refuse?id='+id+'&state='+state;
			}
		}
        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/WbusiManage/js/weixinsdk.js"></script>

    <script type="text/javascript">
        if (isWeixin) {
            $.get('/home/getShare', function (data) {
                weixinService.init(data.title, data.desc, data.imgUrl, data.link);
            })
        }
    </script>
</body>
</html>
