<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸 - 管理版</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>

    
</head>
<body class="admin-browse">
    <input type="hidden" name="navmenu_title" value="产品管理" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">添加产品</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/WbusiManage/user/Logout" id="fa_user">
                <i class="fa fa-user bt-logout icon icon-signin" style="font-size:1.3em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
            
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<div class="bootcards-cards">
<form action="/WbusiManage/sys/wang/savegood" id="addgoodform" method="post" enctype="multipart/form-data">        
<div class="panel panel-default">
            
            <input id="GoodId" name="GoodId" type="hidden" value="" />
            <input id="SubGoods" name="SubGoods" type="hidden" value="" />
            <div class="panel-heading clearfix">
            	<a href="javascript:history.back();">
            		<button name="btn_cancel" type="button" class="btn btn-success pull-left">
                        返回
                    </button>
                </a>
                    <button name="btn_save" type="submit" class="btn btn-success pull-right">
                        保存
                    </button>
            </div>
            <div class="list-group">
            	<div class="list-group-item">
            		<label class="list-group-item-text" for="Title">温馨提示：带*号为必填项</label>
            	</div>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Title">* 产品名称</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-required="产品名称必填" 
                        id="Title" name="proName" type="text" value=""/>
                        <span class="field-validation-valid text-danger" data-valmsg-for="Title" data-valmsg-replace="true"></span>
                    </div>
                </div>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Spec">* 产品单位</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-required="产品单位必填" id="Spec" name="proUnit" type="text" value="" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Spec" data-valmsg-replace="true"></span>
                    </div>
                </div>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Weight">* 库存数量</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-number="The field 库存数量 must be a number." data-val-range="库存数量在0~9999之间" data-val-required="库存数量在0~9999之间" id="Weight" name="proCount" type="text" value="" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Weight" data-valmsg-replace="true"></span>
                    </div>
                </div>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Price">* 产品价格</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-number="The field 价格  must be a number." data-val-required="产品价格必填" id="Price" name="proPrice" type="text" value="" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Price" data-valmsg-replace="true"></span>
                    </div>
                </div>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Price">产品描述</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true"  id="Price" name="proDesc" type="text" value="" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Price" data-valmsg-replace="true"></span>
                    </div>
                </div>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="ImgUrl">产品主图</label>
                    <div class="list-group-item-heading">
                        <label class="bt-upload" for="inputUp">
                            <input type="file" name="attachs" id="ImgUrl" onchange="viewImage(this)"/>
                            <!-- <img width="80" src="" height="60" id="logobase" alt="图片" style="display:none" /> -->
                        </label>
                        <!-- 用于显示上传的图片 -->
                		<img id="preview" width="0" height="0" style="display:none" />
                        <!-- <div id="filebox" style="display:none;">
                            <input type="file" name="files" id="inputUp" accept="image/*" data-type="good" /> 
                        </div>-->
                        <span class="fa fa-spinner fa-spin inline-loading-main" style="display: none"></span>
                        <input data-val="true" data-val-required="产品主图片必填" id="ImgUrl" type="hidden" value="" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="ImgUrl" data-valmsg-replace="true"></span>
                    </div>
                </div>
                <div class="list-group-item">
                    <button name="btn_save" class="btn btn-desktop-primary">
                        保存
                    </button>
                </div>
            </div>
        </div>
</form></div>

        </div>
    </div>
    <!-- slide in menu (mobile only) -->
    <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>
    <script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
    <script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/WbusiManage/src/scripts/takeit.js"></script>
    <script src="/WbusiManage/js/app.js"></script>

    
    <script src="/WbusiManage/js/help.js"></script>

    <script type="text/javascript">
        triggerLocalUploadService.start('inputUp', 'ImgUrl');
		//上传图片后预览图片
		function viewImage(file){
            var preview = document.getElementById('preview');
            if(file.files && file.files[0]){
                //火狐下
                preview.style.display = "block";
                preview.style.width = "150px";
                preview.style.height = "180px";
                preview.src = window.URL.createObjectURL(file.files[0]);
            }else{
                //ie下，使用滤镜
                file.select();
                var imgSrc = document.selection.createRange().text;
                var localImagId = document.getElementById("localImag"); 
                //必须设置初始大小 
                localImagId.style.width = "250px"; 
                localImagId.style.height = "200px"; 
                try{ 
                localImagId.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale)";
                locem("DXImageTransform.Microsoft.AlphaImageLoader").src = imgSrc; 
                }catch(e){ 
                alert("您上传的图片格式不正确，请重新选择!"); 
                return false; 
                } 
                preview.style.display = 'none'; 
                document.selection.empty(); 
                } 
                return true; 
        	}

        $('[name="btn_save"]').click(function () {
            var f = $("#addgoodform").valid();
            var url = $("input[name=ImgUrl]").val();

            if (url == "") {
                $("span[data-valmsg-for=ImgUrl]").removeClass("field-validation-valid").addClass("field-validation-error");
                $("span[data-valmsg-for=ImgUrl]").html("产品图片必填");
                f = false;
            }

            $('input[data-name="LevelPrice"]').each(function (index, e) {
                $(e).attr('name', "LevelPrice")
            });

            return f;
        });

        $(document).on("click", "[name=ImgDesc]", function () {
            if (confirm("删除图片")) {
                $.post("/Admin/Good/DeleteImgByUrl", { url: $(this).attr("src") });
                $(this).parent().remove();
            }
        })
    </script>

    <script type="text/javascript">

        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
</body>
</html>
