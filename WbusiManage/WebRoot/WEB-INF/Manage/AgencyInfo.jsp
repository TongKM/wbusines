<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸 - 管理版</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
    <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>
    
    <style type="text/css">
        .divWeiXin:before {
            content: none;
        }

        #divZhiFuBao:before {
            content: none;
        }
    </style>

</head>
<body class="admin-browse">
        <script type="text/javascript">
            /* (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {
                        window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                    }
                } else {
                    top.document.getElementById('page_title_content').innerHTML = '操作中心';
                    if (document.body.clientWidth <= 1100) {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })(); */
        </script>
    <input type="hidden" name="navmenu_title" value="操作中心" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">代理信息</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/WbusiManage/user/Logout" id="fa_user">
                <i class="fa fa-user bt-logout icon icon-signin" style="font-size:1.3em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
            
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<div class="bootcards-cards">
	<div class="panel panel-default margin">
        <div class="list-group">
            <a class="list-group-item" href="javascript:history.back();">
                <h4 class="list-group-item-heading" style="color: #009900;">
                    返回
                </h4>
            </a>
        </div>
    </div>
    <div class="panel panel-default margin">
        <div class="list-group">
            <a class="list-group-item" >
                <i class="fa fa-user" style="font-size:1.3em;"></i>
                <h4 class="list-group-item-heading">
                    ${user.name}
                    <small>( 审核通过 )</small>
                </h4>
                <p class="list-group-item-text">联系电话：${user.phone}</p>
                <p class="list-group-item-text">电子邮箱：${user.email}</p>
                <p class="list-group-item-text">所在地：${user.address}</p>
            </a>
        </div>
    </div>
        
        <div class="panel panel-default margin">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left">订单</h3>
            </div>
            <div class="list-group">

                    <a class="list-group-item">
                        <i class="icon icon-order-amount-new icon-2x pull-left"></i>
                        <h4 class="list-group-item-heading">订单金额统计</h4>
                        <p class="list-group-item-text">${user.totalAmount}</p>
                    </a>
                    <a class="list-group-item">
                        <i class="icon icon-product-new icon-2x pull-left"></i>
                        <h4 class="list-group-item-heading">商品件数统计</h4>
                        <p class="list-group-item-text">${user.totalCount}</p>
                    </a>
            </div>
        </div>
    
        <!-- <div class="panel panel-default margin">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left">考核</h3>
            </div>
            <div class="list-group">
                <a class="list-group-item" href="/Admin/Agencies/VerificationDetails/dcef3f86-f630-4c7f-ac04-aa9f52ff62e5">
                    <i class="icon icon-assessment-new icon-2x pull-left"></i>
                    <h4 class="list-group-item-heading">
                        考核统计
                    </h4>
                    <p class="list-group-item-text">
                        对该代理资质进行考核
                    </p>
                </a>
                                       
            </div>
        </div> -->

        <div class="panel panel-default margin">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left">管理</h3>
            </div>
            <div class="list-group">
                <a class="list-group-item" href="javascript:cancel('${user.id}');" >
                    <i class="icon icon-administrator icon-2x pull-left" style="width:34px;font-size:1.6em"></i>
                    <h4 class="list-group-item-heading">点击撤销代理资格</h4>
                    <p class="list-group-item-text">
                    </p>
                </a>
            </div>
        </div>
</div>

        </div>
    </div>
    <!-- slide in menu (mobile only) -->
    <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>

    
    <script type="text/javascript">

        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });
		function cancel(id){
			if(confirm('确认撤销该代理资格吗？')){
				location.href='/WbusiManage/sys/agent/cancel?id='+id;
			}
		}
        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/WbusiManage/js/weixinsdk.js"></script>

    <script type="text/javascript">
        if (isWeixin) {
            $.get('/home/getShare', function (data) {
                weixinService.init(data.title, data.desc, data.imgUrl, data.link);
            })
        }
    </script>
</body>
</html>
