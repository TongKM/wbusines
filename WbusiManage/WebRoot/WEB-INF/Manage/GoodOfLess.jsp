<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸 - 管理版</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
    <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>
    <link href="/WbusiManage/css/home.css" rel="stylesheet"/>
    <link href="/WbusiManage/src/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

    <style type="text/css">
        .good-off p,.good-off h4{
           text-decoration:line-through !important;
        }
    </style>

</head>
<body class="admin-browse">
        <script type="text/javascript">
            /* (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {
                        window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                    }
                } else {
                    top.document.getElementById('page_title_content').innerHTML = '产品列表';
                    if (document.body.clientWidth <= 1100) {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })(); */
        </script>
    <input type="hidden" name="navmenu_title" value="产品管理" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">待补货产品</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/WbusiManage/user/Logout" id="fa_user">
                <i class="fa fa-user bt-logout icon icon-signin" style="font-size:1.3em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<div class="bootcards-list">
    <div class="panel panel-default">
        <div class="panel-body">
<form action="/Admin/Good" class="search-form" id="goodForm" method="post" name="goodForm">                <input type="hidden" name="pageNo">
                <div class="row">
                        
                        
                </div>
</form>        </div>
        <div id="productList">
          <div class="list-group" id="proList" style="margin-bottom:0">
          <c:forEach items="${proList}" var="pro">
            <a class="list-group-item" href="/WbusiManage/sys/pro/updateCount?id=${pro.id}">
                <div class="row">
                    <div class="col-lg-12">
                    <c:choose>
                    	<c:when test="${fn:contains(pro.proPicPath, '.')}">
                    	<img class="img-responsive img-rounded pull-left" src="${pro.proPicPath}" 
	                        style="width:60px;height:60px;">
                    	</c:when>
                    	<c:otherwise>
                    	<img class="img-responsive img-rounded pull-left" src="/WbusiManage/src/images/site/no_photo.png" 
	                        style="width:60px;height:60px;">
                    	</c:otherwise>
                    </c:choose>

                        <div class="pull-left">
                            <h4 class="list-group-item-heading">${pro.proName}</h4>
                                <p class="list-group-item-text">
 									单价：${pro.proPrice}
                                </p>
                                <p class="list-group-item-text">
 									库存：${pro.proCount}
                                </p>
                                <c:if test="${pro.proStatus==1}">
	                                <p class="list-group-item-text">
	 									状态：上架
	                                </p>
                                </c:if>
                                <c:if test="${pro.proStatus==2}">
	                                <p class="list-group-item-text">
	 									状态：下架
	                                </p>
                                </c:if>
                        </div>
                    </div>
                </div>
            </a>
            </c:forEach>
    	</div>
    </div>
</div>
    
        <div class="text-right" style="padding:10px 0;">
            <span id="proCount">共 ${fn:length(proList)} 件产品。</span>
        </div>
</div>

        </div>
    </div>
    <!-- slide in menu (mobile only) -->
    <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>
    
    <script src="/WbusiManage/src/moment/moment.js"></script>
    <script src="/WbusiManage/src/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        filterCanvas.init(44);
       
    </script>

    <script type="text/javascript">

        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
    	//左边菜单对应项高亮
        /* var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        } */
    </script>
    <script src="/WbusiManage/js/weixinsdk.js"></script>

    <script type="text/javascript">
        if (isWeixin) {
            $.get('/home/getShare', function (data) {
                weixinService.init(data.title, data.desc, data.imgUrl, data.link);
            });
        }
    </script>
</body>
</html>
