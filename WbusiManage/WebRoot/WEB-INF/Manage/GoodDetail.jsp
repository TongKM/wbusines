<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸 - 管理版</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
    <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>

    <!-- <link rel="stylesheet" href="/WbusiManage/src/jquery-ui-1.11.4/jquery-ui.min.css"> -->

</head>
<body class="admin-browse">
        <script type="text/javascript">
            /* (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {
                        window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                    }
                } else {
                    top.document.getElementById('page_title_content').innerHTML = '产品详情';
                    if (document.body.clientWidth <= 1100) {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })(); */
        </script>
    <input type="hidden" name="navmenu_title" value="产品管理" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">产品详情</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/WbusiManage/user/Logout" id="fa_user">
                <i class="fa fa-user bt-logout icon icon-signin" style="font-size:1.3em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
            
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<div class="bootcards-cards">
    <div class="panel panel-default">
        <div class="panel-heading clearfix text-right">
        <!-- history.back(); -->
            <a class="btn btn-primary" href="javascript:location.href='/WbusiManage/sys/pro/Good';">
                    返回
            </a>
            <c:if test="${sessionScope.pro.proStatus==1}">
            	<a class="btn btn-primary"  href="/WbusiManage/sys/pro/down?proStatus=2&id=${sessionScope.pro.id}">
				下架</a>
            </c:if>
            <c:if test="${sessionScope.pro.proStatus==2}">
            	<a class="btn btn-primary" href="/WbusiManage/sys/pro/up?proStatus=1&id=${sessionScope.pro.id}">
				上架</a>
            </c:if>
            
			<a class="btn btn-danger" onclick="del('${sessionScope.pro.id}');" href="javascript:;">
			删除</a>                
			
			<a class="btn btn-primary pull-right" href="/WbusiManage/sys/pro/updatePro?id=${sessionScope.pro.id}">
                 <span>编辑</span>
            </a>
        </div>
        <div class="list-group">
            <div class="list-group-item">
                <label for="Title">产品名称</label>
                <h4 class="list-group-item-heading">${sessionScope.pro.proName}</h4>
            </div>
            <div class="list-group-item">
                <label>产品状态</label>
                <c:if test="${sessionScope.pro.proStatus==1}">
                	<h4 class="list-group-item-heading">已上架</h4>
                </c:if>
                <c:if test="${sessionScope.pro.proStatus==2}">
                	<h4 class="list-group-item-heading">已下架</h4>
                </c:if>
            </div>
            
            <div class="list-group-item">
                <label for="ArtNo">产品编号</label>
                <h4 class="list-group-item-heading">${sessionScope.pro.id}</h4>
            </div>
            <div class="list-group-item">
                <label for="Spec">产品单位</label>
                <h4 class="list-group-item-heading">${sessionScope.pro.proUnit}</h4>
            </div>
            <div class="list-group-item">
                <label for="Weight">库存数量</label>
                <h4 class="list-group-item-heading">${sessionScope.pro.proCount}</h4>
            </div>
            <div class="list-group-item">
                <label for="Price">产品单价</label>
                <h4 class="list-group-item-heading">${sessionScope.pro.proPrice}</h4>
            </div>
            <div class="list-group-item">
                <label for="Price">产品描述</label>
                <h4 class="list-group-item-heading">${sessionScope.pro.proDesc}</h4>
            </div>
            <div class="list-group-item">
                <label for="ImgUrl">产品主图</label>
                <div class="list-group-item-heading">
                	<c:choose>
                   	<c:when test="${fn:contains(sessionScope.pro.proPicPath, '.')}">
                   	<img src="${sessionScope.pro.proPicPath}" 
                        style="width:60px;height:60px;">
                   	</c:when>
                   	<c:otherwise>
                   	(暂无图片)
                   	<img src="/WbusiManage/src/images/site/no_photo.png" 
                        style="width:60px;height:60px;">
                   	</c:otherwise>
                	</c:choose>
                    <%-- <img src="${sessionScope.pro.proPicPath}" style="width: 200px;height: 200px" id="logobase" alt="图片" /> --%>
                </div>
            </div>
        </div>
    </div>
</div>

        </div>
    </div>
    <!-- slide in menu (mobile only) -->
    <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>
    
    <!-- <script type="text/javascript" src="/WbusiManage/src/jquery-ui-1.11.4/jquery-ui.js"></script> -->

    <script type="text/javascript">

        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });
		function del(id){
			if(confirm('确认删除吗？')){
				location.href='/WbusiManage/sys/pro/del?id='+id;
			}
		}
        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/WbusiManage/js/weixinsdk.js"></script>

    <script type="text/javascript">
        if (isWeixin) {
            $.get('/home/getShare', function (data) {
                weixinService.init(data.title, data.desc, data.imgUrl, data.link);
            });
        }
    </script>
</body>
</html>
