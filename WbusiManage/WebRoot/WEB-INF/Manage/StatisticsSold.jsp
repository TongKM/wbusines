<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="format-detection" content="telephone=no" />
<title>微霸 - 管理版</title>
<meta name="keyword" content="微霸">
<meta name="description" content="微霸">
<link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
<link href="/WbusiManage/src/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
<link href="/WbusiManage/css/v1.css" rel="stylesheet" />
</head>
<script src="/WbusiManage/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="/WbusiManage/js/highcharts.js" type="text/javascript"></script>
<script src="/WbusiManage/js/exporting.js" type="text/javascript"></script>

<body class="admin-browse">
	<input type="hidden" name="navmenu_title" value="销量统计" />

	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand no-break-out" title="Customers" href="#">商品销量统计</a>
			</div>
			<a class="pull-right"
				style="position: absolute; top: 12px; right: 15px;"
				href="/WbusiManage/user/Logout" id="fa_user"> <i
				class="fa fa-user bt-logout icon icon-signin"
				style="font-size:1.3em;"></i> </a> 
			<a href="/WbusiManage/sys/wang/admin"
				class="btn btn-default pull-left" id="btn_home"> <i
				class="icon icon-home" style="font-size:1.3em;padding-top: 10px;"></i> </a>
			<button type="button"
				class="btn btn-default btn-menu pull-left offCanvasToggle"
				data-toggle="offcanvas" id="btn_menu">
				<i class="fa fa-lg fa-bars"></i>
			</button>
		</div>
	</div>

	<div class="container bootcards-container" id="main">
		<div class="row">
			<div class="navmenu search-nav search-nav-2" >
				<ul class="nav">
					<!-- <li class="tt"><a data-title="订单销量统计"
						href="/WbusiManage/sys/order/toStatisticsPurchasers"> 订单销量统计 </a>
					</li> -->

					<!-- <li class="active"><a data-value="2" data-title="商品销量统计"
						href="/WbusiManage/sys/order/toStatisticsSold"> 商品销量统计 </a>
						</li> -->
				</ul>
				<div class="panel-heading clearfix text-center">
						<!-- 点击查看上一个月商品销量统计 -->
						<a class="btn btn-default pull-left" style="width:65px"
							href="/WbusiManage/sys/order/toStatisticsSold?interval=${interval+1}">
							<i class="icon-left-new"></i> </a> <label
							style="padding:0px 15px;margin-top:5px">${date }</label> <input
							type="hidden" name="interval" id="interval" value="${interval }">
						<!-- 如果interval为0，即当前时间的月份，则不能点击下一个月 -->
						<c:if test="${interval>0}">
							<a class="btn btn-default pull-right" style="width:65px;"
								href="/WbusiManage/sys/order/toStatisticsSold?interval=${interval-1}">
								<i class="icon-right-new"></i> </a>
						</c:if>
					</div>
			</div>
			
			<div class="bootcards-cards" style="padding-top:95px">
				<form id="form1">
		<div>
			<script type="text/javascript">
				/*获取json数据开始*/
				//定义变量
				$(document)
						.ready(
								function() {
								var interval=document.getElementById('interval').value;
									sb();
									function sb() {
										var jsonXData = [];
										var jsonyD1 = [];
										var jsonyD2 = [];

										//获取数据
										$
												.ajax({
													url : '/WbusiManage/sys/order/StatisticsSold',
													cache : false,
													async : false,
													data:{'interval':interval},
													success : function(data) {
														for ( var i = 0, y = 4; i < data.orderList.length; i++) {
															var rows = data.orderList[i];
															
																var Time = rows.proId;
															var SumCount;
															if (rows != null)
																SumCount = rows.oCount;
															else
																SumCount = 0;
															var IpCount;
																IpCount = 0;
															jsonXData.push(Time); //赋值
															jsonyD1.push(parseInt(SumCount));
															//jsonyD2.push(parseInt(IpCount));
															y -= 1;
														} //for
														var chart;
														chart = new Highcharts.Chart(
																{
																	chart : {
																		renderTo : 'containerliuliang',
																		//放置图表的容器
																		plotBackgroundColor : null,
																		plotBorderWidth : null,
																		defaultSeriesType : 'column' //图表类型line, spline, area, areaspline, column, bar, pie , scatter 
																	},
																	title : {
																		text : '商品销量统计报表'
																	},
																	xAxis : { //X轴数据
																		categories : jsonXData,
																		lineWidth : 2,
																		labels : {
																			align : 'center',
																			style : {
																				font : 'normal 13px 宋体'
																			}
																		}
																	},
																	yAxis : { //Y轴显示文字
																		lineWidth : 2,
																		title : {
																			text : '数量'
																		}
																	},
																	tooltip : {
																		formatter : function() {
																			return '<b>'
																					+ this.x
																					+ '</b><br/>'
																					+ this.series.name
																					+ ': '
																					+ Highcharts
																							.numberFormat(
																									this.y,
																									0);
																		}
																	},
																	plotOptions : {
																		column : {
																			dataLabels : {
																				enabled : true
																			},
																			enableMouseTracking : true
																		//是否显示title
																		}
																	},
																	series : [
																			{
																				name : '商品销量',
																				data : jsonyD1
																			}
																			 ]
																});
														$("tspan:last").hide(); //把广告删除掉
														//  } //if
													}
												});
									}
								});
			</script>
			<div id="containerliuliang"></div>
		</div>
	</form>
	<%-- <c:if test="${fn:length(orderList)==0}">
		<div class="row text-center body-padding-normal">无相关的数据</div>
	</c:if> --%>
			</div>
		</div>
		<%-- <div class="row">
			<div class="bootcards-cards" style="padding-top:80px">
			<jsp:include page="/WEB-INF/Manage/fz.jsp"></jsp:include>
			</div>
		</div> --%>
	</div>

	<!-- slide in menu (mobile only) -->

	<jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>
	


	 <script type="text/javascript">

        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
         if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        } 
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        } 
    </script> 
</body>
</html>
