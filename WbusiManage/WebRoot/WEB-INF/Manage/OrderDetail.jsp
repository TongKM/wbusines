<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>卖单详情</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>

    
</head>
<body class=admin-browse>
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {

                        if (document.body.className == 'agency-browse') {
                            window.location.href = '/App#' + window.location.pathname + window.location.search;
                            return;
                        }

                        if (document.body.className == 'admin-browse') {
                            window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                            return;
                        }
                    }
                } else {
                    if (document.body.clientWidth <= 1100 && document.body.className == 'admin-browse') {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" value="我的订单" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">卖单详情 </a>
            </div>
                <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/Admin/Employee/Details" id="fa_user">
                    <i class="fa fa-user" style="font-size:1.6em;"></i>
                </a>
                <a style="position: absolute; top: 12px; right: 45px;" href="javascript:void(0)" onclick="remoteFullModal('/Admin/Agencies/RegisterCode')">
                    代理入口
                </a>
                <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                    <i class="fa fa-lg fa-bars"></i>
                </button>
                <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                    <i class="icon icon-home" style="font-size:1.3em;"></i>
                </a>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            

<div class="bootcards-cards">
    <div id="activityCard" class="margin">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left">
                    订单：GJ636322778614033338（交易成功
                    ）
                </h3>
            </div>
            <div class="list-group">
                <div class="list-group-item">
                    <label>订单时间</label>
                    <h4 class="list-group-item-heading">2017-06-05 16:44:21</h4>
                </div>
                <div class="list-group-item">
                    <label>商品总件数</label>
                    <h4 class="list-group-item-heading">10 件</h4>
                </div>
                    <div class="list-group-item">
                        <label>发货时间</label>
                        <h4 class="list-group-item-heading">2017-06-05 16:46:10</h4>
                    </div>
                                    <div class="list-group-item">
                        <label>发货时间</label>
                        <h4 class="list-group-item-heading">2017-06-05 16:46:10</h4>
                    </div>
                <div class="list-group-item">
                    <label>实付款金额</label>
                    <h4 class="list-group-item-heading">
                        <span class="text-danger">&#165;173.00</span><span class="small-txt">(运费：0.00)</span>
                    </h4>
                </div>
            </div>
        </div>
                    <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">买家信息</h3>
                </div>
                <div class="list-group">
                        <a class="list-group-item" href="/Agency/ContactDetails/dcef3f86-f630-4c7f-ac04-aa9f52ff62e5">
                            <label>买家昵称</label>
                            <h4 class="list-group-item-heading">agency_1622584</h4>
                        </a>
                </div>
            </div>
                    <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">付款情况</h3>
                </div>
                <div class="list-group">
                    <div class="list-group-item">
                        <label>付款方式</label>
                        <h4 class="list-group-item-heading">站外支付 </h4>
                    </div>
                    <div class="list-group-item">
                        <label>付款时间</label>
                        <h4 class="list-group-item-heading">2017-06-05 16:44:21</h4>
                    </div>
                </div>
                    <div class="panel-footer">
                                                    <div class="takephoto-queue">
                                    <div class="queue-item" data-src="/src/content/images/order-pay-tpl.jpg">
                                        <div class="img-wrap">
                                            <img src="/src/content/images/order-pay-tpl.jpg" />
                                        </div>
                                    </div>
                            </div>
                    </div>
            </div>
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left">收货地址</h3>
                    <a href="/Order/EditReceivingInformation/GJ636322778614033338" class="btn btn-primary pull-right">
                        <i class="icon icon-pencil"></i>
                        <span>编辑</span>
                    </a>
            </div>
            <div class="list-group">
                <div class="list-group-item">
                    <label>收货地址</label>
                    <h4 class="list-group-item-heading">浙江省 宁波市 鄞州区 宁波市鄞州区南部商务区泰芸巷99-1健宸大厦</h4>
                </div>
                <div class="list-group-item">
                    <label>收货人</label>
                    <h4 class="list-group-item-heading">agency_1622584</h4>
                </div>
                <div class="list-group-item">
                    <label>联系电话</label>
                    <h4 class="list-group-item-heading">400-8790307</h4>
                </div>
            </div>
        </div>
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">物流信息</h3>
                            <a href="/Order/EditLogistics/GJ636322778614033338" class="btn btn-primary pull-right">
                                <i class="icon icon-pencil"></i>
                                <span>编辑</span>
                            </a>
                </div>
                <div class="list-group">
                    <div class="list-group-item">
                        <label>物流公司</label>
                        <h4 class="list-group-item-heading">无</h4>
                    </div>
                </div>
            </div>
                    <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">产品列表</h3>
                </div>
                <div class="list-block media-list order-goods">
                    <ul class="after">
                            <li class="good-item">
                                <div class="item-content">
                                    <div class="item-media">
                                        <img src="/upload/images/good/covers/201706/636320151563611769.jpg" style="width:40px;height:40px" />
                                    </div>
                                    <div class="item-inner">
                                        <div class="item-title-row">
                                            <div class="title">
                                                <div>水光针面膜</div>
                                                <div class="title-spec color-gray">150ml</div>
                                            </div>
                                            <div class="item-price">
                                                <div class="cart-o-price color-gray text-deleted hidden">&#165;5.00</div>
                                                <div class="cart-c-price">&#165;5.00</div>
                                                <div class="cart-c-number color-gray text-right">x3</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="good-item">
                                <div class="item-content">
                                    <div class="item-media">
                                        <img src="/upload/images/good/covers/201704/636286728950118565.jpg" style="width:40px;height:40px" />
                                    </div>
                                    <div class="item-inner">
                                        <div class="item-title-row">
                                            <div class="title">
                                                <div>摇摇水乳</div>
                                                <div class="title-spec color-gray">瓶</div>
                                            </div>
                                            <div class="item-price">
                                                <div class="cart-o-price color-gray text-deleted hidden">&#165;38.00</div>
                                                <div class="cart-c-price">&#165;38.00</div>
                                                <div class="cart-c-number color-gray text-right">x3</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="good-item">
                                <div class="item-content">
                                    <div class="item-media">
                                        <img src="/upload/images/good/covers/201705/636312370022997386.jpg" style="width:40px;height:40px" />
                                    </div>
                                    <div class="item-inner">
                                        <div class="item-title-row">
                                            <div class="title">
                                                <div>111</div>
                                                <div class="title-spec color-gray">111</div>
                                            </div>
                                            <div class="item-price">
                                                <div class="cart-o-price color-gray text-deleted hidden">&#165;11.00</div>
                                                <div class="cart-c-price">&#165;11.00</div>
                                                <div class="cart-c-number color-gray text-right">x4</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                    </ul>
                </div>
                <div class="list-group-item text-right">
                    <div>货款：<span class="small-block">173.00</span></div>
                    <div>运费：<span class="small-block">0.00</span></div>
                    <div>实付：<span class="small-block">173.00</span></div>
                </div>
            </div>
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left">买家备注</h3>
                    <a href="/Order/EditRemark/GJ636322778614033338" class="btn btn-primary pull-right">
                        <i class="icon icon-pencil"></i>
                        <span>编辑</span>
                    </a>
            </div>
            <div class="list-group">
                <div class="list-group-item">
                    <label>备注</label>
                    <h4 class="list-group-item-heading text-danger">这是系统自动生成订单，为了演示使用</h4>
                </div>
            </div>
        </div>
                <div class="panel panel-none text-right" style="padding-right:15px">
                                        <a href="/Admin/Refund/Add/GJ636322778614033338" class="btn btn-desktop-danger btn-small" style="margin-left:15px" name="send">申请退款</a>
        </div>
    </div>
</div>

        </div>
    </div>
        <!-- slide in menu (mobile only) -->
        <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>

    
    <script type="text/javascript">
        function handlerSuccessClose(callback) {
            if (callback.success) {
                location.reload();
                $('#alertModel').modal('hide');
            }
            else {
                var m = setTimeout(function () {
                    popMessage('错误', "关闭订单出错");
                    clearTimeout(m);
                }, 500);
            }
        }
        $('a[name="btn-nopass"]').click(function () {
            $(parent.document).find('#inner_iframe_close').hide();
            $('#editModal').modal()
        })
        $('#editModal').on('hide.bs.modal', function () {
            $(parent.document).find('#inner_iframe_close').show();
        });
    </script>

    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/js/weixinsdk?v=FuHO4EYdVtWj94At6HA29wrWQNt6iYo2nqlZNRPrmqE1"></script>

    <script type="text/javascript">
    if (isWeixin) {
        $.get('/home/getShare', function (data) {
            weixinService.init(data.title, data.desc, data.imgUrl, data.link);
        })
    }
    </script>
    <script src="/src/scripts/takeit.js"></script>
</body>
</html>
