<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸 - 管理版</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
    <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>
    <link href="/WbusiManage/css/home.css" rel="stylesheet"/>
    <script src="/WbusiManage/src/highcharts-4.2.8/highcharts.js"></script>
    
    <style type="text/css">
     .top-purchaser-container .container-title, .month-sold-chart .container-title {
		    background-color: #ffa14a;
		    border-top: 3px solid #ffa14a;
		    /* border-top-left-radius: 4px; */
		    border-top-right-radius: 4px;
		}
		
		.top-purchaser-item .panel-body {
		    padding-left: 0;
		    padding-right: 0;
		    margin-top: 5px;
		}
    </style>

</head>
<body class="admin-browse">
    <input type="hidden" name="navmenu_title" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers">管理版</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/WbusiManage/user/Logout" id="fa_user">
                <i class="fa fa-user bt-logout icon icon-signin" style="font-size:1.3em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<div class="bootcards-cards">
    <input type="hidden" id="roles" value="001,01,0101,0102,002,02,0201,0202,003,0302,0301,030201,004,04,0401,006,06,0601,0602,018,18,1801,1802,022,2201,2202,2203,2204,21,05,23,08,13,15,19,24,17,14,09,10,16,12" />
    <div class="panel panel-default user-container">
        <div class="panel-body" style="margin-top: 10px;">
            <div class="row row-table">
                <img class="pull-left user-logo" src="/WbusiManage/src/images/site/user-logo.jpg"/>
                <div>
                    <h4 class="user-name">${sessionScope.user.name}</h4>
                    <div class="text">
                        当前系统时间：<span id="result"></span>
                    </div>
                    <div class="text">
                        当前登录IP：${ip }
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row top-row">
            <div class="col-sm-6 col-xs-12 top-panel s-deal">
                <a class="col-xs-4 text-center bg-green border-radius-left" id="top_order_check"
                href="/WbusiManage/sys/pro/findByDown">
                    <i class="icon-product"></i>
                    <div><span class="desc">待上架产品</span><span class="number">${daiShangjia }</span></div>
                </a>
                <a class="col-xs-4 text-center bg-green-dark" id="top_agency_check"
                 href="/WbusiManage/sys/agent/VerifyList">
                    <i class="icon-audit-agent"></i>
                    <div><span class="desc">待审核代理</span><span class="number">${noExamine }</span></div>
                </a>
                <a class="col-xs-4 text-center bg-green border-radius-right" id="top_order_send"
                href="/WbusiManage/sys/pro/findByLess">
                    <i class="icon-delivery-order"></i>
                    <div><span class="desc">待补货产品</span><span class="number">${daiBuhuo }</span></div>
                </a>
            </div>
            <div class="col-sm-6 col-xs-12 top-panel s-login">
                <div class="col-xs-4 text-center bg-blue border-radius-left" id="top_login_yesterday_people_count">
                    <i class="icon-online-people"></i>
                    <div><span class="desc">昨日上线人数</span><span class="number">${yesterdayCount }</span></div>
                </div>
                <div class="col-xs-4 text-center bg-blue-dark" id="top_login_yesterday_count">
                    <i class="icon-frequency"></i>
                    <div><span class="desc">昨日上线次数</span><span class="number">${yesterdayTime }</span></div>
                </div>
                <div class="col-xs-4 text-center bg-blue border-radius-right" id="top_noLogin_people_count" href="/Admin/Agencies/NoLoginUser">
                    <i class="icon-not-online"></i>
                    <div><span class="desc">近期未上线</span><span class="number">${nearFuture }</span></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xs-12 middle-panel s-order">
                <div class="panel">
                    <div class="panel-body">
                        <div class="row row1">
                            <div class="col-xs-4 text-center">
                                <i class="icon-deal icon-2x icon"></i>
                            </div>
                            <div class="text-center column2" id="order_currentMonth_TotalAmount">
                                <span class="number">${monthTotal }</span>
                                <div>本月成交金额</div>
                            </div>
                        </div>
                        <div class="row2">
                            <div class="col-xs-6 column column1">
                                <div class="col-xs-4 text-center">
                                    <i class="icon-volume icon-2x icon-column"></i>
                                </div>
                                <div class="col-xs-8 text-center" id="order_currentMonth_TotalCount">
                                    <span class="number">${monthCount }</span>
                                    <div class="desc">本月成交量</div>
                                </div>
                            </div>
                            <div class="col-xs-6 column column2">
                                <div class="col-xs-4 text-center">
                                    <i class="icon-unit-price icon-2x icon-column" style="color:#23b7e5"></i>
                                </div>
                                <div class="col-xs-8 text-center" id="order_currentMonth_PerTicketSales">
                                    <span class="number">${kedanjia }</span>
                                    <div class="desc">本月客单价</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12 middle-panel s-register">
                <div class="panel">
                    <div class="panel-body">
                        <div class="row row1">
                            <div class="col-xs-4 text-center">
                                <i class="icon-apply-agent icon-2x icon"></i>
                            </div>
                            <div class="text-center column2" id="agency_apply_count">
                                <span class="number">${monthAgency }</span>
                                <div>本月申请代理数</div>
                            </div>
                        </div>
                        <div class="row2">
                            <div class="col-xs-6 column column1">
                                <div class="col-xs-4 text-center">
                                    <i class="icon-authorize  icon-2x icon-column"></i>
                                </div>
                                <div class="col-xs-8 text-center" id="agency_oauth_count">
                                    <span class="number">${monthGrant }</span>
                                    <div class="desc">本月授权数</div>
                                </div>
                            </div>
                            <div class="col-xs-6 column column2">
                                <div class="col-xs-4 text-center">
                                    <i class="icon-total-people icon-2x icon-column"></i>
                                </div>
                                <div class="col-xs-8 text-center" id="agency_total_count">
                                    <span class="number">${allGrant }</span>
                                    <div class="desc">代理总人数</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
         <div class="panel panel-default top-purchaser-container hidden-xs">
            <div class="panel-heading bg-green container-title">
                代理订单金额  Top3
            </div>
            <div class="panel-body" style="padding-bottom:0">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="panel panel-default top-purchaser-item lastMonth">
                            <div class="panel-heading">
                                <span class="bg-red">上月</span>
                            </div>
                            <div class="panel-body" id="top_purchaser_lastMonth">
                            	
                                <div class="list-group">
                                    <!-- <div class="list-group-item text-right">
                                        <div class="pull-left">
                                            <img style="width:25px;height:25px;border-radius:100%" alt="" src="/WbusiManage/src/content/images/login-logo.png">
                                            wangtao
                                        </div>
                                        <span>￥200.00</span>
                                    </div> -->
                                    
                                    <c:forEach var="order" items="${lastmonthList }">
                                    	<div class="list-group-item text-right">
                                        <div class="pull-left">
                                            <img style="width:25px;height:25px;border-radius:100%" alt="" src="/WbusiManage/src/content/images/login-logo.png">
                                            ${order.message }
                                        </div>
                                        <span>￥${order.oAmount }</span>
                                    </div>
                                    </c:forEach>
                                    
                                    
                                    <div class="list-group-item text-right">
                                        <div class="pull-left">
                                            &nbsp;
                                        </div>
                                        <span>&nbsp;</span>
                                    </div>
                                    <div class="list-group-item text-right">
                                        <div class="pull-left">
                                            &nbsp;
                                        </div>
                                        <span>&nbsp;</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-default top-purchaser-item currentMonth">
                            <div class="panel-heading">
                                <span class="bg-blue2">本月</span>
                            </div>
                            <div class="panel-body" id="top_purchaser_currentMonth">
                                <div class="list-group">
	                                <c:forEach var="order" items="${thismonthList }">
	                                    	<div class="list-group-item text-right">
	                                        <div class="pull-left">
	                                            <img style="width:25px;height:25px;border-radius:100%" alt="" src="/WbusiManage/src/content/images/login-logo.png">
	                                            ${order.message }
	                                        </div>
	                                        <span>￥${order.oAmount }</span>
	                                    </div>
                                    </c:forEach>
                                    <div class="list-group-item text-right">
                                        <div class="pull-left">
                                            &nbsp;
                                        </div>
                                        <span>&nbsp;</span>
                                    </div>
                                    <div class="list-group-item text-right">
                                        <div class="pull-left">
                                            &nbsp;
                                        </div>
                                        <span>&nbsp;</span>
                                    </div>
                                    <div class="list-group-item text-right">
                                        <div class="pull-left">
                                            &nbsp;
                                        </div>
                                        <span>&nbsp;</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-default top-purchaser-item history">
                            <div class="panel-heading">
                                <span class="bg-green-yellow">总排名</span>
                            </div>
                            <div class="panel-body" id="top_purchaser_history">
                                <div class="list-group">
                                	<!-- <div class="list-group-item text-right">
                                        <div class="pull-left">
                                          <img style="width:25px;height:25px;border-radius:100%" alt="" src="/WbusiManage/src/content/images/login-logo.png">
                                          tongkaiming
                                        </div>
                                        <span>￥630.00</span> 
                                    </div> -->
                                	
                                	<c:forEach var="order" items="${totalList }">
	                                    	<div class="list-group-item text-right">
	                                        <div class="pull-left">
	                                            <img style="width:25px;height:25px;border-radius:100%" alt="" src="/WbusiManage/src/content/images/login-logo.png">
	                                            ${order.message }
	                                        </div>
	                                        <span>￥${order.oAmount }</span>
	                                    </div>
                                    </c:forEach>
                                	
                                    <div class="list-group-item text-right">
                                        <div class="pull-left">
                                            &nbsp;
                                        </div>
                                        <span>&nbsp;</span>
                                    </div>
                                    <div class="list-group-item text-right">
                                        <div class="pull-left">
                                            &nbsp;
                                        </div>
                                        <span>&nbsp;</span>
                                    </div>
                                    <div class="list-group-item text-right">
                                        <div class="pull-left">
                                            &nbsp;
                                        </div>
                                        <span>&nbsp;</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="panel panel-default month-sold-chart">
            <div class="panel-heading text-left container-title">
                近6个月销量统计
            </div>
            <div class="panel-body">
                <div class="bootcards-chart-canvas" id="month_sold_chart" style="height:360px;background:#fff"></div>
            </div>
        </div> -->
</div>

        </div>
    </div>
    <!-- slide in menu (mobile only) -->
    
    <script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
    <script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/WbusiManage/src/scripts/takeit.js"></script>
    <script src="/WbusiManage/js/app.js"></script>
    <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>
    
    <script type="text/javascript">
    	bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });
		function currentTime()
		{
		 //创建Date对象
		 var today = new Date();
		 //分别取出年、月、日、时、分、秒
		 var year = today.getFullYear();
		 var month = today.getMonth()+1;
		 var day = today.getDate();
		 var hours = today.getHours();
		 var minutes = today.getMinutes();
		 var seconds = today.getSeconds();
		 //如果是单个数，则前面补0
		 month  = month<10  ? "0"+month : month;
		 day  = day <10  ? "0"+day : day;
		 hours  = hours<10  ? "0"+hours : hours;
		 minutes = minutes<10 ? "0"+minutes : minutes;
		 seconds = seconds<10 ? "0"+seconds : seconds;
		  
		 //构建要输出的字符串
		 var str = year+"/"+month+"/"+day+" "+hours+":"+minutes+":"+seconds;
		 return str;
		}
		setInterval(function(){$('#result').html(currentTime());},1000);
	</script>
</body>
</html>


