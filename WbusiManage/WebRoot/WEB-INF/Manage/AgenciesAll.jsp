<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸 - 管理版</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
    <jsp:include page="/WEB-INF/jsp/css.jsp"></jsp:include>
    
</head>
<body class="admin-browse">
        <script type="text/javascript">
            /* (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {
                        window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                    }
                } else {
                    top.document.getElementById('page_title_content').innerHTML = '全部代理';
                    if (document.body.clientWidth <= 1100) {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })(); */
        </script>
    <input type="hidden" name="navmenu_title" value="代理管理" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">全部代理</a>
            </div>
            <a class="pull-right" style="position: absolute; top: 12px; right: 15px;" href="/WbusiManage/user/Logout" id="fa_user">
                <i class="fa fa-user bt-logout icon icon-signin" style="font-size:1.3em;"></i>
            </a>
            <a href="/WbusiManage/sys/wang/admin" class="btn btn-default pull-left" id="btn_home">
                <i class="icon icon-home" style="font-size:1.3em"></i>
            </a>
            <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
                <i class="fa fa-lg fa-bars"></i>
            </button>
        </div>
    </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            
<!-- <div class="navmenu search-nav search-nav-2">
    <ul class="nav">
        <li class="active">
            <a data-title="全部代理" href="/Admin/Agencies/All">
                全部代理
            </a>
        </li>
    </ul>
</div> -->
<div class="bootcards-list ">
    <div class="panel panel-default margin-top-nav">
        <div class="panel-body">
<form action="/Admin/Agencies/All" class="search-form" method="post" name="agencyForm" role="form"><input id="searchModel_RegionId" name="searchModel.RegionId" type="hidden" value="" />
	<div class="row">
          <div class="col-xs-9">
              <div class="form-group">
              <!-- 按名字查询代理 -->
                  <input class="form-control search-txt" id="searchWord" style="width: 350px;" 
                  name="searchModel.Keyword" placeholder="请输入名字" type="text" value="" />
                  <i class="fa fa-search"></i>
              </div>
          </div>
          <div class="col-xs-3">
              <!-- <button type="button" class="btn btn-default btn-menu pull-left btn-block" data-toggle="off-filter-canvas">
                  <i class="icon-screen-o-new"></i>
                  <span>筛选</span>
              </button> -->
          </div>
     </div>
</form>        </div>
	<!-- 循环输出代理列表 -->
	
    <div class="list-group" id="agentList">
         <a class="list-group-item" href="/WbusiManage/sys/agent/AgencyInfo">
             <div class="row">
                 <div class="col-sm-12">
                   <i class="fa fa-user" style="font-size:1.3em;"></i>
                     <h4 class="list-group-item-heading">
                         <span>名字</span>
                     </h4>
                     <p class="list-group-item-text">微信号_636322762480597367</p>
                     <p class="list-group-item-text">
                         联系电话：<span class="text-danger">12233</span>
                     </p>
                 </div>
             </div>
         </a>
    </div>
<div class="pager" id="pager" data-action="/Admin/Agencies/All?Status=-1&amp;PageIndex=%23&amp;Sort=TotalSale%20DESC" 
data-current="1" data-updateTargetId="">
<!-- <button class="btn btn-default pull-right" onclick="turnPage('end');">末页</button>
<button class="btn btn-default pull-right" onclick="turnPage('next');">下一页</button>
<button class="btn btn-default pull-right" onclick="turnPage('prev');">上一页</button>
<button class="btn btn-default pull-right" onclick="turnPage('first');">首页</button>
 <div class="pull-left" id="info" style="font-size: 16px">&nbsp;共条记录&nbsp;&nbsp;
第<span id="pageIndex" style="background: none;border-width: 0px;color: black;"></span>
页/<span id="totalPageCount" style="background: none;border-width: 0px;color: black;"></span>页
</div> -->

<a disabled="disabled"><<</a>
<a id="addPage" disabled="disabled"><</a>
<span id="pageIndex">1</span>
<a class="hidden-xs" data-page="2" href="JavaScript:turnPage();">2</a>
<a class="hidden-xs" data-page="3" href="JavaScript:turnPage();">3</a>
<a class="hidden-xs" data-page="4" href="JavaScript:turnPage();">4</a>
<a class="hidden-xs" data-page="5" href="JavaScript:turnPage();">5</a>
<a data-page="6" href="JavaScript:nextPages();">...</a>
<a data-page="2" href="javascript:;">></a>
<a data-page="54" href="/Admin/Agencies/All?PageIndex=54">>></a>

</div>
</div>
</div>
<div class="navmenu off-filter-canvas offcanvas-right">
<form action="/Admin/Agencies/All" method="post" name="searchform" style="padding:15px 8px 0px 8px"><input id="searchModel_Keyword" name="searchModel.Keyword" type="hidden" value="" />
<div class="form-group">
            <label>代理等级</label>
            <select class="form-control" data-val="true" data-val-number="The field LevelId must be a number." id="searchModel_LevelId" name="searchModel.LevelId"><option value="">全部等级</option>
<option value="1">联合创始人</option>
<option value="20">一级</option>
<option value="10">总经销</option>
<option value="6">大区代理</option>
<option value="9">四级代理</option>
<option value="11">玫瑰</option>
<option value="19">VIP顾客</option>
<option value="7">大区合伙人</option>
<option value="21">13</option>
<option value="14">大区合伙人1</option>
<option value="8">国代</option>
<option value="15">皇冠代理</option>
<option value="16">一级代理</option>
<option value="17">黄金代理</option>
<option value="18">白银代理</option>
<option value="2">省代</option>
<option value="3">市代</option>
<option value="13">县代</option>
<option value="12">营销员</option>
</select>
        </div>
        <div class="form-group">
            <label>所在大区</label>
            <select class="form-control" id="searchModel_RegionId" name="searchModel.RegionId"><option value="">全部大区</option>
<option value="9ebf659e-d26a-e711-80e6-8770eed734de">shaoyang</option>
<option value="1caedc14-ba71-e711-80e6-8770eed734de">孙玮团队</option>
<option value="f3c8a6cd-c59e-e711-80e9-95f885448e43">鲁西南</option>
<option value="a0ae0950-c2af-e711-80e9-95f885448e43">哈哈哈</option>
<option value="144c22ad-9bb4-e711-80e9-95f885448e43">dongfang</option>
<option value="7f9169a0-3eb7-e711-80e9-95f885448e43">888</option>
<option value="88807b4e-3fc7-e711-80e9-95f885448e43">广东</option>
<option value="b31c515a-84c8-e711-80e9-95f885448e43">华东区</option>
<option value="cbc1cd6c-71d2-e611-80e3-d069e0bf7646">官方</option>
<option value="d9e2e9b0-c2d4-e611-80e3-d069e0bf7646">健康大使</option>
<option value="45925652-9fdd-e611-80e3-d069e0bf7646">省级合伙人</option>
<option value="b4cdd67d-9fdd-e611-80e3-d069e0bf7646">市级合伙人</option>
<option value="53bb6589-9fdd-e611-80e3-d069e0bf7646">区(县)级合伙人</option>
<option value="23b659f2-c9de-e611-80e3-d069e0bf7646">测试区</option>
<option value="e8d58743-66ec-e611-80e3-d069e0bf7646">省区合伙人</option>
<option value="8569b147-06fb-e611-80e3-d069e0bf7646">飞虎队</option>
<option value="c63d4088-4514-e711-80e3-d069e0bf7646">打包</option>
<option value="32a82dd1-c325-e711-80e3-d069e0bf7646">总区</option>
<option value="89d39ac3-305f-e711-80e5-d7ee49315e86">陈志</option>
<option value="ec30d3bc-bcab-e611-81b8-d885f0e5301f">红领巾队0</option>
<option value="b9405a02-bdab-e611-81b8-d885f0e5301f">执行董事</option>
<option value="617d16b4-986a-e611-a37a-f9a9cc679708">非羽</option>
</select>
        </div>
        <div class="form-group">
            <label>起批限制</label>
            <select class="form-control" id="searchModel_IsRequiredBathRule" name="searchModel.IsRequiredBathRule"><option value="">请选择</option>
<option value="True">开启</option>
<option value="False">关闭</option>
</select>
        </div>
        <div class="form-group">
            <label>考核状态</label>
            <select class="form-control" id="searchModel_IsAutoUpdateLevel" name="searchModel.IsAutoUpdateLevel"><option value="">请选择</option>
<option value="True">开启</option>
<option value="False">关闭</option>
</select>
        </div>
        <div class="form-group">
            <label>代理状态</label>
            <select class="form-control" data-val="true" data-val-number="The field Status must be a number." id="searchModel_Status" name="searchModel.Status"><option selected="selected" value="-1">全部代理</option>
<option value="3">被冻结</option>
<option value="1">被取消资质</option>
</select>
        </div>
        <div class="form-group">
            <label>排序</label>
            <select class="form-control" id="searchModel_Sort" name="searchModel.Sort"><option selected="selected" value="TotalSale Desc">售出排序</option>
<option value="TotalBuy Desc">买入排序</option>
</select>
        </div>
        <input type="submit" value="确定" class="btn btn-desktop-primary btn-block" />
</form></div>

        </div>
    </div>
    <!-- slide in menu (mobile only) -->
    <jsp:include page="/WEB-INF/jsp/adminMenu.jsp"></jsp:include>
    
    <script type="text/javascript">
        filterCanvas.init(44);
    </script>

    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });
        
		window.onload=function(){
  			var searchWord=document.getElementById("searchWord");
  			jump('', 1);
  			//输入框改变内容时触发
     		searchWord.oninput=function(){
       		//alert(1);
       		jump(searchWord.value,1);//默认显示第一页
     	};
};
		//点击页数时进入的函数
     	function turnPage(pageIndex){
     		var searchWord=document.getElementById("searchWord");
     		jump(searchWord.value,pageIndex);
     	}
     	function nextPages(totalPageCount){//点击“...”显示后面页码
     		//获取当前显示的最后一个页数
     		var last=$(".hidden-xs:last").text();
     		if(last=='...'){
     			last=$(".hidden-xs:last").prev().text();
     		}
     		//去掉原先页码的元素
     		$(".hidden-xs").remove();
     		$("#pageIndex").remove();
     		var html="";
   			html+='<a class="hidden-xs" href="JavaScript:prevPages('+totalPageCount+');">...</a>';
   			if(totalPageCount-parseInt(last)>=5){//判断之后显示的页码有几个（5个）
   				for(var i=parseInt(last)+1;i<=parseInt(last)+5;i++){
   				html+='<a class="hidden-xs" class="pages" href="JavaScript:turnPage('+i+');">'+i+'</a>';
   				}
   			}else{
   				for(var i=parseInt(last)+1;i<=totalPageCount;i++){//判断之后显示的页码有几个（小于5个）
   				html+='<a class="hidden-xs" class="pages" href="JavaScript:turnPage('+i+');">'+i+'</a>';
   				}
   			}
   			if((totalPageCount-last)>=5){//判断是否需要向后显示页码的“...”
   			//增加向后显示页码的“...”
   			html+='<a class="hidden-xs" href="JavaScript:nextPages('+totalPageCount+');">...</a>';
   			}
     		//在id="pageIndex"后追加页数
     		$("#addPage").after(html);
     	}
     	function prevPages(totalPageCount){//点击“...”显示前面页码
     		//获取当前显示的第一个页数
     		var first=$(".hidden-xs:first").text();
     		if(first=='...'){
     			first=$(".hidden-xs:first").next().text();
     		}
     		//去掉原先页码的元素
     		$(".hidden-xs").remove();
     		$("#pageIndex").remove();
     		var html="";
     		if((parseInt(first)-5)>5){//判断是否需要向前显示页码的“...”
     			//增加向前显示页码的“...”
     			html+='<a class="hidden-xs" href="JavaScript:prevPages('+totalPageCount+');">...</a>';
     		}
     			for(var i=parseInt(first)-5;i<first;i++){
     				html+='<a class="hidden-xs" class="pages" href="JavaScript:turnPage('+i+');">'+i+'</a>';
     			}
     			html+='<a class="hidden-xs" href="JavaScript:nextPages('+totalPageCount+');">...</a>';
     		//在id="pageIndex"后追加页数
     		$("#addPage").after(html);
     	}
     	//输入框改变内容和点击页码时会调用此函数
     	function jump(searchWord,pageIndex){
     		//alert('搜索：'+searchWord+'页数：'+pageIndex);
     		$.ajax({
            url: "/WbusiManage/sys/agent/mFind", 
            type: "Post", 
            dataType:"json",
            data: {searchWord:searchWord,pageIndex:pageIndex}, //传入搜索字符和分页信息
            success: function (result) {
            	document.getElementById('agentList').innerHTML="";
            	var html='';
            	var list=result.agentList;//获取代理集合
            	//拼接98-111行的页面标签（a标签）
               	for(var i=0;i<list.length;i++){
               		html+='<a class="list-group-item" href="/WbusiManage/sys/agent/AgencyInfo?id='+list[i].id+'">';
                	html+='<div class="row"><div class="col-sm-12">';
                	html+='<i class="fa fa-user" style="font-size:1.3em;"></i>';
                	html+='<h4 class="list-group-item-heading"><span>'+list[i].name+'</span></h4>';
                	html+='<p class="list-group-item-text">'+list[i].email+'</p>';
                	html+='<p class="list-group-item-text">联系电话：<span class="text-danger">'+list[i].phone+'</span>';
                	html+='</p></div></div></a>';
               	}
               	document.getElementById('agentList').innerHTML=html;
               		var page='';
               		//计算出当前页码所在显示页码中的最大页码
               		var mPage='';
               		if(result.pageIndex%5==0){
               			mPage=result.pageIndex;
               		}else{
               			mPage=(parseInt(result.pageIndex/5)*5)+5;
               		}
      
           			if(result.pageIndex>=1 && result.pageIndex<=5){//前面没有“...”，后面可能有，可以点击上一页
           				if(result.pageIndex==1){
           					page+='<a disabled="disabled"><<</a><a id="addPage" disabled="disabled"><</a>';
           				}else{
           					page+='<a href="JavaScript:turnPage(1);"><<</a><a id="addPage" href="JavaScript:turnPage('+(result.pageIndex-1)+');"><</a>';
           				}
           				if(result.totalPageCount>5){//总页数大于5，可以显示后面“...”
	           				for(var i=1;i<=5;i++){//遍历页码也不一样
	       						if(result.pageIndex==i){
	       							page+='<span id="pageIndex">'+result.pageIndex+'</span>';
	       							continue;
	       						}
	       						page+='<a class="hidden-xs" href="JavaScript:turnPage('+i+');">'+i+'</a>';
	       					}
           					page+='<a class="hidden-xs" href="JavaScript:nextPages('+result.totalPageCount+');">...</a>';
           				}else{//总页数大于5，不显示后面“...”
           					for(var i=1;i<=result.totalPageCount;i++){
	       						if(result.pageIndex==i){
	       							page+='<span id="pageIndex">'+result.pageIndex+'</span>';
	       							continue;
	       						}
	       						page+='<a class="hidden-xs" href="JavaScript:turnPage('+i+');">'+i+'</a>';
	       					}
           				}
           				if(result.pageIndex<result.totalPageCount){//不是最后一页时
           					page+='<a href="JavaScript:turnPage('+(result.pageIndex+1)+');">></a>'+
	           				'<a href="JavaScript:turnPage('+result.totalPageCount+');">>></a>';
           				}else{//是最后一页时
           					page+='<a disabled="disabled">></a><a disabled="disabled">>></a>';
           				}
           			}else if(result.pageIndex>5 && result.totalPageCount>mPage){//前面有“...”，后面也有“...”
           				//alert(888);
           				page+='<a href="JavaScript:turnPage(1);"><<</a><a id="addPage" href="JavaScript:turnPage('+(result.pageIndex-1)+');"><</a>';
           				page+='<a class="hidden-xs" href="JavaScript:prevPages('+result.totalPageCount+');">...</a>';
           				for(var i=mPage-4;i<=mPage;i++){
       						if(result.pageIndex==i){
       							page+='<span id="pageIndex">'+result.pageIndex+'</span>';
       							continue;
       						}
       						page+='<a class="hidden-xs" href="JavaScript:turnPage('+i+');">'+i+'</a>';
       					}
       					page+='<a class="hidden-xs" href="JavaScript:nextPages('+result.totalPageCount+');">...</a>';
       					page+='<a href="JavaScript:turnPage('+(result.pageIndex+1)+');">></a>'+
	           			'<a href="JavaScript:turnPage('+result.totalPageCount+');">>></a>';
           			}else if(result.pageIndex>5 && result.totalPageCount<=mPage){//前面有“...”，后面没有“...”
           				//alert(999);
           				page+='<a href="JavaScript:turnPage(1);"><<</a><a id="addPage" href="JavaScript:turnPage('+(result.pageIndex-1)+');"><</a>';
           				page+='<a class="hidden-xs" href="JavaScript:prevPages('+result.totalPageCount+');">...</a>';
           				for(var i=mPage-4;i<=result.totalPageCount;i++){
       						if(result.pageIndex==i){
       							page+='<span id="pageIndex">'+result.pageIndex+'</span>';
       							continue;
       						}
       						page+='<a class="hidden-xs" href="JavaScript:turnPage('+i+');">'+i+'</a>';
       					}
       					if(result.pageIndex<result.totalPageCount){//不是最后一页时
       						page+='<a href="JavaScript:turnPage('+(result.pageIndex+1)+');">></a>'+
	           				'<a href="JavaScript:turnPage('+result.totalPageCount+');">>></a>';
       					}else{//是最后一页时
       						page+='<a disabled="disabled">></a><a disabled="disabled">>></a>';
       					}
       					
           			}
               	//将分页元素设置到页面上
               	var pager=document.getElementById("pager");
               	pager.innerHTML=page;
            }
        	});
     	}
        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);
            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/WbusiManage/js/weixinsdk.js"></script>

    <script type="text/javascript">
        if (isWeixin) {
            $.get('/home/getShare', function (data) {
                weixinService.init(data.title, data.desc, data.imgUrl, data.link);
            });
        }
    </script>
</body>
</html>
