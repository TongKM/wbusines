<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>代理注册统计</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <link href="/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/src/icomoon/style.css" rel="stylesheet" />
    <link href="/css/v1?v=uRKGEzIRg2Dr_9xFU-CwP1fTY_wxVN7k5M0YNhcKRnA1" rel="stylesheet"/>

    
</head>
<body class=agency-browse>
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {

                        if (document.body.className == 'agency-browse') {
                            window.location.href = '/App#' + window.location.pathname + window.location.search;
                            return;
                        }

                        if (document.body.className == 'admin-browse') {
                            window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                            return;
                        }
                    }
                } else {
                    if (document.body.clientWidth <= 1100 && document.body.className == 'admin-browse') {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" value="代理注册统计" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">代理注册统计 </a>
            </div>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
        <div class="navbar navbar-default navbar-fixed-bottom">
            <div class="container">
                <div class="btn-group">
                    <a href="/Shopping" class="btn btn-default ">
                        <i class="icon-home-o-new"></i>
                        首页
                    </a>
                    <a href="/Cart" class="btn btn-default shoping-cart ">
                        <i class="icon-shopping-cart-o-new" style="position:relative;">
                                <span class="shoping-cart-num label" style="margin-left:-2px !important;margin-top:-3px !important;position:absolute;">2</span>
                        </i>
                        购物车
                    </a>
                    <a href="/Agency/PersonalCenter" class="btn btn-default active">
                        <i class="icon-people-o-new" style="position:relative;">
                                <span class="personal-center-msg label" style="margin-left:-5px !important;margin-top:-3px !important;position:absolute;">1</span>
                        </i>
                        个人中心
                    </a>
                </div>
            </div>
        </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            

<div class="navmenu search-nav search-nav-2">
    <ul class="nav">
        <li class="active">
            <a data-title="代理注册统计" href="/Agency/StatisticsRegisteredPerson">
                注册统计
            </a>
        </li>
        <li class="tt">
            <a data-value="2" data-title="代理总况统计" href="/Agency/StatisticsAllRegistered">
                总况统计
            </a>
        </li>
    </ul>
</div>
<div class="bootcards-cards none-top-navbar">
    <div class="panel panel-default" style="margin-top:44px">
        <div class="panel-heading clearfix text-center">
            <a class="btn btn-default pull-left" style="width:65px" href="/Agency/StatisticsRegisteredPerson?beginTime=12%2F01%2F2017%2000%3A00%3A00">
                <i class="icon-left-new"></i>
            </a>
            <label style="padding:0px 15px;margin-top:5px">01月</label>
                <a class="btn btn-default pull-right disabled" style="margin-left:0px;width:65px;border-color:#fff;color:#ccc">
                    <i class="icon-right-new"></i>
                </a>
        </div>
    </div>
        <input type="hidden" id="hasData" value="false" />
        <div class="row text-center body-padding-normal">无相关的数据</div>
</div>

        </div>
    </div>
    <script src="/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/src/scripts/bootstrap.min.js"></script>
    <script src="/src/scripts/fastclick.min.js"></script>
    <script src="/src/scripts/jquery.validate.min.js"></script>
    <script src="/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/js/app?v=GSsrzOsdjrO8Vya2kMFd6t9wqw5HuYxu1clSLWf11s81"></script>

    
    <script src="/src/highcharts-4.2.8/highcharts.js"></script>
    <script type="text/javascript">
        $(function () {
            if ($("#hasData").val() == "false") return;

            //levelChart
            var levelData = eval($("#levelChartData").val());
            var levelCharData = [];
            $.each(levelData, function () {
                var b = new Object();
                b.name = this.labelName;
                b.y = this.amount;
                levelCharData.push(b);
            });
            $('#chartLevel').highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: '代理等级统计图',
                },
                tooltip: {
                    pointFormat: '注册占比: <b>{point.percentage:.2f}%</b>'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            useHTML: true,
                            format: '<big>{point.name}</big><br/> <small>{point.y}人</small>',
                            distance: -50,
                            style: {
                                fontWeight: 'bold',
                                color: 'white',
                                textShadow: '0px 1px 2px black'
                            }
                        },
                        startAngle: -180,
                        endAngle: 180,
                        center: ['50%', '50%']
                    }
                },
                series: [{
                    name: "level",
                    colorByPoint: true,
                    data: levelCharData,
                }]
            });

            //Province chart
            var provinceData = eval($("#provinceChartData").val());
            var provinceCharData = [];
            $.each(provinceData, function () {
                var b = new Object();
                b.name = this.labelName;
                b.y = this.amount;
                provinceCharData.push(b);
            });
            $('#chartProvince').highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: '省份统计图',
                },
                tooltip: {
                    pointFormat: '注册占比: {point.percentage:.2f}%'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            useHTML: true,
                            format: '<big>{point.name}</big><br/> <small>{point.y}人</small>',
                            distance: -50,
                            style: {
                                fontWeight: 'bold',
                                color: 'white',
                                textShadow: '0px 1px 2px black'
                            }
                        },
                        startAngle: -180,
                        endAngle: 180,
                        center: ['50%', '50%']
                    }
                },
                series: [{
                    name: "province",
                    colorByPoint: true,
                    data: provinceCharData,
                }]
            });
        });
    </script>

    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/js/weixinsdk?v=FuHO4EYdVtWj94At6HA29wrWQNt6iYo2nqlZNRPrmqE1"></script>

    <script type="text/javascript">
    if (isWeixin) {
        $.get('/home/getShare', function (data) {
            weixinService.init(data.title, data.desc, data.imgUrl, data.link);
        });
    }
    </script>
    <script src="/src/scripts/takeit.js"></script>
</body>
</html>

