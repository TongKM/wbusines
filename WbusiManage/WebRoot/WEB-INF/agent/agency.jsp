<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>代理</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>

    
</head>
<body class=agency-browse>
        <script type="text/javascript">
            /* (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {

                        if (document.body.className == 'agency-browse') {
                            window.location.href = '/App#' + window.location.pathname + window.location.search;
                            return;
                        }

                        if (document.body.className == 'admin-browse') {
                            window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                            return;
                        }
                    }
                } else {
                    if (document.body.clientWidth <= 1100 && document.body.className == 'admin-browse') {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })(); */
        </script>
    <input type="hidden" name="navmenu_title" value="直属代理" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">直属代理 </a>
            </div>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
        <jsp:include page="/WEB-INF/jsp/menu.jsp"></jsp:include>
    <div class="container bootcards-container" id="main">
        <div class="row">
            

<div class="bootcards-list agency-list-cards">
    <div class="panel panel-default">
        <div class="panel-body">
<form action="/Agency" class="search-form" method="post" name="agencyForm" role="form">                <div class="row">
                <div class="col-xs-9">
                        <div class="form-group">
                            <input class="form-control search-txt" id="searchWord" name="searchModel.Keyword" placeholder="搜索" type="text" value="" />
                            <i class="icon-searched-new"></i>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <button type="button" class="btn btn-default btn-menu pull-left btn-block" data-toggle="off-filter-canvas">
                            <i class="icon-screen-o-new"></i>
                            <span>筛选</span>
                        </button>
                    </div>
                </div>
</form>
</div>
	<c:forEach items="${userList}" var="user">
        <div class="order-item" id="${order.orderNo}">
            	<div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left" title="订单号" >下级代理：${user.name }</h3>
                    <span class="panel-title pull-right ">
                    电子邮箱：${user.email}
                    </span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <span class="panel-title pull-left ">
                    注册日期：<fmt:formatDate value="${user.regDate}"/>
                    </span>
                </div>
                <div class="panel-heading clearfix">
                	<!-- 计算每张订单的商品总数 -->
                	<%-- <c:forEach items="${order.shopList}" var="shop">
                        <a class="list-group-item order-good-item" href="javascript:;" data-id="GJ636528983267090490">
                            <img class="img-responsive img-rounded pull-left" src="${shop.pro.proPicPath}" title="${shop.pro.proName}" style="width:40px;height:40px">
                            <h4 class="list-group-item-heading">${shop.pro.proName}</h4>
                            <p class="list-group-item-text">&#165;${shop.pro.proPrice} x ${shop.count}</p>
                        	<c:set var="count" scope="page" value="${count+shop.count}"></c:set>
                        </a>
                    </c:forEach>  --%>
                    <span class="panel-title pull-left ">
                    代理电话：${user.phone}
                    </span><br>
                    <span class="panel-title pull-left ">
                    代理收货地址：${user.address}
                    </span>
                    <br>
                </div>
                
                <div class="panel-footer text-right">
                	<span class="pull-left" style="color:#f8910b"></span>
                </div>
            </div>
        </div>
        <hr style="color: black;">
        </c:forEach>
        
        <c:if test="${fn:length(userList)==0}">
       	<div class="row text-center">
        	<div class="row text-center body-padding-normal">没有任何信息</div>
    	</div>
       </c:if>
    

    </div>
</div>
<div class="navmenu off-filter-canvas offcanvas-right" style="margin-top:0">
<form action="/Agency" method="post" name="searchform" style="padding:15px 8px 0px 8px"><input id="searchModel_Keyword" name="searchModel.Keyword" type="hidden" value="" />        <div class="form-group">
            <label>代理等级</label>
            <select class="form-control" data-val="true" data-val-number="The field LevelId must be a number." id="searchModel_LevelId" name="searchModel.LevelId"><option value="">全部等级</option>
<option value="20">一级</option>
<option value="10">总经销</option>
<option value="6">大区代理</option>
<option value="9">四级代理</option>
<option value="11">玫瑰</option>
<option value="19">VIP顾客</option>
<option value="7">大区合伙人</option>
<option value="21">13</option>
<option value="14">大区合伙人1</option>
<option value="8">国代</option>
<option value="15">皇冠代理</option>
<option value="16">一级代理</option>
<option value="17">黄金代理</option>
<option value="18">白银代理</option>
<option value="2">省代</option>
<option value="3">市代</option>
<option value="13">县代</option>
<option value="12">营销员</option>
</select>
        </div>
        <div class="form-group">
            <label>起批限制</label>
            <select class="form-control" id="searchModel_IsRequiredBathRule" name="searchModel.IsRequiredBathRule"><option value="">请选择</option>
<option value="True">开启</option>
<option value="False">关闭</option>
</select>
        </div>
        <div class="form-group">
            <label>考核状态</label>
            <select class="form-control" id="searchModel_IsAutoUpdateLevel" name="searchModel.IsAutoUpdateLevel"><option value="">请选择</option>
<option value="True">开启</option>
<option value="False">关闭</option>
</select>
        </div>
        <div class="form-group">
            <label>代理状态</label>
            <select class="form-control" data-val="true" data-val-number="The field Status must be a number." id="searchModel_Status" name="searchModel.Status"><option selected="selected" value="-1">全部代理</option>
<option value="3">被冻结</option>
<option value="1">被取消资质</option>
</select>
        </div>
        <div class="form-group">
            <label>排序</label>
            <select class="form-control" id="searchModel_Sort" name="searchModel.Sort"><option selected="selected" value="TotalSale Desc">售出排序</option>
<option value="TotalBuy Desc">买入排序</option>
</select>
        </div>
        <input type="submit" value="确定" class="btn btn-desktop-primary btn-block" />
</form></div>

        </div>
    </div>
    <script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
    <script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/WbusiManage/js/app.js"></script>

    
    <script type="text/javascript">
        filterCanvas.init(0);
    </script>

    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/WbusiManage/js/weixinsdk.js"></script>

    <script type="text/javascript">
    if (isWeixin) {
        $.get('/home/getShare', function (data) {
            weixinService.init(data.title, data.desc, data.imgUrl, data.link);
        })
    }
    </script>
    <script src="/WbusiManage/src/scripts/takeit.js"></script>
</body>
</html>
