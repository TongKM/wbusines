<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>卖出订单</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>

    
    <link href="/WbusiManage/src/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

</head>
<body class=agency-browse>
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {

                        if (document.body.className == 'agency-browse') {
                            window.location.href = '/App#' + window.location.pathname + window.location.search;
                            return;
                        }

                        if (document.body.className == 'admin-browse') {
                            window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                            return;
                        }
                    }
                } else {
                    if (document.body.clientWidth <= 1100 && document.body.className == 'admin-browse') {
                        var css = document.createElement('link');
                        css.href = "/WbusiManage/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" value="卖出订单" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">卖出订单 </a>
            </div>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
        <jsp:include page="/WEB-INF/jsp/menu.jsp"></jsp:include>
    <div class="container bootcards-container" id="main">
        <div class="row">
            

<div class="off-search-canvas order-search-box" style="z-index: 10;">
	<form action="/Order/SaleOrder" class="search-form" method="post">
		<div class="row">
            <div class="col-xs-12">
                <div class="form-group input-group-search">
                    <input type="text" name="keywords" class="form-control search-txt" placeholder="订单号/买家姓名/买家手机">
                    <input type="hidden" name="tradeStatus" value="0">
                    <button type="submit" class="btn">
                        <i class="icon icon-search-o"></i>
                    </button>
                </div>
            </div>
	    </div>
	</form>
</div>
<div class="navmenu search-nav search-nav-5" style="top:44px;">
    <ul class="nav">
        <li role="presentation" class="active">
            <a data-value="" data-title="全部订单" href="/WbusiManage/sys/order/SaleOrder?oStatus=0">
                已卖出订单
            </a>
        </li>
        <!-- <li role="presentation" class="tt">
            <a data-value="4" data-title="待发货" href="/Order/SaleOrder?tradeStatus=4">
                待发货
            </a>
        </li>
        <li role="presentation" class="tt">
            <a data-value="2" data-title="待付款" href="/Order/SaleOrder?tradeStatus=2">
                待付款
            </a>
        </li> -->
        <!-- <li role="presentation" class="tt">
            <a data-value="3" data-title="待审核" href="/Order/SaleOrder?tradeStatus=3">
                待审核
            </a>
        </li>
        <li role="presentation" class="tt">
            <a data-value="6" data-title="成功" href="/Order/SaleOrder?tradeStatus=6">
                成功
            </a>
        </li> -->
    </ul>
</div>
<div class="bootcards-cards" id="bootcards-cards" style="padding-top:55px">
    <div id="myorderlist" style="display:none">
       <!-- 遍历卖出订单 -->
       <c:forEach items="${orderList}" var="order">
       	<div class="order-item" id="${order.orderNo}">
            	<div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left" title="订单号" style="font-size:13px;">订单：${order.orderNo }</h3>
                    <span class="panel-title pull-right visible-xs">
                    <fmt:formatDate value="${order.oTime}" />
                    </span>
                    <span class="panel-title pull-right hidden-xs">
                    <fmt:formatDate value="${order.oTime}"/></span>
                </div>
                <div class="list-group">
                	<!-- 计算每张订单的商品总数 -->
                	<c:set var="count" scope="page" value="0"></c:set>
                	<!-- 遍历订单里的商品 -->
                	<c:forEach items="${order.shopList}" var="shop">
                        <a class="list-group-item order-good-item" href="javascript:;" data-id="GJ636528983267090490">
                            <img class="img-responsive img-rounded pull-left" src="${shop.pro.proPicPath}" title="${shop.pro.proName}" style="width:40px;height:40px">
                            <h4 class="list-group-item-heading">${shop.pro.proName}</h4>
                            <p class="list-group-item-text">&#165;${shop.pro.proPrice} x ${shop.count}</p>
                        	<c:set var="count" scope="page" value="${count+shop.count}"></c:set>
                        </a>
                    </c:forEach>
                    <div class="list-group-item text-right">
                        <p class="list-group-item-text" style="display:inline">
                            <span class="pull-left">共 ${count} 件商品</span>
                            总计：<b class="total-price"><span>¥</span>${order.oAmount}</b>
                        </p>
                        <!-- <div class="small-txt">(运费：1209.00)</div> -->
                    </div>
                    <div class="list-group-item text-right">
                    <p class="list-group-item-text" >
                    	<span class="pull-left">买家：${order.user.name}&nbsp;&nbsp;&nbsp;| 电话：${order.user.phone}
						 </span><br>
						 <span class="pull-left">送货地址：${order.user.address}</span>
                    	</p>
                   </div><br>
                </div>
                <div class="panel-footer text-right">
                <!-- 显示订单状态 -->
                	<span class="pull-left" style="color:#f8910b">已付款</span>
                    <!-- <span class="pull-left" style="color:#f8910b">等待付款</span> -->
                </div>
            </div>
        </div>
       </c:forEach>
       <c:if test="${fn:length(orderList)==0}">
       	<div class="row text-center">
        <div>没有任何信息</div>
    	</div>
       </c:if>
    

        <script type="text/javascript">
            var scrollPosition = localStorage['scrollPosition'];

            document.getElementById('myorderlist').style.display = '';
            if (scrollPosition) {
                var cards = document.getElementById('bootcards-cards');
                cards.scrollTop = scrollPosition;
            }
        </script>
    </div>
</div>
<div class="modal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="btn-group pull-left">
                    <button class="btn btn-danger" data-dismiss="modal">
                        取消
                    </button>
                </div>
                <h4 class="modal-title">
                    填写理由
                </h4>
            </div>
<form action="/Order/OrderPayCheck" class="form-horizontal" data-ajax="true" data-ajax-begin="handlerBegin" data-ajax-failure="handlerFailure" data-ajax-method="POST" data-ajax-success="handlerSuccess" id="refuse_form" method="post">                <div class="modal-body">
                    <input type="hidden" name="orderId" />
                    <input type="hidden" name="isPass" value="false">
                    <div class="form-group">
                        <label class="col-xs-4 control-label">拒绝理由</label>
                        <div class="col-xs-8">
                            <textarea type="text" name="reason" class="form-control" placeholder="在此处填写订单审核未通过理由"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">
                        确定
                    </button>
                </div>
</form>
</div>
    </div>
</div>

        </div>
    </div>
    <script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
    <script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/WbusiManage/js/app.js"></script>

    
    <script src="/WbusiManage/src/moment/moment.js"></script>
    <script src="/WbusiManage/src/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(document).on("click", "a[name=download]", function () {
            var ids = $(this).attr("data-id");
            location.href = "/Order/ExportOrder?ids=" + ids + "";
        });
        function handlerSuccessClose(callback) {
            if (callback.success) {
                var id = callback.result;
                $('#' + id).find('.panel-footer').html('<span class="pull-left" style="color:#f8910b">交易关闭</span>');
                $('#alertModel').modal('hide');
            }
            else {
                var m = setTimeout(function () {
                    popMessage('错误', "关闭订单出错");
                    clearTimeout(m);
                }, 500);
            }
        }

        var goToDetail = false;
        $('.order-good-item').click(function () {
            var scrollPosition = $('.bootcards-cards').scrollTop();
            localStorage['scrollPosition'] = scrollPosition;

            goToDetail = true;
            if ($('body').hasClass('admin-browse') && $(window).width() >= 900) {
                var id = $(this).data('id');
                var url = '/Order/Detail?id=' + id + '&r=' + Math.random();
                remoteFullModal('/App/DialogBox?redirectUrl=' + encodeURIComponent(url));
                return false;
            };

            return true;
        });
        $('a[name="send"]').click(function () {
            if ($('body').hasClass('admin-browse') && $(window).width() >= 900) {
                var id = $(this).data('id');
                var url = '/Order/Send?id=' + id + '&r=' + Math.random();
                remoteFullModal('/App/DialogBox?redirectUrl=' + encodeURIComponent(url));
                return false;
            };
            return true;
        })

        function selectOrderIds(type) {
            var checkboxs = $('input[type="checkbox"][name="order_checkbox"]:checked');
            if (checkboxs.length == 0) {
                alert("请选择要打印的订单");
                return false;
            }
            var ids = '';
            checkboxs.each(function (index, e) {
                ids += $(e).val() + ',';
            });

            if (type == "orderBill")
                window.open('/Admin/Order/PrintBill?orderIds=' + ids);
            else window.open('/Admin/Order/PrintExpress?orderIds=' + ids);
        }

        function exportOrderIds(e) {
            var checkboxs = $('input[type="checkbox"][name="order_checkbox"]:checked');

            if (checkboxs.length == 0) {
                alert("请选择要导出的订单");
                return false;
            }
            var ids = '';

            checkboxs.each(function (index, e) {
                ids += $(e).val() + ',';
            });

            e.href = "/Order/ExportOrder?ids=" + ids;

            return true;
        }

        $('#order_checkbox_all').click(function () {
            if (this.checked) {
                $('input[type="checkbox"][name="order_checkbox"]').prop('checked', true);
            } else {
                $('input[type="checkbox"][name="order_checkbox"]').prop('checked', false);
            }
        })

        $(window).unload(function () {
            if (!goToDetail) {
                localStorage['scrollPosition'] = '';
            }
        });

        $('#startTime').datetimepicker({
            format: 'YYYY/MM/DD HH:mm:ss'
        });
        $('#endTime').datetimepicker({
            format: 'YYYY/MM/DD HH:mm:ss'
        });

        $('#filter_btn').click(function () {
            var has = $('.order-filter-box').hasClass('hidden');
            if (has) {
                $('.order-filter-box').removeClass('hidden');
                $('[name="closeFilter"]').val('false');
            } else {
                $('.order-filter-box').addClass('hidden');
                $('[name="closeFilter"]').val('true');
            }
        });

        function renderScanData() {
            var ids = '';
            $('a[name="scan"]').each(function () {
                ids += $(this).data('id') + ';';
            });
            if (ids != '') {
                $.get('/TraceSource/GetOrderCodeCount?ids=' + ids, function (result) {
                    if (result.success) {
                        result.result.forEach(function (e) {
                            var v = e.split(';');
                            $('a[name="scan"][data-id="' + v[0] + '"]').find('span').html('(' + v[1] + ')');
                        })
                    }
                });
            }
        }

        renderScanData();
    </script>
    <script type="text/javascript">
        filterCanvas.init(0, 'off-search-canvas');
    </script>
    <script type="text/javascript">
        $('a[name="btn-nopass"]').click(function () {
            var id = $(this).data('id');
            $('#refuse_form').find('[name="orderId"]').val(id);
            $('#refuse_form').find('[name="reason"]').val('');
            $('#editModal').modal()
        })
    </script>

    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/WbusiManage/js/weixinsdk.js"></script>

    <script type="text/javascript">
    if (isWeixin) {
        $.get('/home/getShare', function (data) {
            weixinService.init(data.title, data.desc, data.imgUrl, data.link);
        })
    }
    </script>
    <script src="/WbusiManage/src/scripts/takeit.js"></script>
</body>
</html>
