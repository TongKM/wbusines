<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>部落管家</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>

    
    <style type="text/css">
        .index-group { padding-right: 25px; }
            .index-group .list-group-item { padding-right: 0; }
            .index-group .list-group-item:before { display: none; }
            .list-group-item i{width:25px;}
    </style>

</head>
<body class=agency-browse>
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {

                        if (document.body.className == 'agency-browse') {
                            window.location.href = '/App#' + window.location.pathname + window.location.search;
                            return;
                        }

                        if (document.body.className == 'admin-browse') {
                            window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                            return;
                        }
                    }
                } else {
                    if (document.body.clientWidth <= 1100 && document.body.className == 'admin-browse') {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" value="收货地址" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">收货地址 </a>
            </div>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
    <jsp:include page="/WEB-INF/jsp/menu.jsp"></jsp:include>
    <div class="container bootcards-container" id="main">
        <div class="row">
            

<div class="bootcards-cards none-top-navbar">
    <div class="panel panel-default">
        <input type="hidden" value="" name="viewmodel" />
        <div class="panel-heading clearfix">
            <div class="btn-group pull-lef edit">
                <a class="btn btn-primary" href="javascript:void(0)">
                    <i class="icon-pencil" style="width:15px"></i>
                    <span>编辑</span>
                </a>
            </div>
            <a class="btn btn-primary pull-right" href="/Address/Add">
                <i class="icon-plus-circle" style="width:15px"></i>
                <span>添加</span>
            </a>
        </div>
        <div class="list-group ">
                    <div class="list-group-item" id="link_0">
                        0
                    </div>
                        <a class="list-group-item address-item" data-id="7aef13e2-8c04-e811-80ea-de9633d88086">
                            <i class="icon-choice-o-new pull-left" style="visibility:visible;font-size:1.5em"></i>
                            <i class="icon-cog pull-left hidden" style="font-size:1.5em"></i>
                            <h4 class="list-group-item-heading">wangtao <small>13580570258</small></h4>
                            <p class="list-group-item-text">
                                北京市 北京市  朝阳区
                            </p>
                        </a>
            <a class="list-group-item" href="/Address/Add">
                <i class="icon-plus-circle pull-left" style="font-size:1em"></i>
                <h4 class="list-group-item-heading">
                    添加一个地址
                </h4>
                <p class="list-group-item-text">
                </p>
            </a>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>

        </div>
    </div>
    <script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
    <script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/WbusiManage/js/app.js"></script>

    
    <script type="text/javascript">
        $(".edit").click(function () {
            var viewmodel = $("input[name=viewmodel]").val();
            //变成编辑状态
            if (viewmodel == "") {
                $(this).find("span").html("取消");
                $(this).find("i").addClass("hidden");
                $("input[name=viewmodel]").val("edit");

                $('.icon-choice-o-new').hide();
                $('.icon-cog').removeClass(" hidden");
            } else {

                $(this).find("span").html("编辑");
                $(this).find("i").removeClass("hidden");

                $("input[name=viewmodel]").val("");
                $('.icon-choice-o-new').show();
                $('.icon-cog').addClass("hidden");
            }
        });

        $(".address-item").click(function () {
            var viewmodel = $("input[name=viewmodel]").val();
            var id = $(this).attr("data-id");
            if (viewmodel == "") {
                $.post("/Address/Select", { id: id }, function (result) {
                    if (result) {
                        location.href = "/Cart/Index";
                    }
                });
            } else {
                location.href = "/Address/Edit/" + id + "?fromPage=orderAddress";
            }
        });

        function startSelect(e) {
            var name = $(e).data('name');
            window.location.hash = '#' + name;
            return false;
        }

    </script>

    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
   
</body>
</html>

