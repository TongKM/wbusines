<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>

    
</head>
<body class=agency-browse>
       
    <input type="hidden" name="navmenu_title" value="编辑个人信息" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">编辑个人信息 </a>
            </div>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
       <jsp:include page="/WEB-INF/jsp/menu.jsp"></jsp:include>
    <div class="container bootcards-container" id="main">
        <div class="row">
            

<div class="bootcards-cards">
    <div class="panel panel-default">
<form action="/WbusiManage/sys/wang/save" class="registerForm" id="registerSave" method="post" role="form"> 
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left"></h3>
                <a href="javascript:history.go(-1);" class="btn btn-danger pull-left">
                    取消
                </a>
                <a id="btn_Save" class="btn btn-success pull-right">
                    保存
                </a>
            </div>
            <input type="hidden" name="id" value="${sessionScope.user.id }"/>
            <div class="list-group">
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Email">电子邮箱</label>
                    <div class="list-group-item-heading">
                        <input readonly="readonly" class="form-control" data-val="true" data-val-email="电子邮箱 字段不是有效的电子邮件地址。" data-val-regex="请输入有效的邮箱" data-val-regex-pattern="^[a-zA-Z0-9_-]{1,}@[a-zA-Z0-9]{1,}.(com|net|org|edu|mil|cn|cc)$" data-val-remote="&#39;电子邮箱&#39; is invalid." data-val-remote-additionalfields="*.Email,*.JUserId" data-val-remote-url="/Account/CheckEmail" id="Email" name="Email" placeholder="电子邮箱" type="text" value="${sessionScope.user.email }" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Email" data-valmsg-replace="true"></span>
                    </div>
                </div>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="FirstName">姓名</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-required="姓名必填" id="FirstName" name="name" placeholder="姓名" type="text" value="${sessionScope.user.name }" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="FirstName" data-valmsg-replace="true"></span>
                    </div>
                </div>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Tel">手机号码</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-regex="请填写正确手机号码" data-val-regex-pattern="^(1)[0-9]{10}$" data-val-remote="&#39;手机号码&#39; is invalid." data-val-remote-additionalfields="*.Tel,*.JUserId" id="Tel" name="phone" placeholder="联系电话" type="text" value="${sessionScope.user.phone }" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Tel" data-valmsg-replace="true"></span>
                    </div>
                </div>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Address">默认收货地址</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" id="Address" name="address" placeholder="详细地址" type="text" value="${sessionScope.user.address }" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Address" data-valmsg-replace="true"></span>
                    </div>
                </div>
            </div>
</form>    </div>
</div>

        </div>
    </div>
    <script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
    <script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/WbusiManage/js/app.js"></script>

    
    <script type="text/javascript">

        $("#btn_Save").click(function () {
            var flag = true;
            flag = $("#registerSave").valid();
            var o = $("#registerSave").serialize();
            $("#registerSave")[0].submit();
        });

    </script>
</body>
</html>
