<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>我的业绩</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <link href="/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/src/icomoon/style.css" rel="stylesheet" />
    <link href="/css/v1?v=uRKGEzIRg2Dr_9xFU-CwP1fTY_wxVN7k5M0YNhcKRnA1" rel="stylesheet"/>

    
    <style type="text/css">
        .chart { height: 110px; }
            .chart .highcharts-axis { display: none; }
        .performance-cards .panel-title { color: #3a3a3a; }
    </style>

</head>
<body class=agency-browse>
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {

                        if (document.body.className == 'agency-browse') {
                            window.location.href = '/App#' + window.location.pathname + window.location.search;
                            return;
                        }

                        if (document.body.className == 'admin-browse') {
                            window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                            return;
                        }
                    }
                } else {
                    if (document.body.clientWidth <= 1100 && document.body.className == 'admin-browse') {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" value="个人中心" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">个人中心 </a>
            </div>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
        <div class="navbar navbar-default navbar-fixed-bottom">
            <div class="container">
                <div class="btn-group">
                    <a href="/Shopping" class="btn btn-default ">
                        <i class="icon-home-o-new"></i>
                        首页
                    </a>
                    <a href="/Cart" class="btn btn-default shoping-cart ">
                        <i class="icon-shopping-cart-o-new" style="position:relative;">
                                <span class="shoping-cart-num label" style="margin-left:-2px !important;margin-top:-3px !important;position:absolute;">2</span>
                        </i>
                        购物车
                    </a>
                    <a href="/Agency/PersonalCenter" class="btn btn-default active">
                        <i class="icon-people-o-new" style="position:relative;">
                                <span class="personal-center-msg label" style="margin-left:-5px !important;margin-top:-3px !important;position:absolute;">1</span>
                        </i>
                        个人中心
                    </a>
                </div>
            </div>
        </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            

<div class="bootcards-cards none-top-navbar performance-cards">
    <input type="hidden" name="startDate" value="2017-09-01" />
    <input type="hidden" name="endDate" value="2018-01-01" />

    <p style="padding:10px 15px 0 15px">近5个月统计</p>
    <div class="panel panel-default panel-base">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">我的订购</h3>
        </div>
        <div class="panel-body" style="padding:0;margin:0 -15px;">
            <div id="purchaseChart" class="chart">
            </div>
        </div>
    </div>
    <div class="panel panel-default panel-base">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">我的卖出</h3>
        </div>
        <div class="panel-body" style="padding:0;margin:0 -15px;">
            <div id="soldChart" class="chart">
            </div>
        </div>
    </div>
    <div class="panel panel-default panel-base">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">我的利润</h3>
        </div>
        <div class="panel-body" style="padding:0;margin:0 -15px;">
            <div id="profitChart" class="chart">

            </div>
        </div>
    </div>
</div>

        </div>
    </div>
    <script src="/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/src/scripts/bootstrap.min.js"></script>
    <script src="/src/scripts/fastclick.min.js"></script>
    <script src="/src/scripts/jquery.validate.min.js"></script>
    <script src="/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/js/app?v=GSsrzOsdjrO8Vya2kMFd6t9wqw5HuYxu1clSLWf11s81"></script>

    
    <script src="/src/highcharts-4.2.8/highcharts.js"></script>
    <script type="text/javascript">
        var startDate = $('[name="startDate"]').val();
        var endDate = $('[name="endDate"]').val();

        function getPurchaseData() {
            $.get('/Agency/GetMonthPurchaseAmount?r=' + Math.random() + '&startDate=' + startDate + '&endDate=' + endDate, function (result) {
                if (result) {
                    getProfitData(result);
                    initChart(result, 'purchaseChart', '#ffce85', '#ffab2e');
                }
            });
        }
        function getProfitData(purchaseData) {
            $.get('/Agency/GetMonthSoldAmount?r=' + Math.random() + '&startDate=' + startDate + '&endDate=' + endDate, function (result) {
                if (result) {
                    var data = result;
                    initChart(data, 'soldChart', '#F9A0A4', '#FA646A');

                    var chartData = { data: [], categories: [], minData: 9999999 };
                    data.forEach(function (e, index) {
                        var amount = e.amount - purchaseData[index].amount;
                        if (chartData.minData > amount)
                            chartData.minData = amount;
                        chartData.data.push({ y: amount });
                        chartData.categories.push(new Date(e.time).format('M月'));
                    })

                    renderChart('profitChart', chartData, '#9ac9ff', '#3891f8');
                }
            })
        }
        function initChart(data, filter, color1, color2) {
            var chartData = { data: [], categories: [], minData: 9999999 };
            data.forEach(function (e) {
                if (chartData.minData > e.amount)
                    chartData.minData = e.amount;
                chartData.data.push({ y: e.amount });
                chartData.categories.push(new Date(e.time).format('M月'));
            })

            renderChart(filter, chartData, color1, color2);
        }
        function renderChart(filter, chartData, color1, color2) {
            var length = chartData.data.length;
            var obj = chartData.data[length - 1];
            obj.marker = {
                lineWidth: 4,
            };
            obj.dataLabels = {
                style: {
                    color: color1,
                    fontSize: '14px',
                    fontWeight: '400',
                },
            }

            $('#' + filter).highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'line'
                },
                title: null,
                xAxis: {
                    categories: chartData.categories,
                    tickLength: 0,
                    tickWidth: 0,
                    line: false
                },
                yAxis: {
                    title: null,
                    gridLineWidth: 0,
                    tickLength: 0,
                    labels: { enabled: false },
                    min: chartData.minData,
                    startOnTick: false,

                },
                plotOptions: {
                    line: {
                        color: color1,
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                return formatMoney(this.point.y, 2);
                            },
                            style: {
                                fontWeight: '400',
                                color: '#606060',
                                textShadow: null,
                            },
                        },
                        marker: {
                            fillColor: color2,
                            lineColor: color2,
                        },
                        enableMouseTracking: false,
                    }
                },
                series: [{
                    data: chartData.data,
                    showInLegend: false,
                }]
            });
        }

        getPurchaseData();
    </script>

    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/js/weixinsdk?v=FuHO4EYdVtWj94At6HA29wrWQNt6iYo2nqlZNRPrmqE1"></script>

    <script type="text/javascript">
    if (isWeixin) {
        $.get('/home/getShare', function (data) {
            weixinService.init(data.title, data.desc, data.imgUrl, data.link);
        })
    }
    </script>
    <script src="/src/scripts/takeit.js"></script>
</body>
</html>
