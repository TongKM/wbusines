<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>订单支付</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet"/>
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>
	<style type="text/css">
		* { touch-action: none; }
	</style>
    
</head>
<body class=agency-browse>
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {

                        if (document.body.className == 'agency-browse') {
                            window.location.href = '/App#' + window.location.pathname + window.location.search;
                            return;
                        }

                        if (document.body.className == 'admin-browse') {
                            window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                            return;
                        }
                    }
                } else {
                    if (document.body.clientWidth <= 1100 && document.body.className == 'admin-browse') {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" value="个人中心" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">个人中心 </a>
            </div>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
    <jsp:include page="/WEB-INF/jsp/menu.jsp"></jsp:include>
    <div class="container bootcards-container" id="main">
        <div class="row">

<div class="bootcards-cards order-submit-cards none-top-navbar">
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">支付信息</h3>
        </div>
        <div class="list-group pay-order-info">
            <div class="list-group-item">
                <label>卖家：</label><span>${seller.name}</span>
            </div>
            <div class="list-group-item">
                <label>金额：</label><span class="pay-sum">${oAmount}</span>元
            </div>
        </div>
        <input type="hidden" name="order_pay_amount" value="382.00" />
    </div>
<form action="" class="form-horizontal" method="get" 
enctype="multipart/form-data" onsubmit="return check();">       
 <div class="panel panel-default " id="payType_container">
            <input type="hidden" id="oid" name="orderId" value="${orderId}">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left">支付方式</h3>
            </div>
            <input data-val="true" data-val-required="OrderId 字段是必需的。" id="OrderId" name="OrderId" type="hidden" value="GJ636529024927821624" />
            <input data-val="true" data-val-number="The field OrderPayment must be a number." data-val-required="OrderPayment 字段是必需的。" id="OrderPayment" name="OrderPayment" type="hidden" value="382.00" />
            <div class="pay-list">
                <div class="pay-row">
                    <label class="label-checkbox" name="pay-box">
                        <input id="demo1-alipay" name="demo1" type="radio" value="option1" checked="checked"/>
                        <i class="icon icon-form-checkbox"></i>
                        <span class="inner-text">支付宝支付</span>
                    </label>
                <div class="pay-row">
                    <label class="label-checkbox" name="pay-box">
                        <input id="demo1-weixin" name="demo1" type="radio" value="option2"/>
                        <i class="icon icon-form-checkbox"></i>
                        <span class="inner-text">微信支付</span>
                    </label>
                </div>
            </div>
        </div>
        <div style="display: none;">
        	<p><input id="inputprice" type="text" name="price" class="form-control" value="0.01"/></p>
        </div>
        <div class="panel panel-none" style="margin:10px 10px 25px 10px;">
            <input type="button" id="demoBtn1" value="确定" class="btn btn-desktop-primary btn-block" />
        </div>
       
</form></div>

        </div>
    </div>
     <form style="display: none;" id='formpay' name='formpay' method='post' action='https://pay.paysapi.com'>
        <input name='goodsname' id='goodsname' type='text' value='' />
        <input name='istype' id='istype' type='text' value='' />
        <input name='key' id='key' type='text' value=''/>
        <input name='notify_url' id='notify_url' type='text' value=''/>
        <input name='orderid' id='orderid' type='text' value=''/>
        <input name='orderuid' id='orderuid' type='text' value=''/>
        <input name='price' id='price' type='text' value=''/>
        <input name='return_url' id='return_url' type='text' value=''/>
        <input name='uid' id='uid' type='text' value=''/>
        <input type='submit' id='submitdemo1'/>
    </form>
    
    <script type="text/javascript" src="https://cdn.staticfile.org/jquery/1.11.1/jquery.min.js"></script>

	

<script type="text/javascript">
$(document).ready(function(){

    function getistype(){
        return ($("#demo1-alipay").is(':checked') ? "1" : "2" ); 
    }

    $("#demoBtn1").click(function(){
    	
        $.post(
            "/WbusiManage/pays/pay",
            {
                price : $("#inputprice").val(), 
                istype : getistype(),
                orderId: $("#oid").val()
            },
            function(data){ 
                if (true){
                    $("#goodsname").val(data.data.goodsname);
                    $("#istype").val(data.data.istype);
                    $('#key').val(data.data.key);
                    $('#notify_url').val(data.data.notify_url);
                    $('#orderid').val(data.data.orderid);
                    $('#orderuid').val(data.data.orderuid);
                    $('#price').val(data.data.price);
                    $('#return_url').val(data.data.return_url);
                    $('#uid').val(data.data.uid);
                    $('#submitdemo1').click();
                } else {
                    alert(data.msg);
                }
            }, "json"
        );
    });
});
</script>    
    <script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
    <script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/WbusiManage/js/app.js"></script>
    <script src="/WbusiManage/js/help.js"></script>

    <script type="text/javascript">
    	//验证是否上传支付凭证
    	function check(){
    		var preview = document.getElementById('preview');
    		if(preview.src==""){
    			alert('请上传支付凭证截图！');
    			return false;
    		}else{
    			return true;
    		}
    	}
    	//上传图片后预览图片
		function viewImage(file){
            var preview = document.getElementById('preview');
            if(file.files && file.files[0]){
                //火狐下
                preview.style.display = "block";
                preview.style.width = "150px";
                preview.style.height = "180px";
                preview.src = window.URL.createObjectURL(file.files[0]);
            }else{
                //ie下，使用滤镜
                file.select();
                var imgSrc = document.selection.createRange().text;
                var localImagId = document.getElementById("localImag"); 
                //必须设置初始大小 
                localImagId.style.width = "250px"; 
                localImagId.style.height = "200px"; 
                try{ 
                localImagId.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale)";
                locem("DXImageTransform.Microsoft.AlphaImageLoader").src = imgSrc; 
                }catch(e){ 
                alert("您上传的图片格式不正确，请重新选择!"); 
                return false; 
                } 
                preview.style.display = 'none'; 
                document.selection.empty(); 
                } 
                return true; 
        	}
        
        $('input[name="PayType"]').change(function () {
            $(".pay-content").hide();
            var v = $('input[name="PayType"]:checked')[0].value;
            if (v == "1") {
                $(".pay-zhifubao-content").show();
            } else {
                $(".pay-weixin-content").show();
            }
        });
        $('input[name="DiscountDeposit"]').change(function () {
            var total = $('input[name="order_pay_amount"]').val();
            var deposit = this.value;

            if (parseFloat(total) <= parseFloat(deposit)) {
                if (this.checked) {
                    $('#credential_container').addClass('hidden');
                    $('#payType_container').addClass('hidden');
                    $('.need-pay').removeClass('hidden');
                } else {
                    $('#credential_container').removeClass('hidden');
                    $('#payType_container').removeClass('hidden');
                    $('.need-pay').addClass('hidden');
                }
            }
        });
    </script>

    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/WbusiManage/js/weixinsdk.js"></script>

    <script type="text/javascript">
    if (isWeixin) {
        $.get('/home/getShare', function (data) {
            weixinService.init(data.title, data.desc, data.imgUrl, data.link);
        })
    }
    </script>
    <script src="/WbusiManage/src/scripts/takeit.js"></script>
    <script src="/WbusiManage/js/jquery-1.8.3.js"></script>
</body>
</html>

