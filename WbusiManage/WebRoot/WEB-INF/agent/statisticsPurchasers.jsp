<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style"
	content="black-translucent">
<meta name="format-detection" content="telephone=no" />
<title>销量统计</title>
<meta name="keyword" content="部落">
<meta name="description" content="部落管家">
<link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css"
	rel="stylesheet" />
<link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
<link href="/WbusiManage/css/v1.css" rel="stylesheet" />
<script src="/WbusiManage/js/jquery-1.4.2.min.js" type="text/javascript"></script>


<script src="/WbusiManage/js/highcharts.js" type="text/javascript"></script>
<script src="/WbusiManage/js/exporting.js" type="text/javascript"></script>

</head>
<body class=agency-browse>
<jsp:include page="/WEB-INF/jsp/menu.jsp"></jsp:include>
	<form id="form1">
		<div>
			<script type="text/javascript">
				/*获取json数据开始*/
				//定义变量
				$(document).ready(
								function() {
									sb();
									function sb() {
										var jsonXData = [];
										var jsonyD1 = [];
										var jsonyD2 = [];
										//获取数据
										$.ajax({
													url : '/WbusiManage/sys/agent/chart',
													cache : false,
													async : false,
													success : function(data) {
														// alert(data.tem.buyer.length);
														//  var json = eval("(" + data + ")");
														//if (json.Rows.length > 0) {

														for ( var i = 0, y = 4; i < 5; i++) {
															/* var date1 = new Date();
															date1.setMonth(date1.getMonth()- y);
															var year1 = date1.getFullYear();
															var month1 = date1.getMonth() + 1; 
															month1 = (month1 < 10 ? "0"+ month1: month1);
															var sDate = month1.toString(); */
															var rows = data.tem.buyer[i];
															var rowsbb = data.tem.seller[i];
															if(rows!=null&&rows.ot!=null){
																var Time = rows.ot;
																}
															else
																break;
															var SumCount;
															if (rows != null)
																SumCount = rows.oa;//买入
															else
																SumCount = 0;
															var IpCount;
															if (rowsbb != null&&rowsbb.oa!=null)
																IpCount = rowsbb.oa;//卖出
															else
																IpCount = 0;
															jsonXData
																	.push(Time); //赋值
															jsonyD1
																	.push(SumCount);
															 jsonyD2.push(IpCount);
															y -= 1;
														} //for
														var chart;
														chart = new Highcharts.Chart(
																{
																	chart : {
																		renderTo : 'containerliuliang',
																		//放置图表的容器
																		plotBackgroundColor : null,
																		plotBorderWidth : null,
																		defaultSeriesType : 'area' //图表类型line, spline, area, areaspline, column, bar, pie , scatter 
																	},
																	title : {
																		text : '最近5个月买入订单统计(元)'
																	},
																	xAxis : { //X轴数据
																		categories : jsonXData,
																		lineWidth : 2,
																		labels : {
																			align : 'center',
																			style : {
																				font : 'normal 13px 宋体'
																			}
																		}
																	},
																	yAxis : { //Y轴显示文字
																		lineWidth : 2,
																		title : {
																			text : '金额'
																		}
																	},
																	tooltip : {
																		formatter : function() {
																			return '<b>'
																					+ this.x
																					+ '</b><br/>'
																					+ this.series.name
																					+ ': '
																					+ Highcharts
																							.numberFormat(
																									this.y,
																									0);
																		}
																	},
																	plotOptions : {
																		column : {
																			dataLabels : {
																				enabled : true
																			},
																			enableMouseTracking : true
																		//是否显示title
																		}
																	},
																	series : [
																			{
																				name : '买入订单',
																				data : jsonyD1
																			}/* ,
																			{
																				name : '卖出订单',
																				data : jsonyD2
																			} */ ]
																});
														$("tspan:last").hide(); //把广告删除掉
														//  } //if
													}
												});
									}
								});
			</script>
			<div id="containerliuliang" style="height: 340px;"></div>
		</div>
	</form>
</body>
</html>