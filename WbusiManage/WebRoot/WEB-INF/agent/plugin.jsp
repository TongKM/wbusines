<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>部落管家</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <link href="/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/src/icomoon/style.css" rel="stylesheet" />
    <link href="/css/v1?v=uRKGEzIRg2Dr_9xFU-CwP1fTY_wxVN7k5M0YNhcKRnA1" rel="stylesheet"/>

    
    <style type="text/css">
        .list-group .list-group-item { padding-right: 50px; }
        .btn-primary { border-color: #007aff; padding: 2px 6px; font-size: 14px; border-radius: 2px; }
        .plug-add { position: absolute; right: -1px; top: 50%; margin-top: -7px; }
            .plug-add:hover, .plug-add:focus, .plug-add:focus:active { border-color: #007aff; margin-top: -7px; }
    </style>

</head>
<body class=agency-browse>
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {

                        if (document.body.className == 'agency-browse') {
                            window.location.href = '/App#' + window.location.pathname + window.location.search;
                            return;
                        }

                        if (document.body.className == 'admin-browse') {
                            window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                            return;
                        }
                    }
                } else {
                    if (document.body.clientWidth <= 1100 && document.body.className == 'admin-browse') {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#"> </a>
            </div>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
        <div class="navbar navbar-default navbar-fixed-bottom">
            <div class="container">
                <div class="btn-group">
                    <a href="/Shopping" class="btn btn-default ">
                        <i class="icon-home-o-new"></i>
                        首页
                    </a>
                    <a href="/Cart" class="btn btn-default shoping-cart ">
                        <i class="icon-shopping-cart-o-new" style="position:relative;">
                                <span class="shoping-cart-num label" style="margin-left:-2px !important;margin-top:-3px !important;position:absolute;">2</span>
                        </i>
                        购物车
                    </a>
                    <a href="/Agency/PersonalCenter" class="btn btn-default active">
                        <i class="icon-people-o-new" style="position:relative;">
                                <span class="personal-center-msg label" style="margin-left:-5px !important;margin-top:-3px !important;position:absolute;">1</span>
                        </i>
                        个人中心
                    </a>
                </div>
            </div>
        </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            

<div class="bootcards-cards none-top-navbar">
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">市场</h3>
        </div>
        <div class="list-group">
            <div class="list-group-item">
                <img class="pull-left" src="/src/images/plug/sao-80.png" />
                <div>
                    <h4 class="list-group-item-heading">扫一扫</h4>
                    <p class="list-group-item-text">
                        自动识别单号条形码，帮助快速发货
                    </p>
                </div>
                <img class="plug-add" src="/src/images/site/success-80.png" style="width:20px;height:20px" />
            </div>
            <div class="list-group-item">
                <img class="pull-left" src="/src/images/plug/order-one-more-80.png" />
                <div>
                    <h4 class="list-group-item-heading">再来一单</h4>
                    <p class="list-group-item-text">
                        曾经下过的单可以快速再来一单，免去再次放入购物车烦恼
                    </p>
                </div>
                <img class="plug-add" src="/src/images/site/success-80.png" style="width:20px;height:20px" />
            </div>
                            <div class="list-group-item">
                    <i class="pull-left icon icon-order-export icon-2x-big" style="height: 40px;width:33px;margin-right: 15px;color: rgb(198,198,219);opacity: 1;font-size: 2.4em;margin: 10px 15px 0 7px;"></i>
                    <h4 class="list-group-item-heading">系统下单</h4>
                    <p class="list-group-item-text">
                        预存款余额足够时，下级来订单了，系统自动帮您向上级代理提交订单，实现在线挂机功能
                    </p>
<a class="btn btn-primary plug-add" data-ajax="true" data-ajax-begin="handlerBegin" data-ajax-failure="handlerFailure" data-ajax-method="POST" data-ajax-success="handlerSuccess" href="/Plugin/Add?plugName=%E7%B3%BB%E7%BB%9F%E4%B8%8B%E5%8D%95" style="right:5px">添加</a>                </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <h4 class="list-group-item-heading text-center" style="color:red;font-size:14px">更多内容，敬请期待...</h4>
        </div>
    </div>
</div>

        </div>
    </div>
    <script src="/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/src/scripts/bootstrap.min.js"></script>
    <script src="/src/scripts/fastclick.min.js"></script>
    <script src="/src/scripts/jquery.validate.min.js"></script>
    <script src="/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/js/app?v=GSsrzOsdjrO8Vya2kMFd6t9wqw5HuYxu1clSLWf11s81"></script>

    
    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/js/weixinsdk?v=FuHO4EYdVtWj94At6HA29wrWQNt6iYo2nqlZNRPrmqE1"></script>

    <script type="text/javascript">
    if (isWeixin) {
        $.get('/home/getShare', function (data) {
            weixinService.init(data.title, data.desc, data.imgUrl, data.link);
        })
    }
    </script>
    <script src="/src/scripts/takeit.js"></script>
</body>
</html>
