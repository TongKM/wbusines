<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>

    
</head>
<body class=agency-browse>
    <input type="hidden" name="navmenu_title" value="代理信息" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">代理信息 </a>
            </div>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
        <jsp:include page="/WEB-INF/jsp/menu.jsp"></jsp:include>
    <div class="container bootcards-container" id="main">
        <div class="row">
            

<input data-val="true" data-val-required="JUserId 字段是必需的。" id="JUserId" name="JUserId" type="hidden" value="a79df12c-52f9-411c-bc4d-fa647e68180f" />
<div class="bootcards-cards">
    <div class="panel panel-default margin">
        <div class="panel-heading clearfix">
            <a class="btn btn-primary pull-right" onclick="parent.location.href='/WbusiManage/sys/wang/person?nextpath=edit'">
                <i class="icon-pencil"></i>
                <span>修改</span>
            </a>
        </div>
        <div class="list-group">
            <div class="list-group-item">
                <img src="/WbusiManage/src/images/site/user-logo.jpg" class="img-rounded pull-left">
                <label>姓名</label>
                <h4 class="list-group-item-heading">${user.name }</h4>
            </div>
        </div>
        <div class="list-group">
            <div class="list-group-item">
                <label>联系电话</label>
                <h4 class="list-group-item-heading">${user.phone }</h4>
            </div>
            <div class="list-group-item">
                <label>邮箱</label>
                <h4 class="list-group-item-heading">${user.email }</h4>
            </div>
            <div class="list-group-item">
                <label>默认收货地址</label>
                <h4 class="list-group-item-heading">${user.address }</h4>
            </div>
            <div class="list-group-item">
                <label>注册时间</label>
                <h4 class="list-group-item-heading"><fmt:formatDate value="${user.regDate }" pattern="yyyy-MM-dd"></fmt:formatDate></h4>
            </div>
        </div>
    </div>
</div>
        </div>
    </div>
    <script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
    <script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/WbusiManage/js/app.js"></script>

    
</body>
</html>

