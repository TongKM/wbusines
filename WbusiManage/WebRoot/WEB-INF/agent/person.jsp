<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>个人中心</title>
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>


    <style type="text/css">
        .list-group-item i.icon-signout { font-size: 1.6em; display: block; width: 1.3em; }
    </style>

</head>
<body class=agency-browse>

<input type="hidden" name="navmenu_title" value="个人中心" />
<!-- fixed top navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand no-break-out" title="Customers" href="#">个人中心 </a>
        </div>
        <!-- menu button to show/ hide the off canvas slider -->

    </div>
</div>
<jsp:include page="/WEB-INF/jsp/menu.jsp"></jsp:include>
<div class="container bootcards-container" id="main">
    <div class="row">

        <div class="bootcards-cards panel-person-cards none-top-navbar">
            <div class="panel panel-person-top">
                <div class="panel-body" id="imgContent">
                    <a class="panel-person-top-content" href="#" onclick="parent.location.href='/WbusiManage/sys/wang/person?nextpath=detail'">
                        <div class="logo">
                            <img src="/WbusiManage/src/images/site/user-logo.jpg" alt="头像" class="img-circle img-responsive">
                        </div>
                        <ul class="user-info">
                            <li class="user-name" style="font-size:medium;">${sessionScope.user.name}</li>
                            <li>${sessionScope.user.email}</li>
                        </ul>
                    </a>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="list-group">
                    <a class="list-group-item" href="javacript:;" style="padding-top:18px;padding-bottom:18px">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="icon icon-my-recharge pull-left" style="font-size:1.4em;color:rgb(255,171,46);opacity:1"></i>
                                <div class="pull-left">
                                    <h4 class="list-group-item-heading" style="margin-top:3px;">我的资产</h4>
                                </div>
                                <span class="pull-right deposit-amount">
                                &#165; ${sessionScope.user.property }
                            </span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="panel panel-default panel-person-middle">
                <div class="panel-body">
                    <div class="row row1">
                        <a class="col-xs-3 text-center" onclick="parent.location.href='/WbusiManage/sys/wang/person?nextpath=BuyOrder'">
                            <div class="icon icon-audit-order-new" style="color:rgb(65,189,84)">
                            </div>
                            <div>买入订单</div>
                        </a>
                        <!-- /Agency/VerificationDetails -->
                        <a class="col-xs-3 text-center">
                            <div class="icon icon-agent-level" style="color:rgb(65,189,173)"></div>
                            <div>我的考核</div>
                        </a>
                        <!-- <a class="col-xs-3 text-center" href="/WbusiManage/sys/order/SaleOrder">
                            <div class="icon icon-sell-order" style="color:rgb(147,145,232)">
                            </div>
                            <div>卖出订单</div>
                        </a>
                        <a class="col-xs-3 text-center" href="#" onclick="parent.location.href='/WbusiManage/sys/wang/person?nextpath=Purchasers'">
                        </a> -->
                        <a class="col-xs-3 text-center" onclick="parent.location.href='/WbusiManage/sys/wang/person?nextpath=Purchasers'">
		                    <div class="icon icon-volume" style="color:rgb(255,171,45)"></div>
		                    <div>买入统计</div>
		                </a>
		                <a class="col-xs-3 text-center" onclick="parent.location.href='/WbusiManage/sys/wang/person?nextpath=edit';">
		                    <div class="icon icon-agent-management" style="color:rgb(147,145,232)"></div>
		                    <div>修改个人信息</div>
		                </a>
                    </div>
                    <!-- <div class="row row2">
                        <a class="col-xs-3 text-center" onclick="javascript:parent.location.href='/WbusiManage/sys/agent/find'" href="javascript:;">
                            <div class="icon icon-agent-management" style="color:rgb(1,161,234)"></div>
                            <div>代理管理</div>
                        </a>
                    </div> -->
                </div>
            </div>
            <!-- <div class="panel panel-default panel-person-bottom panel-base">
                <div class="list-group">
                    <a class="list-group-item" href="#">
                        <i class="icon icon-address-management  pull-left"></i>
                        <h4 class="list-group-item-heading">地址管理</h4>
                    </a>
                    <a class="list-group-item" href="#">
                        <i class="icon icon-payment pull-left"></i>
                        <h4 class="list-group-item-heading">支付管理</h4>
                    </a>
                    <a class="list-group-item" href="#">
                        <i class="icon icon-market-plug-in pull-left"></i>
                        <h4 class="list-group-item-heading">市场插件</h4>
                    </a>
                </div>
            </div> -->
            <div class="panel panel-default panel-person-bottom panel-base">
                <div class="list-group">
                    <a class="list-group-item" href="#" onclick="parent.location.href='/WbusiManage/sys/wang/person?nextpath=About'">
                        <i class="icon icon-about-new pull-left"></i>
                        <h4 class="list-group-item-heading">关于</h4>
                    </a>
                </div>
            </div>
            <div class="panel panel-default panel-person-bottom hidden-xs hidden-sm">
                <div class="list-group">
                    <a class="list-group-item" href="/Account/LogOff" onclick="return confirm('您确定要退出吗？')">
                        <i class="icon icon-retreat-safely pull-left"></i>
                        <h4 class="list-group-item-heading">安全退出</h4>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>
<input type="hidden" id="flag" value="${flag}"/>
<script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
<script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
<script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
<script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
<script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
<script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
<script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
<script src="/WbusiManage/js/app.js"></script>
<script type="text/javascript">
	$(function(){//用于判断是否显示小红点
		if($("#flag").val() == 0) {
			$(".shoping-cart-num").hide();
		}else{
			$(".shoping-cart-num").show();
		} 
	});
</script>

</body>
</html>
