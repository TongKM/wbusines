<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>部落管家</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>

    
</head>
<body class=agency-browse>
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {

                        if (document.body.className == 'agency-browse') {
                            window.location.href = '/App#' + window.location.pathname + window.location.search;
                            return;
                        }

                        if (document.body.className == 'admin-browse') {
                            window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                            return;
                        }
                    }
                } else {
                    if (document.body.clientWidth <= 1100 && document.body.className == 'admin-browse') {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" value="新增收货地址" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">新增收货地址 </a>
            </div>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
    <jsp:include page="/WEB-INF/jsp/menu.jsp"></jsp:include>
    <div class="container bootcards-container" id="main">
        <div class="row">
            

<div class="bootcards-cards">
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left"></h3>
            <a id="btn_Save" class="btn btn-success pull-right">
                保存
            </a>
        </div>
        <div class="list-group">
<form action="/Address/Add" class="form-horizontal" id="addressForm" method="post" role="form">                        <input id="requestUrl" type="hidden" value="/Address/List" />
                <div class="list-group-item" style="border-top:0">
                    <label class="list-group-item-text" for="Receiver">收货人</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" id="Receiver" name="Receiver" type="text" value="" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Receiver" data-valmsg-replace="true"></span>
                    </div>
                </div>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="Tel">联系电话</label>
                    <div class="list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-required="联系电话 必填" id="Tel" name="Tel" type="text" value="" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Tel" data-valmsg-replace="true"></span>
                    </div>
                </div>
                <a class="list-group-item" href="/Address/SelectArea?fromPage=add">
                    <label class="list-group-item-text">
                        所在城市
                    </label>
                    <div class="list-group-item-heading" style="font-size:14px">
                        <p></p>
                        <input type="hidden" id="DistrictId" name="DistrictId">
                        <span class="field-validation-valid" data-valmsg-for="DistrictId"></span>
                    </div>
                </a>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="AddressDetail">详细地址</label>
                    <div class="list-group-item-heading">
                        <textarea class="form-control" cols="20" data-val="true" data-val-required="详细地址 必填" id="AddressDetail" name="AddressDetail" rows="2">
</textarea>
                        <span class="field-validation-valid text-danger" data-valmsg-for="AddressDetail" data-valmsg-replace="true"></span>
                    </div>
                </div>
                <div class="list-group-item">
                    <label class="list-group-item-text" for="IsDefault">默认地址</label>
                    <div class="list-group-item-heading">
                        <input checked="True" data-val="true" data-val-required="默认地址 字段是必需的。" id="IsDefault" name="IsDefault" style="width:21px;height:21px;margin-left:14px" type="checkbox" value="true" /><input name="IsDefault" type="hidden" value="false" />
                    </div>
                </div>
</form>        </div>
    </div>
</div>


        </div>
    </div>
    <script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
    <script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/WbusiManage/js/app.js"></script>

    
    <script type="text/javascript">
        var disabledStorage = false;
        $(document).ready(function () {
            $("#btn_Save").click(function () {
                var flag = $("#addressForm").valid();
                var o = $("#addressForm").serialize();
                var btn = $(this);
                $("#addressForm span[data-valmsg-for=DistrictId]").attr("class", "field-validation-valid");
                if ($("input[name=DistrictId]").val() == "") {
                    $("#addressForm span[data-valmsg-for=DistrictId]").attr("class", "text-danger field-validation-error");
                    $("#addressForm span[data-valmsg-for=DistrictId]").html("请选择所在城市");
                    flag = false;
                }
                if (flag) {

                    $(this).addClass("disabled");
                    $.ajax({
                        url: $("#addressForm")[0].action, type: "post", data: o, success: function (result) {
                            disabledStorage = true;
                            clearStorageData();
                            if (result) {
                                var url = $("#requestUrl").val();
                                if (url != "") {
                                    location.href = url;
                                }
                            } else {
                                location.href = "Error"
                            }
                        }
                    });
                }
            });
        });
        window.onunload = storageData;
        window.onload = getStoreData;

        function getStoreData() {
            var addressId = window.localStorage["AddressId"];
            if (addressId != '') {
                return;
            }
            var receiver = window.localStorage["Receiver"];
            if (receiver) {
                $('#Receiver').val(receiver);
            }

            var tel = window.localStorage["Tel"];
            if (tel) {
                $('#Tel').val(tel);
            }

            var addressDetail = window.localStorage["AddressDetail"];
            if (addressDetail) {
                $('#AddressDetail').val(addressDetail);
            }
        }

        function storageData() {
            if (disabledStorage)
                return;

            var receiver = $('#Receiver').val();
            var tel = $('#Tel').val();
            var addressDetail = $('#AddressDetail').val();

            window.localStorage['Receiver'] = receiver;
            window.localStorage['Tel'] = tel;
            window.localStorage['AddressDetail'] = addressDetail;
            window.localStorage['AddressId'] = '';
        }

        function clearStorageData() {
            localStorage.removeItem('Receiver');
            localStorage.removeItem('Tel');
            localStorage.removeItem('AddressDetail');
            localStorage.removeItem('AddressId');
        }
    </script>

    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
</body>
</html>
