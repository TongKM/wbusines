<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>我的订单</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>

    
</head>
<body class=agency-browse>
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {

                        if (document.body.className == 'agency-browse') {
                            window.location.href = '/App#' + window.location.pathname + window.location.search;
                            return;
                        }

                        if (document.body.className == 'admin-browse') {
                            window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                            return;
                        }
                    }
                } else {
                    if (document.body.clientWidth <= 1100 && document.body.className == 'admin-browse') {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" value="我的订单" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">我的订单 </a>
            </div>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
        <jsp:include page="/WEB-INF/jsp/menu.jsp"></jsp:include>
        
    <div class="container bootcards-container" id="main">
        <div class="row">
<input type="hidden" id="biao" value="${active}">
<div class="navmenu search-nav">
    <ul class="nav">
        <li role="presentation" id="all">
            <a data-value="" data-title="全部订单" href="/WbusiManage/sys/order/BuyOrder?oStatus=0">
                全部
            </a>
        </li>
        <li role="presentation" class="tt" id="pay">
            <a data-value="2" data-title="待付款" href="/WbusiManage/sys/order/BuyOrder?oStatus=1">
                未交易
                    <!-- <i class="order-msg"></i> -->
            </a>
        </li>
        <li role="presentation" class="tt" id="send">
            <a data-value="4" data-title="待发货" href="/WbusiManage/sys/order/BuyOrder?oStatus=2">
                已交易
            </a>
        </li>
        <!-- <li role="presentation" class="tt" id="sent">
            <a data-value="3" data-title="已发货" href="/WbusiManage/sys/order/BuyOrder?oStatus=3">
                已发货
            </a>
        </li> -->
        <!-- <li role="presentation" class="tt" id="receive">
            <a data-value="6" data-title="已收货" href="/WbusiManage/sys/order/BuyOrder?oStatus=4">
                已收货
            </a>
        </li> -->
    </ul>
</div>
<div class="bootcards-cards">
    <div id="myorderlist" style="padding-top:35px">
    <!-- 遍历订单 -->
        <c:forEach items="${sessionScope.orderList}" var="order">
        <c:if test="${status==0 || null==status}">
        		<div class="order-item" id="${order.orderNo}" class="${order.id}">
            	<div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left" title="订单号" style="font-size:13px;">订单：${order.orderNo }</h3>
                    <span class="panel-title pull-right visible-xs">
                    <fmt:formatDate value="${order.oTime}" />
                    </span>
                    <span class="panel-title pull-right hidden-xs">
                    <fmt:formatDate value="${order.oTime}"/></span>
                </div>
                <div class="list-group">
                	<!-- 计算每张订单的商品总数 -->
                	<c:set var="count" scope="page" value="0"></c:set>
                	<!-- 遍历订单里的商品 -->
                	<c:forEach items="${order.shopList}" var="shop">
                        <a class="list-group-item order-good-item" href="javascript:;" data-id="GJ636528983267090490">
                            <img class="img-responsive img-rounded pull-left" src="${shop.pro.proPicPath}" title="${shop.pro.proName}" style="width:40px;height:40px">
                            <h4 class="list-group-item-heading">${shop.pro.proName}</h4>
                            <p class="list-group-item-text">&#165;${shop.pro.proPrice} x ${shop.count}</p>
                        	<c:set var="count" scope="page" value="${count+shop.count}"></c:set>
                        </a>
                    </c:forEach>
                    <div class="list-group-item text-right">
                        <p class="list-group-item-text" style="display:inline">
                            <span class="pull-left">共 ${count} 件商品</span>
                            总计：<b class="total-price"><span>¥</span>${order.oAmount}</b>
                        </p>
                        <!-- <div class="small-txt">(运费：1209.00)</div> -->
                    </div>
                </div>
                <div class="panel-footer text-right">
                <!-- 显示订单状态 -->
                <c:choose>
                	<c:when test="${order.oStatus==1}">
                		<span class="pull-left" style="color:#f8910b">等待付款</span>
                		<!-- 关闭订单1 -->
                		<a class="btn btn-desktop-default order-btn-small" data-ajax="true" 
                		 onclick="delOrder('${order.id}','${order.orderNo}');"
                		 href="javascript:;">关闭交易</a>
                		<!-- 付款 -->
                		<a href="/WbusiManage/sys/order/pay?orderId=${order.id}&oAmount=${order.oAmount}" class="btn btn-desktop-primary order-btn-small" 
                		data-id="GJ636528983267090490" style="color:#fff"><i class="icon-shopping-cart"></i>付款</a>
                	</c:when>
                	<c:when test="${order.oStatus==2}">
                		<span class="pull-left" style="color:#f8910b">已付款</span>
                	</c:when>
                </c:choose>
                    <!-- <span class="pull-left" style="color:#f8910b">等待付款</span> -->
                </div>
            </div>
        </div>
        </c:if>
        <c:if test="${status==1}">
        	<c:if test="${order.oStatus==1}">
        	
        		<div class="order-item" id="${order.orderNo}" class="${order.id}">
            	<div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left" title="订单号" style="font-size:13px;">订单：${order.orderNo }</h3>
                    <span class="panel-title pull-right visible-xs">
                    <fmt:formatDate value="${order.oTime}" />
                    </span>
                    <span class="panel-title pull-right hidden-xs">
                    <fmt:formatDate value="${order.oTime}"/></span>
                </div>
                <div class="list-group">
                	<!-- 计算每张订单的商品总数 -->
                	<c:set var="count" scope="page" value="0"></c:set>
                	<!-- 遍历订单里的商品 -->
                	<c:forEach items="${order.shopList}" var="shop">
                        <a class="list-group-item order-good-item" href="javascript:;" data-id="GJ636528983267090490">
                            <img class="img-responsive img-rounded pull-left" src="${shop.pro.proPicPath}" title="${shop.pro.proName}" style="width:40px;height:40px">
                            <h4 class="list-group-item-heading">${shop.pro.proName}</h4>
                            <p class="list-group-item-text">&#165;${shop.pro.proPrice} x ${shop.count}</p>
                        	<c:set var="count" scope="page" value="${count+shop.count}"></c:set>
                        </a>
                    </c:forEach>
                        <!-- <a class="list-group-item order-good-item" href="/Order/Detail/GJ636528983267090490" data-id="GJ636528983267090490">
                            <img class="img-responsive img-rounded pull-left" src="/upload/images/good/covers/201712/636485923839808496.jpg" title="办公用品" style="width:40px;height:40px">
                            <h4 class="list-group-item-heading">办公用品</h4>
                            <p class="list-group-item-text">&#165;12.00 x 2</p>
                        </a> -->
                    <div class="list-group-item text-right">
                        <p class="list-group-item-text" style="display:inline">
                            <span class="pull-left">共 ${count} 件商品</span>
                            总计：<b class="total-price"><span>¥</span>${order.oAmount}</b>
                        </p>
                        <!-- <div class="small-txt">(运费：1209.00)</div> -->
                    </div>
                </div>
                <div class="panel-footer text-right">
                <!-- 显示订单状态 -->
                <c:choose>
                	<c:when test="${order.oStatus==1}">
                		<span class="pull-left" style="color:#f8910b">等待付款</span>
                		<!-- 关闭订单 -->
                		<a class="btn btn-desktop-default order-btn-small" data-ajax="true" 
                		 onclick="delOrder('${order.id}','${order.orderNo}');"
                		 href="javascript:;">关闭交易</a>
                		<!-- 付款 -->
                		<a href="/WbusiManage/sys/order/pay?orderId=${order.id}&oAmount=${order.oAmount}" class="btn btn-desktop-primary order-btn-small" 
                		data-id="GJ636528983267090490" style="color:#fff"><i class="icon-shopping-cart"></i>付款</a>
                	</c:when>
                	<c:when test="${order.oStatus==2}">
                		<span class="pull-left" style="color:#f8910b">已付款</span>
                	</c:when>
                </c:choose>
                    <!-- <span class="pull-left" style="color:#f8910b">等待付款</span> -->
                </div>
            </div>
        </div>
        	</c:if>
        </c:if>
        
        <c:if test="${status==2}">
        	<c:if test="${order.oStatus==2}">
        		<div class="order-item" id="${order.orderNo}">
            	<div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left" title="订单号" style="font-size:13px;">订单：${order.orderNo }</h3>
                    <span class="panel-title pull-right visible-xs">
                    <fmt:formatDate value="${order.oTime}" />
                    </span>
                    <span class="panel-title pull-right hidden-xs">
                    <fmt:formatDate value="${order.oTime}"/></span>
                </div>
                <div class="list-group">
                	<!-- 计算每张订单的商品总数 -->
                	<c:set var="count" scope="page" value="0"></c:set>
                	<!-- 遍历订单里的商品 -->
                	<c:forEach items="${order.shopList}" var="shop">
                        <a class="list-group-item order-good-item" href="javascript:;" data-id="GJ636528983267090490">
                            <img class="img-responsive img-rounded pull-left" src="${shop.pro.proPicPath}" title="${shop.pro.proName}" style="width:40px;height:40px">
                            <h4 class="list-group-item-heading">${shop.pro.proName}</h4>
                            <p class="list-group-item-text">&#165;${shop.pro.proPrice} x ${shop.count}</p>
                        	<c:set var="count" scope="page" value="${count+shop.count}"></c:set>
                        </a>
                    </c:forEach>
                        <!-- <a class="list-group-item order-good-item" href="/Order/Detail/GJ636528983267090490" data-id="GJ636528983267090490">
                            <img class="img-responsive img-rounded pull-left" src="/upload/images/good/covers/201712/636485923839808496.jpg" title="办公用品" style="width:40px;height:40px">
                            <h4 class="list-group-item-heading">办公用品</h4>
                            <p class="list-group-item-text">&#165;12.00 x 2</p>
                        </a> -->
                    <div class="list-group-item text-right">
                        <p class="list-group-item-text" style="display:inline">
                            <span class="pull-left">共 ${count} 件商品</span>
                            总计：<b class="total-price"><span>¥</span>${order.oAmount}</b>
                        </p>
                        <!-- <div class="small-txt">(运费：1209.00)</div> -->
                    </div>
                </div>
                <div class="panel-footer text-right">
                <!-- 显示订单状态 -->
                <c:choose>
                	<c:when test="${order.oStatus==1}">
                		<span class="pull-left" style="color:#f8910b">等待付款</span>
                		<!-- 关闭订单 -->
                		<a class="btn btn-desktop-default order-btn-small" 
                		 onclick="delOrder('${order.id}','${order.orderNo}');"
                		 href="javascript:;">关闭交易</a>
                		<!-- 付款1 -->
                		<a href="/WbusiManage/sys/order/pay" class="btn btn-desktop-primary order-btn-small" 
                		data-id="GJ636528983267090490" style="color:#fff"><i class="icon-shopping-cart"></i>付款</a>
                	</c:when>
                	<c:when test="${order.oStatus==2}">
                		<span class="pull-left" style="color:#f8910b">已付款</span>
                	</c:when>
                </c:choose>
                    <!-- <span class="pull-left" style="color:#f8910b">等待付款</span> -->
                </div>
            </div>
        </div>
        	</c:if>
        </c:if>
        
        
        </c:forEach>
<!-- <div class="pager" data-action="/Order/BuyOrder?tradeStatus=0&amp;pageIndex=%23" data-current="1" data-updateTargetId="">
<a disabled="disabled"><<</a><a disabled="disabled"><</a><span data-pageIndex='1'>1</span><a disabled="disabled">></a>
<a disabled="disabled">>></a></div>
    </div>
</div> -->

        </div>
    </div>
    <script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
    <script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/WbusiManage/js/app.js"></script>

    
    <script type="text/javascript">
    	/* function cilck(){
    		$(this).attr('class','active');
    	} */
    	function delOrder(order,orderNo){
    		if(confirm("确认关闭吗？")){
    			//$("#"+order).html("");
    			$.ajax({
                url: "/WbusiManage/sys/order/closeOrder",
                type: "Post",
                data: { orderId: order},
                dataType:"JSON",
                success: function (result) {
                    if (result.data=="1") {
                    	//alert('成功');
                    	$("#"+orderNo).remove();
                        //doPaymentGood(result.dataKey);
                    } else {
                    	//alert("出错");
                    }
                }
            	});
    			//location.href="/WbusiManage/sys/order/closeOrder?orderId="+order;
    		}
    	}
    	$(function(){
    		var val = $("#biao").val();
			$("#"+val+"").addClass("active");
    	});
        function handlerSuccessClose(callback) {
            if (callback.success) {
                var id = callback.result;
                $('#' + id).find('.panel-footer').html('<span class="pull-left" style="color:#f8910b">交易关闭</span>');
                $('#alertModel').modal('hide');
            }
            else {
                var m = setTimeout(function () {
                    popMessage('成功', "关闭订单成功");
                    clearTimeout(m);
                }, 500);
            }
        }

        $('a').click(function () {
            if ($(this).attr('href') == '') return true;

            var id = $(this).attr('data-id');
            if (id == "") return true;

            var state = {
                title: '部落管家',
                url: location.href,
                id: id,
            };
            window.history.replaceState(state, state.title, state.url);
            return true;
        });

        window.onpopstate = function (event) {
            var currentState = window.history.state;
            if (currentState.id != undefined) {
                var a = document.createElement('a');
                $(a).attr('href', '#' + currentState.id);
                $(a).attr('style', 'display:none');
                document.body.appendChild(a);
                a.click();
                setTimeout(function () {
                    $(a).remove();
                }, 1000);
            }
        };
    </script>

    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/WbusiManage/js/weixinsdk.js"></script>

    <script type="text/javascript">
    if (isWeixin) {
        $.get('/home/getShare', function (data) {
            weixinService.init(data.title, data.desc, data.imgUrl, data.link);
        });
    }
    </script>
    <script src="/WbusiManage/src/scripts/takeit.js"></script>
</body>
</html>

