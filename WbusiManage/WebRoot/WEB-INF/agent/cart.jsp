<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>我的购物车</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>

    
    <style type="text/css">
        .no-cart {
            position: absolute;
            top: 25%;
            width: 100%;
        }

            .no-cart .icon-shopping-new {
                font-size: 45px;
                display: block;
                margin: 15px auto;
            }
    </style>

</head>
<body class=agency-browse>
    <input type="hidden" name="navmenu_title" value="我的购物车" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">我的购物车 </a>
            </div>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
    <jsp:include page="/WEB-INF/jsp/menu.jsp"></jsp:include>
    <div class="container bootcards-container" id="main">
        <div class="row">
        
<c:if test="${fn:length(sessionScope.cartList)!=0 && fin!=0}">
<div class="bootcards-cards" style="padding-top:0">
<form action="/WbusiManage/sys/order/addOrder" id="addOrderForm" method="post" name="addOrderForm"> 
		<input type="hidden" name="cartlist" value="${sessionScope.cartList }"/>         
  <input type="hidden" name="sourceOrderId" />
            <input type="hidden" name="isRetail" />
            <div class="panel panel-default">
                    <a href="javascript:;" class="list-group-item">
                        <div class="postaddress">
                            <div class="addrContent">
                                <span class="address-title">收货地址</span>：
                                <c:if test="${sessionScope.user.address==null}">
                                	<!-- <b>未设置</b> -->
                                </c:if>
                                <%-- <span id="addrContent">${sessionScope.user.address}</span> --%>
                                <input id="addrContent" name="reAddress" style="border-width: 0;outline: none;width: 400px" 
                                value="${sessionScope.user.address}" placeholder="未设置(第一次设置会保存为默认收货地址)">
                                <%-- <input type="hidden" name="reAddress" value="${sessionScope.user.address}"> --%>
                            </div>
                        </div>
                    </a>
            </div>
            <div class="panel panel-default">
                <div class="list-block media-list cart-list">
                    <ul>
                    <!-- 遍历放在购物车里的商品信息 -->
                    <c:forEach items="${sessionScope.cartList}" var="pro">
                            <li id="cart${pro.id}" class="cart-item" data-title="${pro.proName}" data-price="${pro.proPrice}" data-id="${pro.id}" 
                            data-num="${pro.count}" data-proCount="${pro.proCount }" data-goodid="${pro.id}" data-unit="${pro.proUnit}">
                                <input type="hidden" value="1" name="number" />
                                <div class="item-content item-link">
                                    <div class="item-media">
                                        <img src="${pro.proPicPath}" style="width:53px;height:53px" />
                                    </div>
                                    <div class="item-inner">
                                        <div class="item-title-row">
                                            <div class="title">
                                                <div>${pro.proName}</div>
                                                <div class="title-spec color-gray">单位：${pro.proUnit}</div>
                                                <span class="price-desc hidden"></span>
                                                <span class="accumulate-desc hidden"></span>
                                            </div>
                                            <div class="item-price" id="item-price">
                                                <div class="cart-o-price color-gray text-deleted hidden"></div>
                                                <div class="cart-c-price">￥${pro.proPrice}</div>
                                                <div class="cart-c-number color-gray text-right">数量：${pro.count}</div>
                                                <!-- 存放该种商品总价 -->
                                                <input type="hidden" id="${pro.id}" class="itemprice" name="" value="${pro.proPrice*pro.count}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                       </c:forEach>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default text-right" style="padding:5px;">
                <textarea class="form-control" cols="20"  name="message" placeholder="给卖家留言..." rows="3">
</textarea>
            </div>
            <div class="panel panel-default mark">
                <div class="panel-body">
                        <!-- <div class="text-right" id="divlimit">
                            首批最低货款：
                            <span id="firstLimitSum" data-value="100.00">&#165;100.00</span>
                        </div> -->
                        <hr />
                                        <div class="text-right">
                        <!-- <div class="paymentgood">
                            <span class="ti">货款</span>
                            <span class="content">¥0.00</span>
                        </div> -->
                        <!-- <div class="expressFee">
                            <span class="ti">运费</span>
                            <span class="content">¥0.00</span>
                        </div> -->
                        <div class="paymentotal">
                            <span class="ti">总计</span>
                            <span class="content" id="goodPrice" data-value=""></span>
                            <input type="hidden" name="oAmount" id="totalprice" value="">
                        </div>
                    </div>
                </div>
            </div>
</form>        <div class="panel panel-none" style="padding:0 5px;margin-bottom:50px">
            <button type="button" id="submit_order" class="btn btn-desktop-primary btn-block order-submit">生成订单</button>
        </div>
</div>
</c:if>

<c:if test="${fn:length(sessionScope.cartList)==0 || fin==0}">
	<div class="text-center body-padding-normal no-cart"><div>哦，哦，购物车空了…(⊙_⊙)…</div></div>
</c:if>
<!-- 编辑商品数量模态框 -->
<div class="modal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="form-horizontal">
                <div class="modal-header">
                    <div class="btn-group pull-left">
                        <button class="btn btn-danger" data-dismiss="modal">
                            取消
                        </button>
                    </div>
                    <div class="btn-group pull-right">
                        <button class="btn btn-success">
                            保存
                        </button>
                    </div>
                    <h3 class="modal-title">
                        编辑购物车
                    </h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="editId">
                    <input type="hidden" name="price">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <h4 class="edit-title" style="padding:8px 0px 8px 15px"></h4>
                        </div>
                    </div>
                    <div class="form-group">
                    <input type="hidden" id="buyerid" value="${sessionScope.user.id}">
                        <label class="col-xs-3 control-label">数量</label>
                        <div class="col-xs-9">
                            <input type="number" name="num" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" style="color:#000">单价</label>
                        <div class="col-xs-9">
                            <p class="edit-price" style="padding-top:7px"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" style="color:#000">小计</label>
                        <div class="col-xs-9">
                            <p class="edit-subtotal" style="padding-top:7px;color:#f40"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-block btn-del">
                    <i class="icon-delete-new"></i>
                    从购物车删除
                </button>
            </div>
        </div>
    </div>
</div>

        </div>
    </div>
    <script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
    <script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/WbusiManage/js/app.js"></script>

    
    <script type="text/javascript">
    	totalprice();
    	//计算总价函数
    	function totalprice(){
    		var total=0;
    		$(".itemprice").each(function(){
    			var price=this.value;
    			total=parseFloat(total)+parseFloat(price);
    		});
    		if(total!=0){
    			document.getElementById('goodPrice').innerHTML='￥'+total;
    			document.getElementById('totalprice').value=total;
    		}
    		
    		
    		//alert(document.getElementById('totalprice').value);
    	}
    	
        var sourceOrderId = $('[name="sourceOrderId"]').val();
        var remarkValue = '';

        window.onload = pageload;
        window.onunload = function () {
            var remark = 'remark';
            if (remarkValue.length <= 0) {
                remarkValue = $('#Remark').text();
            }
            set_cookie(remark, remarkValue);
        };

        $('#Remark').focusout(function () {
            remarkValue = $(this).val();
        });

        function pageload() {
            if (sourceOrderId) {
                window.localStorage['SourceOrderId'] = sourceOrderId;
            }

            var remark = 'remark';
            var remark = getCookie(remark);
            $("#Remark").text(remark);
        }
        var host = window.location.host;
        var path = 'http://' + host + '/Cart/Index';
        ///存放cookie内容
        function set_cookie(name, value, minutes, path, domain, secure) {
            var cookie = name + '=' + escape(value);
            if (minutes) {
                var expiration = new Date((new Date()).getTime() + minutes * 60000);
                cookie += ';expires=' + expiration.toGMTString();
            }
            if (path) cookie += ';path=' + path;
            if (domain) cookie += ';domain=' + domain;
            if (secure) cookie += ';secure';
            document.cookie = cookie;
        }
        ///获得cookie内容
        function getCookie(cookieName) {
            var cookieString = document.cookie;
            var start = cookieString.indexOf(cookieName + '=');
            // 加上等号的原因是避免在某些 Cookie 的值里有
            // 与 cookieName 一样的字符串。
            if (start == -1) // 找不到
                return null;
            start += cookieName.length + 1;
            var end = cookieString.indexOf(';', start);
            if (end == -1) return unescape(cookieString.substring(start));
            return unescape(cookieString.substring(start, end));
        }
    </script>
    <script type="text/javascript">
    	//提交订单单击事件
        $('#submit_order').click(function () {
            //var addressId = $('input[name="AddressId"]').val();
            var address=document.getElementById('addrContent').value;
            if (address == '' || address == undefined) {
                ShowErrorMessage("请先设置收货地址", 1500);
                return;
            }
            var num=0;
            $("li").each(function(){
            	//获取商品库存数量proCount/购买数量count
            	var proCount=$(this).attr("data-proCount");
            	var count=$(this).attr("data-num");
            	//var count=$(this).find(".cart-c-number color-gray text-right").
            	var title=$(this).attr("data-title");
            	var unit=$(this).attr("data-unit");
            	if(parseInt(count)>parseInt(proCount)){
            		alert(title+' 的库存不足，无法生成订单！最大购买数量:'+proCount+'，单位:'+unit);
            		num++;
            	}
            });
            if(num>0){
            	return;
            }
            
            /* var goodPrice = parseFloat($('#goodPrice').data('value'));

            if ($('#firstLimitSum').length > 0) {
                var limitSum = parseFloat($('#firstLimitSum').data('value'));
                if (limitSum > goodPrice) {
                    ShowErrorMessage("货款金额必须大于首批最低货款");
                    return;
                }
            } */

            /* var isRetail = $('input[name="isRetail"]').val();
            if ($('#secondLimitSum').length > 0 && isRetail == 'false') {
                var limitSum = parseInt($('#secondLimitSum').data('value'));
                if (limitSum > goodPrice) {
                    ShowErrorMessage("货款金额必须大于续批最低金额");
                    return;
                }
            } */

            if (!sourceOrderId) {
                sourceOrderId = window.localStorage['SourceOrderId'];
                if (sourceOrderId)
                    $('[name="sourceOrderId"]').val(sourceOrderId);
            }
            window.localStorage['SourceOrderId'] = '';
            $('#addOrderForm')[0].submit();
        });

        ShowSuccessMessage = function (message, life) {
            var time = 1500;
            if (life) {
                time = life;
            }
            $("#tip_message").remove();
            var msg = '<div id="tip_message"><span>' + message + "</span></div>";
            $("body").append(msg);

            $("#tip_message").show(function () {
                setTimeout(function () {
                    $("#tip_message").fadeOut(20);
                }, time)
            });
        };

        ShowErrorMessage = function (message, life) {
            ShowSuccessMessage(message, life);
            $("#tip_message span").addClass("error");
        };

        function doPaymentGood(data) {
            $(".paymentgood .content").html("¥" + data.cartAmount.toFixed(2));
            $(".expressFee .content").html("¥" + data.expressFee.toFixed(2));

            var payAmount = data.cartAmount + data.expressFee;
            $(".paymentotal .content").html("¥" + payAmount.toFixed(2));
            $(".paymentotal .content").data("value", data.cartAmount.toFixed(2));

            $('.shoping-cart-num').html(data.cartCount);

            $('input[name="isRetail"]').val(data.isRetail);
            data.purchaseItems.forEach(function (e) {
                var $item = $('.cart-list .cart-item[data-goodId="' + e.goodId + '"]');

                if (data.levelId != data.realLevelId || data.isRetail == true) {
                    $item.find('.cart-o-price').removeClass('hidden');
                    $item.find('.price-desc').removeClass('hidden');
                } else {
                    $item.find('.cart-o-price').addClass('hidden');
                    $item.find('.price-desc').addClass('hidden');
                }

                $item.data('num', e.number);
                $item.data('price', e.calcPrice);
                $item.find('.cart-o-price').html(e.levelPrice.toFixed(2));
                $item.find('.cart-c-price').html(e.calcPrice.toFixed(2));
                $item.find('.cart-c-number').html("数量：" + e.number);

                var title = e.calcTitle || '代发';
                if (title.length > 6)
                    title = title.substring(0, 6) + '...';
                $item.find('.price-desc').html(title);

                $item.find('.accumulate-desc').addClass('hidden');

                if (e.accumulateAmount != null) {
                    $item.find('.accumulate-desc').html('已购' + e.accumulateAmount + '件');
                    $item.find('.accumulate-desc').removeClass('hidden');
                }
            });
        }

        $(".cart-list .cart-item").click(function () {
            var title = $(this).data('title');
            var id = $(this).data("id");
            var num = parseInt($(this).data('num'));
            var price = parseFloat($(this).data('price'));
            var subtotal = (num * price).toFixed(2);
            $("#editModal .edit-title").html(title);
            $("#editModal .edit-price").html("¥" + price.toFixed(2));
            $("#editModal .edit-subtotal").html("¥" + subtotal);
            $("#editModal input[name=price]").val(price);
            $("#editModal input[name=num]").val(num);
            $("#editModal input[name=editId]").val(id);
            $('#editModal').modal();
        });
		//修改数量或删除商品后点击‘保存’
        $('#editModal .btn-success').click(function () {
            var num = parseInt($('#editModal input[name="num"]').val());
            if (isNaN(num) || num <= 0) {
                $('#editModal input[name="num"]').focus();
                return false;
            }
            var id = $('#editModal input[name="editId"]').val();
            var $item = $('.cart-list .cart-item[data-id="' + id + '"]');
            var price = parseFloat($item.data('price'));
            var subtotal = (price * num).toFixed(2);//修改数量后的价格

            $item.find('.cart-c-number').html("数量：" + num);
            $item.data('num', num);

            var addressId = $("#AddressId").val() || "";
            var buyerid=document.getElementById('buyerid').value;
            $('#editModal').modal("hide");
            $.ajax({
                url: "/WbusiManage/sys/cart/EditCart",
                type: "Post",
                data: { proId: id, count: num, buyerid: buyerid },
                success: function (result) {
                    if (result.success) {
                        doPaymentGood(result.dataKey);
                    } else {
                    	var price=$("#editModal input[name=price]").val();//获取单价
                    	var tprice=price*num;//计算修改数量后该种商品总价
                    	$("#"+id).val(tprice);//将总价设置到隐藏域里
                    	totalprice();//调用计算总价函数
                    	//将修改后的数量设置在li的data-num里
                    	$("#cart"+id).attr("data-num",num);
                        //ShowErrorMessage(result.message);
                    }
                }
            });
            
        });

        $('#editModal input[name="num"]').bind('keyup change blur', function () {
            var _num = $('#editModal input[name="num"]').val().trim();
            if (_num == "") return;
            var num = parseInt(_num);
            if (isNaN(num) || num <= 0) {
                $('#editModal input[name="num"]').val(1);
                return false;
            }
            var pirce = parseFloat($('#editModal input[name="price"]').val());
            var subtotal = (num * pirce).toFixed(2);
            $('#editModal .edit-subtotal').html('¥' + subtotal);
        });
		//删除商品
        $('#editModal .btn-del').click(function () {
            var id = $('#editModal input[name="editId"]').val();
            $('#editModal').modal('hide');
            var $item = $('.cart-list .cart-item[data-id="' + id + '"]');
            $item.remove();
            if ($(".cart-list .cart-item").length == 0) {
                window.localStorage['SourceOrderId'] = '';
                $(".bootcards-cards").html("<div class=\"text-center body-padding-normal no-cart\"><div>哦，哦，购物车空了…(⊙_⊙)…</div></div>");
                $('.shoping-cart-num').hide();
            };
			var buyerid=document.getElementById('buyerid').value;
            var addressId = $("#AddressId").val() || "";
            $.ajax({
                url: "/WbusiManage/sys/cart/delPro",
                type: "Post",
                data: { proId: id, buyerid: buyerid },
                success: function (result) {
                    if (result.success) {
                        if ($(".cart-list .cart-item").length != 0) {
                            doPaymentGood(result.dataKey);
                        }
                    } else {
                    	totalprice();
                        //ShowErrorMessage(result.message);
                    }
                }
            });
        });

        var _addressId = $('#AddressId').val() || "";
        /* $.post('/Cart/GetPayInfo?addressId=' + _addressId, function (callback, status) {
            if (status == 'success') {
                if (callback.success) {
                    doPaymentGood(callback.dataKey);
                    $('#submit_order').removeClass('disabled');
                } else {
                    ShowErrorMessage('请求失败');
                }
            } else {
                ShowErrorMessage('请求失败');
            }
        }) */
    </script>

    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
</body>
</html>
