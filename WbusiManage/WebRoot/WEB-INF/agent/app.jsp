<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <title>微霸 - 代理版</title>
    <link href="/WbusiManage/src/bootstrap-3.3-2.5-dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" />

    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/src/content/app/style.less" type="text/css" rel="stylesheet/less" />

    <script src="/WbusiManage/src/less.js-3.x/dist/less.min.js"></script>

    <style type="text/css">
        .page-content, .cover {
            bottom: 0;
            width: 100%;
            height: calc(100% - 46px);
            border: 0;
            overflow: hidden;
            position: absolute;
            top: 46px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row container-row">
        <div id="message" class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-lg-3 col-md-1 col-sm-1 container-col hidden-xs no-message" style="overflow:hidden">
            <div class="msg-title panel panel-default">
                <div class="panel-body text-center">
                        <span class="icon-alert">
                        </span>
                    <span class="hidden-xs hidden-sm hidden-md">
                            消息中心<span id="msgCount"></span>
                        </span>
                </div>
            </div>

            <div id="msgCenter">
                <div class="msg-list">
                    <div class="default-msg panel panel-default resolved no-need-resolve">
                        <div class="panel-body">
                                <span class="icon-alert">
                                </span>
                            <br />
                            <span class="text">
                                    没有消息
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="app" class="col-lg-7 col-md-9 col-sm-9 col-xs-12 container-col no-message">
            <div class="page">
                <div class="page-title">
                    <div class="row ">
                        <div class="col-md-4 col-sm-4 col-xs-8">
                            <span style="margin-left:25px;"></span>

                            <%-- <a href="/WbusiManage/${pagePath}"><i class="bt-home icon icon-home" title="首页"></i></a> --%>
                            <a href="javascript:;" onclick="history.back();">
                            <i class="bt-back icon icon-back icon-left-new" title="返回"></i></a>
                            <a href="javascript:;" onclick="location.reload();">
                            <i class="bt-refresh icon icon-refresh" title="刷新应用"></i></a>

                        </div>

                        <div class="col-md-4 col-sm-4 text-center hidden-xs">代理版</div>

                        <div class="col-md-4 col-sm-4 col-xs-4 text-right">
                            <a href="/WbusiManage/user/Logout"><i class="bt-logout icon icon-signin" title="安全退出"></i></a>
                            <!-- <a href="http://www.buluoguanjia.com/Help" class="bt-user icon icon-doubt" title="帮助中心" target="_blank"></a> -->
                            <span style="margin-right:35px;"></span>
                        </div>
                    </div>

                </div>

                <iframe class="page-content" id="iframeMain" src="/WbusiManage${pagePath}">
                	
                </iframe>

            </div>
        </div>
    </div>
</div>
</body>
</html>
