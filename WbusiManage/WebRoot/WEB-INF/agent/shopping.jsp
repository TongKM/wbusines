<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>微霸 - 代理端</title>
    <meta name="keyword" content="微霸">
    <meta name="description" content="微霸">
    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />
    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>


    <link href="/WbusiManage/src/swiper/swiper3.1.0.min.css" rel="stylesheet" />
    <style>
        .swiper-pagination {
            bottom: 40px !important;
            left: 0px !important;
            width: 100% !important;
        }

        #productItemsPanel .list-group-item .list-group-item-heading {
            font-size: 15px;
            max-height: 2.2em;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: normal;
        }
        .agency-browse .navbar-fixed-top{display:block;}
        body.agency-browse {padding-top: 44px;}
    </style>

</head>
<body class=agency-browse>
<input type="hidden" name="navmenu_title" value="在线下单" />
<!-- fixed top navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand no-break-out" title="Customers" href="#">在线下单 </a>
        </div>
        <!-- menu button to show/ hide the off canvas slider -->
		
        <button type="button" class="btn btn-default btn-menu pull-left offCanvasToggle" data-toggle="offcanvas" id="btn_menu">
            <!-- <i class="icon-search" style="color:#999"></i> -->
        </button>

    </div>
</div>
	<!-- 搜索商品 -->
	<input type="text" name="keywords" id="cc" class="form-control search-txt" 
	placeholder="按名称搜索">
<!-- 引入页面底部的导航栏 -->
<jsp:include page="/WEB-INF/jsp/menu.jsp"></jsp:include>
<div class="container bootcards-container" id="main">
    <div class="row">

        <div class="bootcards-cards">
            <div class="items-container" id="productItemsPanel">
                <div class="panel panel-default">
                    <div class="list-group" id="proList">
                    <c:forEach items="${sessionScope.list}" var="pro">
                    	<a class="list-group-item list-item" onclick="itemClick(this)" data-title="${pro.proName}" data-id="${pro.id}" 
                        data-url="${pro.proPicPath}" data-spec="${pro.proUnit}" data-jprice="${pro.proPrice}" data-price="308.00" 
                        data-buyer="${sessionScope.user.id}" data-seller="${sessionScope.user.superior}" data-img="${pro.proPicPath}" 
                        data-proDesc="${pro.proDesc}" data-proCount="${pro.proCount}" href="#">
                            <div class="row">
                                <input type="hidden" name="descImg" value="/WbusiManage/upload/images/good/covers/201712/636489628219657747.jpg" />
                                <div class="col-sm-12">
                                
                                    <img class="img-responsive img-rounded pull-left" src="${pro.proPicPath}" style="width:60px;height:60px;">
                                    <h4 class="list-group-item-heading">${pro.proName}</h4>
                                    <small class="text-muted">单位：${pro.proUnit }</small>---
                                    <small class="text-muted">库存：${pro.proCount}</small>
                                    <div style="padding-left:75px">
                                        <p class="list-group-item-text">
                                            <span class="price-font" style="font-size:16px">￥${pro.proPrice}</span><span class="product-small"></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </a><br>
                    </c:forEach>
                    </div>
                </div>
            </div>
        </div>
        <!-- 点击商品后出现的模态框 -->
        <div class="modal" id="docsModal" tabindex="-1" role="dialog" aria-labelledby="docsModal" 
        aria-hidden="true">
            <div class="cart-ui-dialog modal-dialog">
                <div class="modal-content" style="border:0px;border-radius:0px 0px 6px 6px" >
                    <div class="img-box swiper-container" >
                    </div>
                    <span class="dialog-close" style="z-index:10;padding: 10px 0 0 24px;">
                <img src="/WbusiManage/src/images/site/close-64-64.png" style="width:26px" />
            </span>
                    <span class="cart-product-price"></span>
                    <div class="number-control" style="float:right">
                        <div class="mui-number">
                            <a href="javascript:void(0)" class="decrease cart-change-btn disabled" style="text-decoration:none">-</a>
                            <input title="订购数量" id="count" type="text" onkeyup="this.value =this.value.replace(/\D/g,'')" onafterpaste="this.value =this.value.replace(/\D/g,'')" name="ttnumber" value="1" class="num">
                            <a href="javascript:void(0)" class="increase cart-change-btn" style="text-decoration:none">+</a>
                        </div>
                    </div>
                    <div class="product-desc product-desc-content" style="max-height:63px;line-height:21px;margin-top:13px;padding-left:15px;padding-right:10px; overflow:hidden">
                    </div>
                    <div class="cart-operate">
                        <a class="btn btn-warning add-cart-btn" style="color:#f1593f !important;" href="javascript:void(0)">
                            <i class="icon-shopping-new"></i>
                            <span>加入购物车</span>
                        </a>
                        
                    </div>
                </div>
            </div>
        </div>
        <nav class="navmenu offcanvas offcanvas-left">
            <div class="list-group" style="background:#fff">
                <div class="list-group-item" style="font-size:16px;line-height:20px">产品类别</div>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping">
                    全部产品
                </a>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping?categotyId=1">
                    健康酒
                </a>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping?categotyId=5">
                    日化用品
                </a>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping?categotyId=17">
                    食品
                </a>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping?categotyId=21">
                    玫瑰测试
                </a>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping?categotyId=22">
                    游戏
                </a>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping?categotyId=12">
                    保健品
                </a>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping?categotyId=15">
                    女装
                </a>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping?categotyId=25">
                    测试
                </a>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping?categotyId=26">
                    测试
                </a>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping?categotyId=16">
                    饰品
                </a>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping?categotyId=11">
                    白酒
                </a>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping?categotyId=4">
                    宠物
                </a>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping?categotyId=24">
                    白酒
                </a>
                <a class="list-group-item" style="font-size:16px;line-height:20px" href="/Shopping?categotyId=23">
                    纸尿裤
                </a>
            </div>
        </nav>

    </div>
</div>
<input type="hidden" id="flag" value="${flag}"/>
<script src="/WbusiManage/src/jquery/jquery-2.1.4.min.js"></script>
<script src="/WbusiManage/src/scripts/bootstrap.min.js"></script>
<script src="/WbusiManage/src/scripts/fastclick.min.js"></script>
<script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
<script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
<script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
<script src="/WbusiManage/src/bootcards-1.1.2/js/bootcards.min.js"></script>
<script src="/WbusiManage/js/app.js"></script>


<script type="text/javascript" src="/WbusiManage/src/swiper/swiper3.1.0.min.js"></script>
<script type="text/javascript" src="/WbusiManage/src/jquery-ui-1.11.4/jquery-ui-effect.min.js"></script>
<script type="text/javascript">

	$(function(){//用于判断是否显示小红点
		if($("#flag").val() == 0) {
			$(".shoping-cart-num").hide();
		}else{
			$(".shoping-cart-num").show();
		} 
	});
	window.onload=function(){
  			var proName=document.getElementById("cc");
  			//输入框改变内容触发
     		proName.oninput=function(){
       		//alert(1);
       		$.ajax({
            url: "/WbusiManage/sys/pro/find", 
            type: "Post", 
            dataType:"json",
            data: {proName:proName.value,uId:1}, 
            success: function (result) {
            	document.getElementById('proList').innerHTML="";
            	//$("#proList").empty();
            	var html="";
            	var list=result.list;
            	var user=result.user;
               	for(var i=0;i<list.length;i++){
               		html+="<a class='list-group-item list-item' onclick='itemClick(this)'";
               		html+=" data-title="+list[i].proName+" data-id="+list[i].id;
               		html+=" data-url="+list[i].proPicPath+" data-spec="+list[i].proUnit;
               		html+=" data-jprice="+list[i].proPrice+" data-buyer="+user.id;
               		html+=" data-seller="+user.superior+" data-img="+list[i].proPicPath;
               		html+=" data-proDesc="+list[i].proDesc+" data-proCount="+list[i].proCount+" href='#'";
               		html+="<div class='row' style='height: 85.8px;'><input type='hidden' name='descImg'";
                    html+=" value='/WbusiManage/upload/images/good/covers/201712/636489628219657747.jpg' />";
                    html+="<div class='col-sm-12'>";
					html+="<img class='img-responsive img-rounded pull-left'";
                    html+="src="+list[i].proPicPath+" style='width:60px;height:60px;'>";
                    html+="<h4 class='list-group-item-heading'>"+list[i].proName+"</h4>";
                    html+="<small class='text-muted'>单位："+list[i].proUnit+"</small>---";
                    html+="<small class='text-muted'>库存："+list[i].proCount+"</small>";
                    html+="<div style='padding-left:75px'><p class='list-group-item-text'>";
                    html+="<span class='price-font' style='font-size:16px'>";
                    html+="￥"+list[i].proPrice+"</span><span class='product-small'></span>";
                    html+="</p></div></div></div></a><br>";
               	}
               	document.getElementById('proList').innerHTML=html;
            }
        	});
     	}
}
    function itemClick(e) {
        var self = e;
        var goodId = $(self).attr("data-id");//商品id
        var title = $(self).attr("data-title") +'（'+ $(self).attr("data-spec")+'）';
        //var desc = $(self).attr("data-desc");
        var jprice = $(self).attr("data-jprice");
        var img=$(self).attr("data-img");//商品图片路径
        var buyer=$(self).attr("data-buyer");//买家id
        var seller=$(self).attr("data-seller");//卖家id
        var proDesc=$(self).attr("data-proDesc");//商品描述
        var proCount=$(self).attr("data-proCount");//商品库存

        $("#docsModal .add-cart-btn").attr("data-id", goodId);
        $("#docsModal .add-cart-btn").attr("data-buyer", buyer);
        $("#docsModal .add-cart-btn").attr("data-seller", seller);
        $("#docsModal .add-cart-btn").attr("data-proCount", proCount);
        $("#docsModal .product-desc-content").html(proDesc);
        $("#docsModal [name=ttnumber]").val(1);

        if (!$("#docsModal .decrease").hasClass("disabled")) {
            $("#docsModal .decrease").addClass("disabled");
        }
        $("#docsModal .cart-product-price").html("￥" + jprice);

        $("#docsModal .img-box").remove();

        var container = document.createElement("div");
        $(container).attr("class", "img-box swiper-container");

        var wrapper = document.createElement("div");
        $(wrapper).attr("class", "swiper-wrapper");

        var pagination = document.createElement("div");
        //$(pagination).attr("class", "swiper-pagination");

        $(self).find("[name=descImg]").each(function () {

            var slide = document.createElement("div");
            $(slide).attr("class", "swiper-slide");
            $(slide).attr("height","100px");
            $(slide).html("<img src='" + img + "'/>");
            wrapper.appendChild(slide);

            /* var bullet = document.createElement("span");
            $(bullet).attr("class", "swiper-pagination-bullet");
			
            pagination.appendChild(bullet); */
        });

        container.appendChild(wrapper);
        container.appendChild(pagination);

        var priceDiv = document.createElement("div");
        $(priceDiv).attr("class", "cart-product-title");
        $(priceDiv).attr("style", "z-index:10");
        $(priceDiv).html(title);

        container.appendChild(priceDiv);

        $(container).insertBefore("#docsModal .dialog-close");

        $("#docsModal").modal();
        setTimeout(function () {
            var swiper = new Swiper('.swiper-container', {
                direction: 'horizontal',
                loop: true,
                // 如果需要分页器
                pagination: '.swiper-pagination',
            });
        }, 300);
        return false;
    }
	//点击‘加入购物车’进入
    $(".cart-ui-dialog .add-cart-btn").click(function () {
        var img = $(".cart-ui-dialog .swiper-slide-active").find("img").eq(0);
        var num = parseInt(10);/* $(".cart-ui-dialog [name=ttnumber]").val() */
        if (isNaN(num) || num <= 0) {
            $(".cart-ui-dialog [name=ttnumber]").focus();
            return;
        }
        //获取所需数据proId,count, buyer,seller
        var proId=$("#docsModal .add-cart-btn").attr("data-id");
        var count=document.getElementById('count').value;
        var buyer1=$("#docsModal .add-cart-btn").attr("data-buyer");
        var seller1=$("#docsModal .add-cart-btn").attr("data-seller");
        var proCount=$("#docsModal .add-cart-btn").attr("data-proCount");
        if(parseInt(count)>parseInt(proCount)){
        	alert('库存数量不足，无法加入购物车！');
        	return;
        }
        //计算库存剩余数量
        var remain=proCount-count;
        addToCart(proId, count, buyer1,seller1);
    });
    $(".cart-ui-dialog .cart-change-btn").click(function () {
        var num = parseInt($(".cart-ui-dialog .num").val());
        if ($(this).hasClass("decrease")) {
            num--;
        } else {
            num++;
        }
        if (num <= 1) {
            num = 1;
            $(".cart-ui-dialog .decrease").addClass("disabled");
        } else {
            $(".cart-ui-dialog .decrease").removeClass("disabled");
        }
        $(".cart-ui-dialog .num").val(num);
    })

    var canAdd = "true";
    ///加入购物车，需获取的数据：商品id/商品数量/买家id/买家id
    function addToCart(proId, count, buyer,seller) {
        if (canAdd == "false") return;

        var cart = new Object();
        //cart["GoodId"] = $(o).attr("data-id");
        //cart["Number"] = num;
		//购物车右上角小红点
        
        //addCartFlash(img);
        
        $(".shoping-cart-num").show();
        $("#docsModal").modal("hide");
        $.ajax({
            url: "/WbusiManage/sys/shop/addToCart", 
            type: "Post", 
            dataType:"json",
            data: {proId:proId,count:count,buyer:buyer,seller:seller}, 
            success: function (result) {
                canAdd = "true";
                
            }
        });
    };

    function addCartFlash(img) {
        $(".shoping-cart-num").animate({
            backgroundColor: "#ddd"
        }, 400);

        setTimeout(function () {
            $(".shoping-cart-num").animate({
                backgroundColor: "red"
            }, 300);
        }, 500);
    };

    $(".dialog-close").click(function () {
        $("#docsModal").modal("hide");
    });
</script>

<script type="text/javascript">
    bootcards.init({
        offCanvasHideOnMainClick: true,
        offCanvasBackdrop: true,
        enableTabletPortraitMode: true,
        disableRubberBanding: false,
        disableBreakoutSelector: 'a.no-break-out'
    });

    //fix for minimal-ui bug in Safari:
    //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
    if (bootcards.isXS()) {
        window.addEventListener("orientationchange", function () {
            window.scrollTo(0, 0);
        }, false);

        window.scrollTo(0, 0);
    }
</script>
<script type="text/javascript">
	
    var value = $('input[name="navmenu_title"]').val();
    if (value) {
        $('a[data-title="' + value + '"]').parent().addClass('active');
    }
</script>

</body>
</html>

