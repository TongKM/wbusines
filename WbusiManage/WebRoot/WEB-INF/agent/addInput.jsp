<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no" />
    <title>充值申请</title>
    <meta name="keyword" content="部落">
    <meta name="description" content="部落管家">
    <link href="/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/src/icomoon/style.css" rel="stylesheet" />
    <link href="/css/v1?v=uRKGEzIRg2Dr_9xFU-CwP1fTY_wxVN7k5M0YNhcKRnA1" rel="stylesheet"/>

    
</head>
<body class=agency-browse>
        <script type="text/javascript">
            (function appInit() {
                var iframe = top.document.getElementById('iframeMain');
                if (!iframe) {
                    if ((window.navigator.userAgent.toLowerCase().indexOf("micromessenger") < 0 || !window.navigator.userAgent.toLowerCase().match('/(iphone|ipod|ipad|android|ios)/i')) && document.body.clientWidth >= 1024) {

                        if (document.body.className == 'agency-browse') {
                            window.location.href = '/App#' + window.location.pathname + window.location.search;
                            return;
                        }

                        if (document.body.className == 'admin-browse') {
                            window.location.href = '/Admin/App#' + window.location.pathname + window.location.search;
                            return;
                        }
                    }
                } else {
                    if (document.body.clientWidth <= 1100 && document.body.className == 'admin-browse') {
                        var css = document.createElement('link');
                        css.href = "/src/content/font.css";
                        css.rel = 'stylesheet';
                        document.getElementsByTagName('head')[0].appendChild(css);
                    }
                }
            })();
        </script>
    <input type="hidden" name="navmenu_title" value="充值申请" />
    <!-- fixed top navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand no-break-out" title="Customers" href="#">充值申请 </a>
            </div>
            <!-- menu button to show/ hide the off canvas slider -->
            
        </div>
    </div>
        <div class="navbar navbar-default navbar-fixed-bottom">
            <div class="container">
                <div class="btn-group">
                    <a href="/Shopping" class="btn btn-default ">
                        <i class="icon-home-o-new"></i>
                        首页
                    </a>
                    <a href="/Cart" class="btn btn-default shoping-cart ">
                        <i class="icon-shopping-cart-o-new" style="position:relative;">
                                <span class="shoping-cart-num label" style="margin-left:-2px !important;margin-top:-3px !important;position:absolute;">2</span>
                        </i>
                        购物车
                    </a>
                    <a href="/Agency/PersonalCenter" class="btn btn-default active">
                        <i class="icon-people-o-new" style="position:relative;">
                                <span class="personal-center-msg label" style="margin-left:-5px !important;margin-top:-3px !important;position:absolute;">1</span>
                        </i>
                        个人中心
                    </a>
                </div>
            </div>
        </div>
    <div class="container bootcards-container" id="main">
        <div class="row">
            

<div class="bootcards-cards">
    <form action="/Deposit/AddInput" class="" data-ajax="true" data-ajax-begin="handlerBegin" data-ajax-failure="handlerFailure" data-ajax-method="POST" data-ajax-success="handlerSuccess" id="form0" method="post">        <div class="panel panel-default panel-base">
            <input data-val="true" data-val-required="Guid 字段是必需的。" id="TargetUserId" name="TargetUserId" type="hidden" value="00000000-0000-0000-0000-000000000000" />
            
            <div class="list-group list-group-content">
                <div class="list-group-item">
                    <label>账户</label>
                    <div class="content deposit-pay">
                        <div class="pay-list">
                            <div class="pay-row">
                                <label class="label-checkbox" name="pay-box">
                                    <input type="radio" value="1" name="PayType" id="payType1" checked />
                                    <i class="icon icon-form-checkbox"></i>
                                    <span class="inner-text">支付宝</span>
                                </label>
                                    <div class="pay-content pay-zhifubao-content" style="padding-bottom:10px;padding-left: 0">
                                        <div class="pay-info">17608388303</div>
                                    </div>
                            </div>
                            <div class="pay-row">
                                <label class="label-checkbox" name="pay-box">
                                    <input type="radio" value="2" name="PayType" id="payType2" />
                                    <i class="icon icon-form-checkbox"></i>
                                    <span class="inner-text">微信支付</span>
                                </label>
                                    <div class="pay-content pay-weixin-content" style="padding-left: 0">
                                        <img alt="图片" src="/upload/images/payimages/201704/636285538624623965.jpg" class="weixin-pay-pic" />
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list-group-item">
                    <label for="Amount">金额</label>
                    <div class="content list-group-item-heading">
                        <input class="form-control" data-val="true" data-val-number="The field 金额 must be a number." data-val-range="金额在0.01~9999999之间" data-val-range-max="9999999" data-val-range-min="0.01" data-val-required="金额在0.01~9999999之间" id="Amount" name="Amount" placeholder="金额" style="padding-left:0" type="text" value="" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Amount" data-valmsg-replace="true"></span>
                    </div>
                </div>
                <div class="list-group-item">
                    <label for="ApplyMemo">留言</label>
                    <div class="content">
                        <input class="form-control" id="ApplyMemo" name="ApplyMemo" placeholder="申请说明" style="padding-left:0" type="text" value="" />
                    </div>
                </div>
                <div class="list-group-item">
                    <div style="height:150px;margin-top:15px">
                        <div class="takephoto-queue takephoto-queue-single">
                        </div>
                        <div class="takephoto-trigger takephoto-trigger-single" data-length="1" data-material="depositMaterial" data-input-img="payImg" data-input-file="pay_file">
                            <div class="add-icon">+</div>
                            <div class="add-text">上传凭证</div>
                        </div>
                        <input type="file" name="files" id="pay_file" accept="image/*" style="display:none" />
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-none" style="margin:10px 10px 25px 10px;">
            <input type="submit" value="确定" class="btn btn-desktop-primary btn-block">
        </div>
</form></div>

        </div>
    </div>
    <script src="/src/jquery/jquery-2.1.4.min.js"></script>
    <script src="/src/scripts/bootstrap.min.js"></script>
    <script src="/src/scripts/fastclick.min.js"></script>
    <script src="/src/scripts/jquery.validate.min.js"></script>
    <script src="/src/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/src/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/src/bootcards-1.1.2/js/bootcards.min.js"></script>
    <script src="/js/app?v=GSsrzOsdjrO8Vya2kMFd6t9wqw5HuYxu1clSLWf11s81"></script>

    
    <script src="/js/help?v=DF-ZCs6GrRTFdWThwRea9fc-_j5GXfzTN4vPLC9rWx81"></script>

    <script type="text/javascript">
        $('input[name="PayType"]').change(function () {
            $(".pay-content").hide();
            var v = $('input[name="PayType"]:checked')[0].value;
            if (v == "1") {
                $(".pay-zhifubao-content").show();
            } else {
                $(".pay-weixin-content").show();
            }
        });
    </script>

    <script type="text/javascript">
        bootcards.init({
            offCanvasHideOnMainClick: true,
            offCanvasBackdrop: true,
            enableTabletPortraitMode: true,
            disableRubberBanding: false,
            disableBreakoutSelector: 'a.no-break-out'
        });

        //fix for minimal-ui bug in Safari:
        //http://stackoverflow.com/questions/22391157/gray-area-visible-when-switching-from-portrait-to-landscape-using-ios-7-1-minima
        if (bootcards.isXS()) {
            window.addEventListener("orientationchange", function () {
                window.scrollTo(0, 0);
            }, false);

            //initial redraw - needed to fix an issue with the minimal-ui and foot location
            //when the page if first opened
            window.scrollTo(0, 0);
        }
    </script>
    <script type="text/javascript">
        var value = $('input[name="navmenu_title"]').val();
        if (value) {
            $('a[data-title="' + value + '"]').parent().addClass('active');
        }
    </script>
    <script src="/js/weixinsdk?v=FuHO4EYdVtWj94At6HA29wrWQNt6iYo2nqlZNRPrmqE1"></script>

    <script type="text/javascript">
    if (isWeixin) {
        $.get('/home/getShare', function (data) {
            weixinService.init(data.title, data.desc, data.imgUrl, data.link);
        })
    }
    </script>
    <script src="/src/scripts/takeit.js"></script>
</body>
</html>

