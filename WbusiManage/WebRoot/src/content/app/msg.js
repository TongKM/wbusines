define(['jquery', 'xhrs', 'requirejs/text!/src/content/app/templates/messge.tpl.html', 'template7', 'bootstrap'], function ($, xhrs, msgTpl) {
    var intervalId;
    var lastMsgId;
    var intervalMsgId, intervalResolveId;
    var audioNewMessage = new Audio(path + '/content/medias/new.wav');
    var audioResolvedMessage = new Audio(path + '/content/medias/resolved.wav');
    var title = document.title;
    var isInit = false;
    var hideCoverTime = 5000;

    var url;
    if (window.location.hash && window.location.hash.length > 0)
        url = window.location.hash.substring(1, window.location.hash.length);
    else
        url = homeUrl;
    document.getElementById('iframeMain').src = url;


    $(window).resize(function () {
        initFrame();
    });

    function initFrame() {
        var i = $('#iframeMain').contents().find('#btn_menu');
        if (i && i.length == 1) {
            if (i.is(':visible'))
                $('.bt-menu').show();
            else
                $('.bt-menu').hide();
        }
    }

    $('#iframeMain').on('load', function () {
        if (!isInit) {
            isInit = true;
            initFrame();
        }

        $('#iframeMain')[0].contentWindow.onbeforeunload = function () {
            $('.bt-refresh').addClass('spinning');

            $('.cover').stop().fadeIn();
            intervalId = setTimeout(function () {
                $('.cover').stop().fadeOut('slow');
            }, hideCoverTime);
        };

        clearInterval(intervalId);
        intervalId = undefined;
        $('.cover').stop().fadeOut('fast');

        var i = $('#iframeMain').contents().get(0);
        history.replaceState(null, '', window.location.pathname + "#" + i.location.pathname + i.location.search);
        document.title = i.title;

        $('.bt-refresh').removeClass('spinning');
    });


    $('.bt-home').click(function () {
        document.getElementById('iframeMain').contentWindow.location.href = homeUrl;
    });

    $('.bt-back').click(function () {
        document.getElementById('iframeMain').contentWindow.history.back()
    });

    $('.bt-refresh').click(function () {
        document.getElementById('iframeMain').contentWindow.location.reload();
    });

    $('.bt-user').click(function () {
        document.getElementById('iframeMain').contentWindow.location.href = '/Admin/Employee/Details';
    });

    $('.bt-logout').click(function () {
        var b = confirm('您确定要安全退出吗？');
        if (!b)
            return;

        document.getElementById('iframeMain').contentWindow.location.href = '/Account/LogOff';
        var b = $('.msg-list').find('.msg-item:visible');
        if (b.length > 0)
            b.slideUp('slow', function () {
                toggleShowMessage();
            });
        else
            toggleShowMessage();

        clearInterval(intervalMsgId);
        clearInterval(intervalResolveId);
    });

    $('.bt-menu ').click(function () {
        $('#btn_menu', $('#iframeMain').contents()).trigger('click');
    });

    $('.bt-msg-center').click(function () {
        var b = !$(this).data('show-message');
        toggleShowMessage(b);
    });

    $('.msg-list').on('click', '.msg-item', function () {
        var url = $(this).data('url');
        if (url)
            if (url.indexOf('http') == 0)
                window.open('/Tool/OpenOuterPage?url=' + encodeURIComponent(url));
            else
                $('#iframeMain')[0].src = url;

        $(this).find('.bt-delete').trigger('click');
    });

    $('.msg-list').on('mouseenter', '.msg-item', function () {
        $(this).find('.glyphicon').each(function () {
            var b = $(this).hasClass('hidden');
            if (b)
                $(this).removeClass('hidden');
            else
                $(this).addClass('hidden');
        });

        var logo = $(this).find('img.logo');
        if (logo.is(':visible'))
            $(this).find('img.logo').popover('show');
    });

    $('.msg-list').on('mouseleave', '.msg-item', function () {
        $(this).find('.glyphicon').each(function () {
            var b = $(this).hasClass('hidden');
            if (b)
                $(this).removeClass('hidden');
            else
                $(this).addClass('hidden');
        })
        $(this).find('img.logo').popover('hide');
    });

    $('.msg-list').on('click', '.bt-delete', function (e) {
        var msgId = $(this).parents('.msg-item').data('key');
        var target = $(this);

        if (msgId)
            xhrs.post('/api/msgcenter/delete', { '': msgId }, function (data) {
                if (data > 0) {
                    $(target).parents('.msg-item').slideUp('slow', function () {
                        toggleShowMessage();
                    });
                }
            });
        else {
            $(target).parents('.msg-item').slideUp('slow', function () {
                toggleShowMessage();
            });
        }

        e.preventDefault();
        e.stopPropagation();
    });

    var compiledTemplate = Template7.compile(msgTpl);

    //xhrs.get('/api/msgcenter/get_unread', function (data) {
    //    var msgData = data;

    //    getMsgUserLogo(data, function (msgData) {
    //        var result = compiledTemplate(msgData);
    //        $('.msg-list').html(result);
    //    });
    //});

    var isLoadingNew = false, isLoadingResolve = false;
    var loadingCount = 0, loadingResolveCount = 0;
    var newIntervalTime = 120000, resolveIntervalTime = 60000;

    getNewMessage();

    intervalMsgId = setInterval(function () {
        if (!isLoadingNew || loadingCount > 5)
            getNewMessage();

        loadingCount++;
    }, newIntervalTime);

    intervalResolveId = setInterval(function () {
        if (!isLoadingResolve || loadingResolveCount > 5)
            getMessageResolved();

        loadingResolveCount++;
    }, resolveIntervalTime);

    function getNewMessage() {
        isLoadingNew = true;
        loadingCount = 0;

        xhrs.get('/api/msgcenter/get_new', { lastMsgId: lastMsgId }, function (data) {
            isLoadingNew = false;
            getMsgUserLogo(data, function (msgData) {
                $('.default-msg').slideUp();
                var result = compiledTemplate(msgData);
                $(result).appendTo('.msg-list').hide().slideDown('slow', function () {
                    alertNewMessage();
                    toggleShowMessage();
                });
                //$(result).appendTo('.msg-list').hide("slide", { direction: "down" }, 1000);
            });
        });
    }

    function getMessageResolved() {
        isLoadingResolve = true;
        loadingResolveCount = 0;

        var messageIds = [];
        $('.msg-list').find('.need-resolve').each(function () {
            var msgId = $(this).data('key');
            messageIds.push(msgId);
        });

        if (messageIds && messageIds.length > 0) {
            xhrs.post('/api/msgcenter/get_msgs_isresolved', { '': messageIds.toString() }, function (data) {
                isLoadingResolve = false;
                if (data && data.count > 0) {

                    audioResolvedMessage.play();

                    data.msgs.map(function (x) {
                        var item = $('.msg-list').find('.msg-item[data-key="' + x.messageId + '"]');
                        item.removeClass('need-resolve').addClass('resolved');
                        item.find('.button').html(' <span class="glyphicon glyphicon-ok" alt="点击删除"></span> <a class="bt-delete" href="#"> <span class="glyphicon glyphicon-trash hidden" alt="点击删除"></span> </a>')
                    });
                }
            });
        }
    }

    function getMsgUserLogo(msgData, func) {
        if (!msgData || msgData.count == 0)
            return;

        lastMsgId = msgData.msgs[msgData.msgs.length - 1].messageId;

        var userIds = msgData.msgs.map(function (x) {
            return x.sendUserId;
        }).toString();

        if (userIds && userIds.length > 0) {

            xhrs.post('/api/user/get_user_logo', { '': userIds }, function (data) {
                msgData.msgs.map(function (x) {
                    var user = data.filter(function (a) {
                        return a.userId == x.sendUserId;
                    });

                    if (user.length > 0) {
                        x.logo = user[0].logo && user[0].logo.length > 0 ? user[0].logo : '/src/images/site/user-logo.jpg';
                        x.realName = user[0].realName;
                    }
                    else {
                        x.logo = '/src/images/site/user-logo.jpg';
                    }
                });

                if (func)
                    func(msgData);
            });
        }
    }

    function alertNewMessage() {
        audioNewMessage.play();

        var timer = "";
        var isBlurred = false;

        if (!isBlurred)
            timer = window.setInterval(function () {
                document.title = (document.title == ('【　　　】' + title) ? ('【新消息】' + title) : ('【　　　】' + title));
            }, 1000);

        $(window).on("blur", function () {
            isBlurred = true;
        }).on("focus", function () {
            isBlurred = false;
            document.title = title;
            clearInterval(timer);
        });

        hideNoUseMsg();
    }

    function hideNoUseMsg() {
        var height = $('.msg-list').height();
        var containerHeight = $('.container').height();

        if (height > containerHeight) {
            var hideItem = $('.msg-list').find('.resolved:first');

            //if (hideItem.length == 0)
            //    hideItem = $('.msg-list').find('.no-need-resolve:first');

            if (hideItem.length == 1) {
                hideItem.slideUp('slow', function () {
                    $(this).find('.bt-delete').trigger('click');
                    $(this).remove();
                    setTimeout(hideNoUseMsg, 1);
                });
            }
        }
    }

    function toggleShowMessage(b) {
        var count = $('.msg-list').find('.msg-item:visible').length;

        if (b == undefined)
            b = count > 0;
        if (count > 0)
            $('#msgCount').html('【' + count + '条】');
        else
            $('#msgCount').html('');

        if (b) {
            $('#app').removeClass('no-message').addClass('with-message');
            $('#message').removeClass('no-message').addClass('with-message');
            $('#bt-msg-center').addClass('with-message');
        }
        else {
            $('#app').removeClass('with-message').addClass('no-message');
            $('#message').removeClass('with-message').addClass('no-message');
            $('#bt-msg-center').removeClass('no-message');
        }
        $('.bt-msg-center').data('show-message', b);
    }
});