define(['jquery'], function ($) {
    return {
        post: function (url, pData, func) {
            if (!token || token.length == 0) {
                return;
            }

            if (typeof pData == 'function') {
                func = pData;
                pData = undefined;
            }

            $.post(url + '?token=' + token, pData, function (data) {
                if (data && data.status == 4096) {
                    console.log(data.message);
                }
                else if (func)
                    func(data);
            });
        },
        get: function (url, query, func) {
            if (!token || token.length == 0) {
                return;
            }

            var apiServer = '';

            if (typeof query == 'function') {
                func = query;
                query = undefined;
            }

            if (query) {
                for (var name in query) {
                    apiServer += name + '=' + query[name] + '&';
                }
                apiServer = apiServer.replace(/&$/gi, '');
            }

            $.get(url + '?token=' + token + '&' + apiServer, function (data) {
                if (data && data.status == 4096){
                    console.log(data.message);
                }
                else if (func)
                    func(data);
            });
        },

    }
})