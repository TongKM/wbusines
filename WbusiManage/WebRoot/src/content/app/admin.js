﻿require.config({
    baseUrl: path,
    paths: {
        'jquery': ['jquery/jquery-2.1.4'],
        'bootstrap': ['bootstrap-3.3-2.5-dist/js/bootstrap.min'],
        'xhrs': ['content/app/utils/xhrs'],
        'template7': ['Template7-1.1.0/dist/template7'],
        'msg': ['content/app/msg'],
        'takit': ['content/app/takeit'],
    },
    shim: {
        bootstrap: ['jquery']
    },
})

require(['msg'], function ($, xhrs) {

});