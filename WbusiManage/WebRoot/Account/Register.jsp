<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    
    
    <title>选择注册的方式 - 代理</title>

    <script type="text/javascript">
        (function appInit() {
            if (top)
                if (top.location.href != self.location.href) {
                    top.location.href = self.location.href;
                }
        })();
    </script>

    <link href="/WbusiManage/src/bootcards-1.1.2/css/bootcards-ios.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/WbusiManage/src/icomoon/style.css" rel="stylesheet" />

    <link href="/WbusiManage/css/v1.css" rel="stylesheet"/>

    <link href="/WbusiManage/src/content/pc.css" rel="stylesheet" />

    <!--<link rel="icon" href="图片位置" type="image/x-icon"/>-->

    <style type="text/css">
        h2{
            color: gray;
            font-style: italic;
        }
        a, a:hover {
            color:#dfdbdb
        }
    </style>

</head>
<body>
<div class="container bootcards-container">
    <div class="iframe">
        <div class="row">
            <div class="register-frame">
                <div class="text-center" style="margin:25px 0 ;">
                    <h2><img style="width: 110px;height: 86px" src="/WbusiManage/src/content/images/register-logo.gif"></h2>
                </div>
                <div class="text-center">
                    <form id="formss" onsubmit="return subEmil();" action="/WbusiManage/reg/RegisterWithTel" class="registerForm" id="registerSave" method="post" role="form" style="padding-left:15px;padding-right:15px" >        
                    <div class="form-group">
                        <label class="sr-only" for="Tel">邮箱账号</label>
                        <input class="form-control" data-val="true" data-val-regex="请填写正确邮箱账号" data-val-regex-pattern="^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$" data-val-remote="&#39;邮箱账号&#39; is invalid." data-val-remote-additionalfields="*.Tel" data-val-remote-url="/Account/CheckTelepone" data-val-required="请填写邮箱账号" id="Tel" name="Tel" placeholder="注册邮箱账号" type="text" value="${emil }" />
                        <span class="field-validation-valid text-danger" data-valmsg-for="Tel" data-valmsg-replace="true"></span>
                    </div>
                       <!--  <div class="form-group">
                            <label class="sr-only" for="Password">密码</label>
                            <input class="form-control" data-val="true" data-val-length="密码为5~18为字符" data-val-length-max="18" data-val-length-min="5" data-val-required="请填写密码" id="Password" name="Password" placeholder="密码" type="password" />
                            <span class="field-validation-valid text-danger" data-valmsg-for="Password" data-valmsg-replace="true"></span>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="ConfirmPassword">确认密码</label>
                            <input class="form-control" data-val="true" data-val-equalto="密码和确认密码不匹配。" data-val-equalto-other="*.Password" id="ConfirmPassword" name="ConfirmPassword" placeholder="确认密码" type="password" />
                            <span class="field-validation-valid text-danger" data-valmsg-for="ConfirmPassword" data-valmsg-replace="true"></span>
                        </div> -->
                        <div class="form-group panel panel-none">
                            <input type="submit" id="btn_Save" class="btn btn-desktop-primary btn-lg btn-block" onclick="" style="border-radius:6px" value="确定" />
                        </div>
                    </form>    
                    </div>

                <!-- <div class="text-center" style="margin-top:15px;">

                    <a href="/Account/LoginWithTencent?returnUrl=%2FAgency%2FRegister">QQ申请注册</a>
                    |
                    <a href="/Account/LoginWithSina?returnUrl=%2FAgency%2FRegister">新浪微博申请注册</a>

                    |
                    <a href="/Account/RegisterWithWeixin?returnUrl=%2FAgency%2FRegister">微信申请注册</a>
-->

                    <div style="margin-top:15px;">
                        &nbsp;&nbsp;&nbsp;我已经有账号，
                        <a style="color: black;font-style: italic;" href="/WbusiManage/Account/Login.jsp">立即登陆</a>
					           
                </div> 
            </div>

        </div>
    </div>
</div>
<input id="hidden" type="hidden" value="${error }"/>
<script src="/WbusiManage/src/jquery/jquery-2.1.4.js"></script>

<script src="/WbusiManage/src/scripts/jquery.validate.min.js"></script>
<script src="/WbusiManage/src/scripts/jquery.validate.unobtrusive.min.js"></script>
<script src="/WbusiManage/src/scripts/jquery.unobtrusive-ajax.min.js"></script>

<script src="/WbusiManage/src/scripts/takeit.js"></script>

<script>
	$(function(){
		if($("#hidden").val()==1)
			{
				alert("此邮箱已被注册，请重新填写邮箱!");
			}
	});
   function subEmil(){
    var temp = document.getElementById("Tel");
   	var re=/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
   	if(re.test(temp.value)){
   		//$("#formss").attr('action','reg/RegisterWithTel');
   		//$("#formss").submit();
   		location.href='/WbusiManage/reg/RegisterWithTel?Tel='+temp.value;
   		return true;
   	}
   	return false;
   }
   
</script>
  </body>
</html>
